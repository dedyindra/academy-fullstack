package com.enigma.model;

import java.util.Objects;

public class Circle extends Shape{
    protected final Double pi=3.14;
    protected   Double r;

    public   Circle(Double r){
        this.r = r;
    }

    public Double getR() {
        return r;
    }

    public void setR(Double r) {
        this.r = r;
    }

    public Circle() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Circle circle = (Circle) o;
        return Objects.equals(r, circle.r);
    }

    @Override
    public int hashCode() {
        return Objects.hash(r);
    }

    @Override
    public Double getSurface() {
            return pi*r*r;
    }

    public   Double getRound(){
        return pi*r*2;
    }


}
