package com.enigma.model;

public class Block extends  Rectangle {
    Double height ;
    public Block(Double length, Double width,Double height) {
        super(length, width);
        this.height = height;
    }

    public Double getVolume(){
        return this.length*this.width*this.height;
    }
}
