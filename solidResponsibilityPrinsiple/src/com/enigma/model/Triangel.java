package com.enigma.model;

public class Triangel extends Shape{
    private  Double base;
    private Double height;

    public Triangel(Double base, Double height) {
        this.base = base;
        this.height = height;
    }

    @Override
    public Double getSurface() { return 0.5*base*height;
    }
}
