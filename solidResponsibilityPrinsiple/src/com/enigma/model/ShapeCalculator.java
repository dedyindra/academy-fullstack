package com.enigma.model;

import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;

public class ShapeCalculator {
 List<Shape> shape = new ArrayList<>();
 // Depedency injection
CircleDao circleDao;

    public ShapeCalculator(CircleDao circleDao) {
        this.circleDao = circleDao;
    }

    public void addShape(Shape shape){
     this.shape.add(shape);
 }
 public   Double  calculate(){
     Double relust=0.0;
     for (Shape shape1 :shape ) {
     relust = relust+shape1.getSurface(); }
     return relust;
    }

    // Depedency injection
    public   Double  sum(){
     List<Circle> shapes = circleDao.getall();
        Double relust=0.0;
        for (Shape shape1 :shape ) {
            relust = relust+shape1.getSurface(); }
        return relust;
    }

    public void writeText() {
        try {
            FileWriter fileWriter = new FileWriter("E:\\first.txt",true);
            fileWriter.write(toString());
            fileWriter.close();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public String toString() {
        return "ShapeCalculator{" +
                "shape=" + calculate() +
                '}';
    }
}
