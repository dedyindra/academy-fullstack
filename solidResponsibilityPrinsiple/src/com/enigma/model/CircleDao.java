package com.enigma.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class CircleDao {


    List<Circle> circles = new ArrayList<>();
    public void save(Circle circle){
            circles.add(circle);
    }
    public List<Circle> getall(){
     return  this.circles;
    }
    public Circle get(Double r){
        Circle found = new Circle();
        for (Circle circle:circles ) {
             if (circle.getR()==r){
                 found =circle;
             }
        }
     return  found;
    }

}
