import React from 'react';
import './App.css';
import Navbar from "./navbar/Navbar";
import Sidebar from "./sidebar/Sidebar";
import Footer from "./footer/Footer";
import Content from "./content/Content";
import {BrowserRouter as Router, Switch, Route} from "react-router-dom";
import FormLogin from "./Login/FormLogin";

function App() {
    return (
        <Router>
            <Switch>
                <Route exact path="/">
                    <FormLogin/>
                </Route>
                <div className="wrapper">
                    <Navbar/>
                    <Sidebar/>
                    <Content/>
                    <Footer/>
                </div>
            </Switch>
        </Router>

    );
}

export default App;
