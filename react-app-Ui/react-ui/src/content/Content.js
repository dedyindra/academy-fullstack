import React from 'react';
import {BrowserRouter as Router, Switch, Route} from "react-router-dom";
import Dashboard from "./Dashboard";
import Genre from "./Genere";
import ListGenre from "./ListGenre";
import ArtistContainer from "./learn-redux/ArtistContainer";
import {Provider} from "react-redux";
import {createStore} from "redux";
import artist from "./learn-redux/ArtistReducer";

function Content() {
    return (

        <div className="wrapper">
            <div className="content-wrapper">
                <Switch>
                    <Route path="/dashboard">
                        <Dashboard/>
                    </Route>
                    <Route path="/artist">
                        <Provider store={createStore(artist)}>
                            <ArtistContainer/>
                        </Provider>
                    </Route>
                    <Route path="/genre">
                        <Genre/>
                    </Route>
                    <Route path="/list-genre">
                        <ListGenre/>
                    </Route>
                </Switch>
            </div>
        </div>

    );
}

export default Content;
