import React, {Component} from "react";

class ListGenre extends Component {

    render() {
        return (
            <>
                <div className="content-header">
                    <div className="container-fluid">
                        <div className="row mb-2">
                            <div className="col-sm-6">
                                <h1 className="m-0 text-dark">List Genre</h1>
                            </div>
                            <div className="col-sm-6">
                                <ol className="breadcrumb float-sm-right">
                                    <li className="breadcrumb-item"><a href="#">Home</a></li>
                                    <li className="breadcrumb-item active">List Genre</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>

                <section className="content">
                    <div className="container-fluid">
                        <div className="col-12">
                            <div className="card">
                                <div className="card-header">
                                    <h3 className="card-title">Responsive Hover Table</h3>

                                    <div className="card-tools">
                                        <div className="input-group input-group-sm" >
                                            <input type="text" name="table_search" className="form-control float-right"
                                                   placeholder="Search"/>

                                                <div className="input-group-append">
                                                    <button type="submit" className="btn btn-default"><i
                                                        className="fas fa-search"></i></button>
                                                </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="card-body table-responsive p-0">
                                    <table className="table table-hover table-striped" >
                                        <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>User</th>
                                            <th>Date</th>
                                            <th>Status</th>
                                            <th>Reason</th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>183</td>
                                            <td>John Doe</td>
                                            <td>11-7-2014</td>
                                            <td><span className="tag tag-success">Approved</span></td>
                                            <td>Bacon ipsum dolor sit amet salami venison chicken flank fatback doner.
                                            </td>
                                            <td className="text-right py-0 align-middle">
                                                <div className="btn-group btn-group-sm">
                                                    <a href="#" className="btn btn-info"><i className="fas fa-eye"></i></a>
                                                    <a href="#" className="btn btn-warning"><i className="fas fa-edit"></i></a>
                                                    <a href="#" className="btn btn-danger"><i
                                                        className="fas fa-trash"></i></a>
                                                </div>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>175</td>
                                            <td>Mike Doe</td>
                                            <td>11-7-2014</td>
                                            <td><span className="tag tag-danger">Denied</span></td>
                                            <td>Bacon ipsum dolor sit amet salami venison chicken flank fatback doner.
                                            </td>
                                            <td className="text-right py-0 align-middle">
                                                <div className="btn-group btn-group-sm">
                                                    <a href="#" className="btn btn-info"><i className="fas fa-eye"></i></a>
                                                    <a href="#" className="btn btn-warning"><i className="fas fa-edit"></i></a>

                                                    <a href="#" className="btn btn-danger"><i
                                                        className="fas fa-trash"></i></a>
                                                </div>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>

                </section>
            </>
        );

    }

}
export default ListGenre;
