import React, {Component} from "react";

class Genre extends Component {
    render() {
        return (
            <>
                <div className="content-header">
                    <div className="container-fluid">
                        <div className="row mb-2">
                            <div className="col-sm-6">
                                <h1 className="m-0 text-dark">Genre</h1>
                            </div>
                            <div className="col-sm-6">
                                <ol className="breadcrumb float-sm-right">
                                    <li className="breadcrumb-item"><a href="#">Home</a></li>
                                    <li className="breadcrumb-item active">Genre</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
                <section className="content">
                    <div className="container-fluid">
                        <div className="card card-default">
                            <div className="card-header">
                                <h3 className="card-title">Form Genre</h3>

                                <div className="card-tools">
                                    <button type="button" className="btn btn-tool" data-card-widget="collapse"><i
                                        className="fas fa-minus"></i></button>
                                    <button type="button" className="btn btn-tool" data-card-widget="remove"><i
                                        className="fas fa-remove"></i></button>
                                </div>
                            </div>
                            <div className="card-body">
                                <div className="row">
                                    <div className="col-md-12">
                                    <div className="form-group">
                                        <label htmlFor="exampleInputEmail1">Email address</label>
                                        <input type="text" className="form-control" id="exampleInputEmail1"
                                               placeholder="Enter email"/>
                                    </div>
                                    </div>
                                    <div className="col-md-12">
                                        <div className="form-group">
                                            <label>Minimal</label>
                                            <select className="form-control select2" >
                                                <option selected="selected">Alabama</option>
                                                <option>Alaska</option>
                                                <option>California</option>
                                                <option>Delaware</option>
                                                <option>Tennessee</option>
                                                <option>Texas</option>
                                                <option>Washington</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div className="card-footer">
                                <button type="submit" className="btn btn-primary">Submit</button>

                            </div>
                        </div>
                    </div>

                </section>
            </>
        );

    }

}
export default Genre;
