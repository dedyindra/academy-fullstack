import React, {Component} from "react";
import {connect} from "react-redux"
import {fetchDataArtist} from "./ArtistService";
import {getArtist} from "./ArtistAction";

class Artistlist extends Component {
    componentDidMount() {
        this.fetchDetail()
    }
    fetchDetail = async () => {
        const data = await fetchDataArtist();
        this.props.dispatch({...getArtist,payload:data});
    }
    render() {

        return (
            <div className="card">
                <div className="card-header">
                    <h3 className="card-title">Responsive Hover Table</h3>

                    <div className="card-tools">
                        <div className="input-group input-group-sm">
                            <input type="text" name="table_search" className="form-control float-right"
                                   placeholder="Search"/>

                            <div className="input-group-append">
                                <button type="submit" className="btn btn-default"><i
                                    className="fas fa-search"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="card-body table-responsive p-0">
                    <table className="table table-hover table-striped">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>User</th>
                            <th>Date</th>
                            <th>Status</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        {this.props.artists.map((element, index) => {
                            return  <tr>
                                <td>{index + 1}</td>
                                <td>{element.name}</td>
                                <td>{element.bornPlace}</td>
                                <td>{element.debut}</td>

                                <td className="text-right py-0 align-middle">
                                    <div className="btn-group btn-group-sm">
                                        <a href="#" className="btn btn-info"><i className="fas fa-eye"></i></a>
                                        <a href="#" className="btn btn-warning"><i className="fas fa-edit"></i></a>
                                        <a href="#" className="btn btn-danger"><i
                                            className="fas fa-trash"></i></a>
                                    </div>
                                </td>
                            </tr>
                        })}

                        </tbody>
                    </table>
                </div>
            </div>
        );

    }

}
const mapStateToProps = (state) => {
    return {...state,artists: state.artists}
}

export default connect(mapStateToProps)(Artistlist);
