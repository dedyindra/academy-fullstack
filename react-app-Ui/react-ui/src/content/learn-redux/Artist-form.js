import React, {Component} from "react";
import {connect} from "react-redux";
import {fetchDataArtist, PostData} from "./ArtistService";
import {changebornplace, changedebut, changeName, getArtist} from "./ArtistAction";

class ArtistForm extends Component {

    handleOnSubmit = async (event) => {
        event.preventDefault()
        let artist = this.props.artist;
        await PostData(artist).then(this.fetchDetail)

    }

    componentDidMount() {
        this.fetchDetail()
    }
    fetchDetail = async () => {
        const data = await fetchDataArtist();
        this.props.dispatch({...getArtist,payload:data});
    }
    render() {
        return (
            <div className="card card-default">
                <div className="card-header">
                    <h3 className="card-title">Form Genre</h3>
                </div>
                <form onSubmit={this.handleOnSubmit}>
                <div className="card-body">
                    <div className="row">
                        <div className="col-md-12">
                            <div className="form-group" >
                                <label htmlFor="exampleInputEmail1">Name Artist</label>
                                <input type="text" className="form-control" onChange={(event) => {
                                    this.props.dispatch({
                                        ...changeName, index: this.props.index,
                                        newName: event.target.value
                                    })
                                }} value={this.props.name}
                                       id="exampleInputEmail1"
                                       placeholder="Enter email"/>
                            </div>
                        </div>
                        <div className="col-md-12">
                            <div className="form-group">
                                <label htmlFor="exampleInputEmail1">born Place</label>
                                <input type="text" className="form-control" onChange={(event) => {
                                    this.props.dispatch({
                                        ...changebornplace,
                                        index: this.props.index,
                                        newBornPlace: event.target.value
                                    })
                                }} value={this.props.bornPlace}

                                       id="exampleInputEmail1"
                                       placeholder="Enter email"/>
                            </div>
                        </div>

                        <div className="col-md-12">
                            <div className="form-group">
                                <label htmlFor="exampleInputEmail1">Debut</label>
                                <input type="date" className="form-control" onChange={(event) => {
                                    this.props.dispatch({
                                        ...changedebut, index: this.props.index,
                                        newDebut: event.target.value
                                    })
                                }} value={this.props.debut}
                                       id="exampleInputEmail1"
                                       placeholder="Enter email"/>
                            </div>
                        </div>



                        <div className="col-md-12">
                            <div className="form-group">
                                <label>Minimal</label>
                                <select className="form-control select2" >
                                    <option selected="selected">Alabama</option>
                                    <option>Alaska</option>
                                    <option>California</option>
                                    <option>Delaware</option>
                                    <option>Tennessee</option>
                                    <option>Texas</option>
                                    <option>Washington</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="card-footer">
                    <button type="submit" className="btn btn-primary">Submit</button>

                </div>
                </form>
            </div>

        );

    }

}
const mapStateToProps = (state) => {
    return {...state}
}
export default connect(mapStateToProps)(ArtistForm);
