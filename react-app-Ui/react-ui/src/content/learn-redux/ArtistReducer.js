const intialState = {
    artists: [],
    artist: {
        name: "",
        bornPlace: "",
        debut: "",
        songs: []
    },
    song: []
}
export default function artist(state = intialState, action) {
    console.log(state, action, "ini artist")
    switch (action.type) {
        case 'ADD_SONG':
            return {...state, artist: {...state.artist, songs: state.artist.songs.concat([{nameLagu: ""}])}};
        case "GET_ARTIST":
            return {...state, artists: action.payload};
        case "GET_SONG":
            return {...state, song: action.payload};
        case 'HANDLE_CHANGE_NAME':
            return  {...state, artist: {...state.artist, name: action.newName }}
        case 'HANDLE_CHANGE_BORNPLACE':
            return  {...state, artist: {...state.artist, bornPlace: action.newBornPlace }}
        case 'HANDLE_CHANGE_DEBUT':
            return  {...state, artist: {...state.artist, debut: action.newDebut }}
        case 'HANDLE_CHANGE_NAMELAGU':
            return {...state, artist : {...state.artist, songs: state.artist.songs.map((element,index)=>{
                        if (index===action.index){
                            return  {...element,  nameLagu: action.newNameLagu }
                        }else {
                            return element
                        }
                    })}}
        default:
            return state;
    }
}
