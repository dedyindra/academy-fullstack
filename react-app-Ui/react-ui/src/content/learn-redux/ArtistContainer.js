import React, {Component} from "react";
import Artistlist from "./Artist-list";
import ArtistForm from "./Artist-form";
import {connect} from "react-redux";

class ArtistContainer extends Component {
    render() {
        return (
            <>
                <div className="content-header">
                    <div className="container-fluid">
                        <div className="row mb-2">
                            <div className="col-sm-6">
                                <h1 className="m-0 text-dark">Genre</h1>
                            </div>
                            <div className="col-sm-6">
                                <ol className="breadcrumb float-sm-right">
                                    <li className="breadcrumb-item"><a href="#">Home</a></li>
                                    <li className="breadcrumb-item active">Genre</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
                <section className="content">
                    <div className="container-fluid">
                        <div className="row">
                        <div className="col-md-6">
                            <ArtistForm/>
                        </div>
                        <div className="col-md-6">
                            <Artistlist/>
                        </div>
                        </div>
                    </div>

                </section>
            </>
        );

    }

}
export default connect()(ArtistContainer);
