import React from 'react';
import {Link} from "react-router-dom";

function Sidebar() {
    return (
        <div>
            <aside className="main-sidebar sidebar-dark-primary elevation-4">
                <a href="index3.html" className="brand-link">
                    <img src="dist/img/AdminLTELogo.png" alt="AdminLTE Logo"
                         class="brand-image img-circle elevation-3"/>
                    <span className="brand-text font-weight-light"> GALON ISI ULANG</span>
                </a>
                <div className="sidebar">
                    <div className="user-panel mt-3 pb-3 mb-3 d-flex">
                        <div className="image">
                            <img src="dist/img/user2-160x160.jpg" className="img-circle elevation-2" alt="User Image"/>
                        </div>
                        <div className="info">
                            <a href="#" className="d-block">Alexander Pierce</a>
                        </div>
                    </div>

                    <nav className="mt-2">
                        <ul className="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
                            data-accordion="false">

                            {/*<li className="nav-item has-treeview menu-open">*/}
                            {/*    <a href="#" className="nav-link active">*/}
                            <li className="nav-item  ">

                                    <Link  className="nav-link " to="/dashboard" >
                                    <i className="nav-icon fas fa-tachometer-alt"></i>
                                    <p>
                                        Dashboard
                                    </p>
                                </Link>

                            </li>
                            <li className="nav-item  ">

                                <Link  className="nav-link " to="/artist" >
                                    <i className="nav-icon fas fa-tachometer-alt"></i>
                                    <p>
                                        Artist
                                    </p>
                                </Link>

                            </li>
                            <li className="nav-item has-treeview">
                                <Link href="#" className="nav-link  " >
                                    <i className="nav-icon fas fa-copy"></i>
                                    <p>
                                        Genre
                                        <i className="fas fa-angle-left right"></i>
                                    </p>
                                </Link>
                                <ul className="nav nav-treeview">
                                    <li className="nav-item">
                                        <Link to="/genre" className="nav-link">
                                            <i className="far fa-circle nav-icon"></i>
                                            <p>Form Genre</p>
                                        </Link>
                                    </li>
                                    <li className="nav-item">
                                        <Link to="/list-genre" className="nav-link">
                                            <i className="far fa-circle nav-icon"></i>
                                            <p>List Genre</p>
                                        </Link>
                                    </li>
                                </ul>
                            </li>

                        </ul>
                    </nav>
                </div>
            </aside>

        </div>

    );
}

export default Sidebar;
