class Player {
    constructor(id,name,club,nationality,position,createAt,updateAt){
        this.id = id;
        this.name=name;
        this.club = club;
        this.nationality = nationality;
        this.position = position;
        this.createAt = createAt;
        this.updateAt = updateAt;
    }
}
export default Player;