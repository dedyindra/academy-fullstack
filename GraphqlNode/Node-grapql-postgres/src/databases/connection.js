import { createConnection } from "typeorm";

async function createDbConnection() {
    const connection = await createConnection({
        type: process.env.DB_DRIVER || "postgres",
        host: process.env.DB_HOST || "localhost",
        port: process.env.DB_PORT || 5432,
        username: process.env.DB_USERNAME || "postgres",
        password: process.env.DB_PASSWORD || "enigmacamp",
        database: process.env.DB_NAME || "graphqlLearn",
        synchronize: process.env.DB_SYNC === "true" || true,
        logging: process.env.DB_LOGGING === "true" || true,

    });
    return connection;
}
export default createDbConnection;
