package com.enigma.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Movement {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    private final String RIGHT = "R";
    private final String LEFT = "L";
    private final String FORWARD = "F";
    private final String BACK = "B";


}
