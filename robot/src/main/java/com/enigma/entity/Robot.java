package com.enigma.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "robot")
public class Robot {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
   private Integer id;
    private Integer postX;
    private Integer postY;
    private Integer fuel;
    @NotNull
    @Enumerated(EnumType.STRING)
    Direction direction;

    public Robot(Integer postX, Integer postY, Integer fuel, @NotNull Direction direction) {
        this.postX = postX;
        this.postY = postY;
        this.fuel = fuel;
        this.direction = direction;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getPostX() {
        return postX;
    }

    public void setPostX(Integer postX) {
        this.postX = postX;
    }

    public Integer getPostY() {
        return postY;
    }

    public void setPostY(Integer postY) {
        this.postY = postY;
    }

    public Integer getFuel() {
        return fuel;
    }

    public void setFuel(Integer fuel) {
        this.fuel = fuel;
    }

    public Direction getDirection() {
        return direction;
    }

    public void setDirection(Direction direction) {
        this.direction = direction;
    }
}
