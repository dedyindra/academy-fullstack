package com.enigma.demo.service;

import com.enigma.entity.Outlet;
import com.enigma.exception.ResourceNotFoundException;
import com.enigma.repositories.OutletRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.text.SimpleDateFormat;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(SpringExtension.class)
@SpringBootTest
class OutletServiceImplTest {
@Autowired
    OutletService outletService;
@MockBean
AnotherService anotherService;
@MockBean
    OutletRepository outletRepository;
    @Test
    void createNewOutlet() {
            Outlet newOutlet=new Outlet("Kokas","A12");
            outletService.createNewOutlet(newOutlet);
            Mockito.verify(anotherService,Mockito.times(1)).panggilCetak(newOutlet.getOutletName());
                }
    @Test
    void createNewOutlet_sould_save() {
        Outlet newOutlet=new Outlet("Kokas","A12");
        outletService.createNewOutlet(newOutlet);
        Mockito.verify(outletRepository,Mockito.times(1)).save(newOutlet);
    }
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat();
    @Test
    void getOutletById() {
        //persiapan
        Outlet outlet = new Outlet("Dedy indra","A02");
        Mockito.when(outletRepository.findById(10)).thenReturn(Optional.of(outlet));
        //progress
        outletService.getOutletById(10);
        Mockito.verify(outletRepository,Mockito.times(1)).findById(10);
    }


    @Test
    void getByid_should_equal_objeck_mock(){
        Outlet outlet = new Outlet("Dedy indra","A02");
            outlet.setId(10);
        Mockito.when(outletRepository.findById(10)).thenReturn(Optional.of(outlet));
        Outlet outlet1 =outletService.getOutletById(10);
        assertEquals (outlet,outlet1);
    }
    @Test
    void GetTrowsException() {
        Assertions.assertThrows(ResourceNotFoundException.class,()->{outletService.getOutletById(8999);});
    }
}