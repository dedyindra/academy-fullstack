package com.enigma.repositories;

import com.enigma.entity.Outlet;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
@Repository
public interface OutletRepository extends JpaRepository<Outlet,Integer> {

}
