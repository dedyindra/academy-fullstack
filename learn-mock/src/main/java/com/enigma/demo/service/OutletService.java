package com.enigma.demo.service;
import com.enigma.entity.Outlet;
import java.util.List;

public interface OutletService {
    Outlet createNewOutlet(Outlet outlet);
    List<Outlet> getOutlet();
    Outlet getOutletById(Integer id);
}
