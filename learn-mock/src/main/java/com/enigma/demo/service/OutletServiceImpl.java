package com.enigma.demo.service;
import com.enigma.entity.Outlet;
import com.enigma.repositories.OutletRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
@Service
public class OutletServiceImpl implements OutletService {
@Autowired
    OutletRepository outletRepository;
@Autowired
AnotherService anotherService;
    @Override
    public Outlet createNewOutlet(Outlet outlet) {
        anotherService.panggilCetak(outlet.getOutletName());
        return outletRepository.save(outlet);
    }
    @Override
    public List<Outlet> getOutlet() {
        return outletRepository.findAll();
    }

    @Override
    public Outlet getOutletById(Integer id) {
        return outletRepository.findById(id).get();
    }
}


