package com.enigma.demo.service;

import org.springframework.stereotype.Service;

@Service
public class AnotherServiceImpl implements AnotherService{
    @Override
    public void panggilCetak(String message) {
        System.out.println(message);
    }
}
