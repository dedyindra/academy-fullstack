package com.enigma.entity;

import javax.persistence.*;

@Entity
@Table(name = "mst_outlet")
public class Outlet {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private  String outletName;
    private  String outletCode;

    public Outlet(String outletName, String outletCode) {
        this.outletName = outletName;
        this.outletCode = outletCode;
    }

    public Outlet() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getOutletName() {
        return outletName;
    }

    public void setOutletName(String outletName) {
        this.outletName = outletName;
    }

    public String getOutletCode() {
        return outletCode;
    }

    public void setOutletCode(String outletCode) {
        this.outletCode = outletCode;
    }
}
