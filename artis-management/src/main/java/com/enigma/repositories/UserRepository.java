package com.enigma.repositories;

import com.enigma.entity.Song;
import com.enigma.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User,String> {
    public User findUserByUsername(String keyword);

}
