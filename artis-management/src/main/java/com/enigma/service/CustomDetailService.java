package com.enigma.service;

import com.enigma.Exception.UserNotResponse;
import com.enigma.entity.User;
import com.enigma.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class CustomDetailService implements UserDetailsService {

    @Autowired
    UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        //yng entity
        User userFound = userRepository.findUserByUsername(username);
        if (userFound==null){
            throw  new UserNotResponse();
        }
        //karena namanya sama ini manggil di paket
        return new org.springframework.security.core.userdetails.User(userFound.getUsername(),userFound.getPassword(),new ArrayList<GrantedAuthority>());
    }
}
