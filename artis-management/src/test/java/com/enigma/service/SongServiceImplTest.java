package com.enigma.service;

import com.enigma.entity.Song;
import com.enigma.repositories.SongRepository;
import com.enigma.service.impl.SongServices;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
@ExtendWith(SpringExtension.class)
@SpringBootTest
class SongServiceImplTest {
    @Autowired
    SongServices songServices;
    @MockBean
    SongRepository songRepository;
    @Test
    void save_data_song() {
        Song song = new Song("bertahan disana");
        songServices.save(song);
        Mockito.verify(songRepository, Mockito.times(1)).save(song);
    }
    @Test
    void get_data_byId(){
        Song song = new Song("mungkinkah");
        song.setId("dedya");
        Mockito.when(songRepository.findById("dedya")).thenReturn(Optional.of(song));
        Song song1 = songServices.getSongId("dedya");
        assertEquals(song,song1);
    }

    @Test
    void getAll_Song(){
        Song song = new Song("mungkinkah");
        song.setId("dedyIs");
        List<Song> lisSong = new ArrayList<>();
        lisSong.add(song);
        lisSong.add(song);
        Mockito.when(songRepository.findAll()).thenReturn( lisSong);
        assertEquals(2,songServices.getAll().size());
    }

}