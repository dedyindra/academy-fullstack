package com.enigma;

import com.enigma.entity.Mail;
import com.enigma.service.MailService;
import javafx.application.Application;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScans;
import org.springframework.stereotype.Component;

@SpringBootApplication

public class EmailApplication {

    public static void main(String[] args) {

        Mail mail = new Mail();
        mail.setMailFrom("dedyindra352@gmail.com");
        mail.setMailTo("dedyindra120@gmail.com");
        mail.setMailSubject("Email Example uji coba");
        mail.setMailContent("Learn How to send Email using Spring Boot!!!\n\nThanks\nwww.dedycloud.com");

        ApplicationContext ctx = SpringApplication.run(EmailApplication.class, args);
        MailService mailService = (MailService) ctx.getBean("mailService");
        mailService.sendEmail(mail);

    }
}