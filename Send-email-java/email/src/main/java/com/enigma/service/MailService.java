package com.enigma.service;


import com.enigma.entity.Mail;

public interface MailService {
    public void sendEmail(Mail mail);
}