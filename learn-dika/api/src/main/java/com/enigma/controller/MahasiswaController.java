package com.enigma.controller;

import com.enigma.entity.Mahasiwa;
import com.enigma.service.MahasiswaSerivce;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class MahasiswaController {
@Autowired
    MahasiswaSerivce mahasiswaSerivce;
@GetMapping("/mahasiswa")
public List<Mahasiwa> getAllMahasiswa(){
    return mahasiswaSerivce.getAllMahasiswa();
}

}
