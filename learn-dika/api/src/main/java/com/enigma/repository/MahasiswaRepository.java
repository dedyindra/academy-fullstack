package com.enigma.repository;

import com.enigma.entity.Mahasiwa;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MahasiswaRepository extends JpaRepository<Mahasiwa,String> {
}
