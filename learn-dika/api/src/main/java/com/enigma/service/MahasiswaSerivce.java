package com.enigma.service;

import com.enigma.entity.Mahasiwa;
import com.enigma.repository.MahasiswaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MahasiswaSerivce {
@Autowired
    MahasiswaRepository mahasiswaRepository;

public List<Mahasiwa> getAllMahasiswa(){
    return mahasiswaRepository.findAll();
}
}
