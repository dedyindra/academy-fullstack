package com.enigma.entity;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "mahasiswa")
public class Mahasiwa {
    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    private String id;
    private String nama;
    private Integer npm;
    private String jurusan;
    private String ProgramStudi;

    public Mahasiwa(String nama, Integer npm, String jurusan, String programStudi) {
        this.nama = nama;
        this.npm = npm;
        this.jurusan = jurusan;
        ProgramStudi = programStudi;
    }

    public Mahasiwa() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public Integer getNpm() {
        return npm;
    }

    public void setNpm(Integer npm) {
        this.npm = npm;
    }

    public String getJurusan() {
        return jurusan;
    }

    public void setJurusan(String jurusan) {
        this.jurusan = jurusan;
    }

    public String getProgramStudi() {
        return ProgramStudi;
    }

    public void setProgramStudi(String programStudi) {
        ProgramStudi = programStudi;
    }
}
