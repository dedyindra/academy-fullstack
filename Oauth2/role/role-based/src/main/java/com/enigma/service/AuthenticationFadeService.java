package com.enigma.service;

import org.springframework.security.core.Authentication;

public interface AuthenticationFadeService {
    Authentication getAuthentication();

}
