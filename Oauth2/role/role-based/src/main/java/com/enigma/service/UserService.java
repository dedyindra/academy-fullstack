package com.enigma.service;

import com.enigma.dto.UserDto;
import com.enigma.model.User;

import java.util.List;

public interface UserService {

    UserDto save(UserDto user);
    List<UserDto> findAll();
    User findOne(long id);
    void delete(long id);
}
