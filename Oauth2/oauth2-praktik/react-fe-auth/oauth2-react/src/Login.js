import React, { Component } from 'react';
import './Login.css';
import AuthService from "./components/AuthService";

class Login extends Component {
    constructor(){
        super();
        this.handleFormSubmit = this.handleFormSubmit.bind(this);
        this.Auth = new AuthService();
        this.state = {
            loginForm: {
                username :"",
                password:""
            }
        }
    }
    handleInputUsername = (event) => {
        let login = {...this.state.loginForm}
        login.username = event.target.value;
        this.setState({loginForm: {...login}})
        console.log(login.username)
    }
    handleInputPassword = (event) => {
        let pass = {...this.state.loginForm}
        pass.password = event.target.value;
        this.setState({loginForm: {...pass}})
        console.log(pass.password )
    }
    componentWillMount(){
        if(this.Auth.loggedIn())
            this.props.history.replace('/');
    }
    handleFormSubmit(e){
        e.preventDefault();
        console.log("hit")
        this.Auth.login(this.state.loginForm)

            .then(res =>{
                this.props.history.replace('/');
            })
            .catch(err =>{
                alert(err);
            })
    }
    render() {
        return (
            <div className="center">
                <div className="card">
                    <h1>Login</h1>
                    <form>
                        <input
                            className="form-item"
                            placeholder="Username goes here..."
                            name="username"
                            type="text"
                            onChange={this.handleInputUsername}
                        />
                        <input
                            className="form-item"
                            placeholder="Password goes here..."
                            name="password"
                            type="password"
                            onChange={this.handleInputPassword}
                        />
                        <input
                            className="form-submit"
                            value="SUBMIT"
                            type="submit"
                            onClick={this.handleFormSubmit}
                        />
                    </form>
                </div>
            </div>
        );
    }
}

export default Login;