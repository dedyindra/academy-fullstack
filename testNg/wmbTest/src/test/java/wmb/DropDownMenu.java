package wmb;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;


public class DropDownMenu {
	WebDriver driver;
	public DropDownMenu (WebDriver driver) {
		this.driver = driver;
	}
	
	public void dropDown() {
		Select drdown = new Select(driver.findElement(By.xpath("//select[@id='exampleFormControlSelect1']"))) ;
		drdown.selectByVisibleText("1");
//		drdown.selectByIndex(1);
//		drdown.selectByValue("1");
		String display = "1";
		 WebElement textField = driver.findElement(By.xpath("//select[@id='exampleFormControlSelect1']"));
		assert textField.getText().contains(display);		

		
	}

}


