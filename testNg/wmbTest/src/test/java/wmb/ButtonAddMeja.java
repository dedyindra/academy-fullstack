package wmb;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class ButtonAddMeja {
	WebDriver driver;
	public ButtonAddMeja (WebDriver driver) {
		this.driver = driver;
	}
	
	public void buttonAdd() {
		WebElement buttons = driver.findElement(By.xpath("//button[text()='Submit']"));
		buttons.click();

		WebElement textFailed = driver.findElement(By.xpath("//h2[text()='Failed']"));
		String failed = "Failed";
		assert textFailed.getText().contains(failed);

		WebElement oke = driver.findElement(By.xpath("//button[text()='OK']"));
		oke.click();
		
	}
}


