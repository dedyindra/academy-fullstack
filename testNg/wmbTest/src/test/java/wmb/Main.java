package wmb;

import org.testng.annotations.Test;


public class Main  extends BaseClass{

	@Test(priority= 0)
	public void TestTitle()throws InterruptedException{
		Title page = new Title(driver);
		driver.get("http://localhost:3000/");
		Thread.sleep(5000);
		page.titleShow();
		
	}
	@Test(priority= 1)
	public void TestJudulApp()throws InterruptedException{
		judulApp pages = new judulApp(driver);
		driver.get("http://localhost:3000/");
		Thread.sleep(8000);
		pages.titleShoWMB();
		pages.gambarMakanan();
	}
	@Test(priority= 2)
	public void testButtonTable()throws InterruptedException{
		ButtonTableClikck button = new ButtonTableClikck(driver);
		driver.get("http://localhost:3000/");
		Thread.sleep(8000);
		button.buttonTable();;
	}
	@Test(priority= 3)
	public void testButtonTablePopUp()throws InterruptedException{
		buttonTableModal button = new buttonTableModal(driver);
		driver.get("http://localhost:3000/table");
		Thread.sleep(10000);
		button.buttonTablePopUp();
	}
	@Test(priority= 4)
	public void tesFaildeAddTable()throws InterruptedException{
		ButtonAddMeja buttonTable = new ButtonAddMeja(driver);
		driver.get("http://localhost:3000/table-container");
		Thread.sleep(5000);
		buttonTable.buttonAdd();
	}
	
	@Test(priority= 5)
	public void testDropDown()throws InterruptedException{
		DropDownMenu dr = new DropDownMenu(driver);
		driver.get("http://localhost:3000/menu-container");
		//Thread.sleep(5000);
		dr.dropDown();
	}
	
	@Test(priority= 6)
	public void testDelete()throws InterruptedException{
		ButtonDeleteMenu delete = new ButtonDeleteMenu(driver);
		driver.get("http://localhost:3000/menu-container");
		Thread.sleep(5000);
		delete.deleteMenu();
	}
	@Test(priority= 7)
	public void tesUpload()throws InterruptedException{
		UploadGambar upload = new UploadGambar(driver);
		driver.get("http://localhost:3000/menu-container");
		Thread.sleep(5000);
		upload.uploadImage();
	}
	
	
}
