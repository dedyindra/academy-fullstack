package wmb;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class ButtonDeleteMenu {

	WebDriver driver;
	public ButtonDeleteMenu (WebDriver driver) {
		this.driver = driver;
	}
	
	public void deleteMenu() throws InterruptedException {
		 WebElement buttonClick = driver.findElement(By.xpath("//button[@class='btn btn-danger btn-circle']"));
		 buttonClick.click();
		 Thread.sleep(3000);
		driver.switchTo().alert().dismiss();
		buttonClick.click();
		 Thread.sleep(3000);
		driver.switchTo().alert().accept();
		
	}
	
}


