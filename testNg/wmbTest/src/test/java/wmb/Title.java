package wmb;

import static org.testng.Assert.assertEquals;

import org.openqa.selenium.WebDriver;

public class Title {
	WebDriver driver;
	public Title (WebDriver driver) {
		this.driver = driver;
	}
	
	public void titleShow() {
		String title = driver.getTitle();
		String titleWeb = "WRB";
		assertEquals(title, titleWeb);		
	}
	
}

