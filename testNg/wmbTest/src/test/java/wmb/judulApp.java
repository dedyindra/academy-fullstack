package wmb;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class judulApp {

	WebDriver driver;
	public judulApp (WebDriver driver) {
		this.driver = driver;
	}
	
	public void titleShoWMB() {
		WebElement title = driver.findElement(By.xpath("//h1[text()='Warung Makan Bahari']	"));
		if (title.isDisplayed()) {
			System.out.println("Judul Tampil");
		}
		String titleWeb = "Warung Makan Bahari";
		assert title.getText().contains(titleWeb);		
	}

	public void gambarMakanan() {
		WebElement gambar = driver.findElement(By.xpath("//img[@alt='logo']	"));
		if(gambar.isDisplayed()) {
			System.out.println("gambar Tampil");
		}		
		
	}

}

