import 'package:flutter/material.dart';
import 'package:flutter_card_member/add_card_member.dart';
import 'card_member.dart';
void main() => runApp(
  MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'something',
      initialRoute: CardMember.routeName,
      routes: {
        CardMember.routeName: (context) => CardMember(),
        AddCardMember.routeName:(context)=>AddCardMember()
      }),
);
