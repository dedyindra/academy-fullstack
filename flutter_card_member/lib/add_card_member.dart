import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_card_member/card_member.dart';
import 'package:flutter_card_member/dao/persondao.dart';
import 'package:flutter_card_member/model/person.dart';

class AddCardMember extends StatefulWidget {
  static const routeName = 'addCardMember';

  @override
  State createState() {
    return _AddCardMemberState();
  }
}

enum Gender { male, female }

class _AddCardMemberState extends State {
  Person person;
  Gender gender;

  @override
  void initState() {
    person = Person(' ', 0, Gender.male);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: new Text("Add Card Member")),
        body: Container(
          padding: EdgeInsets.all(20.0),
          child: ListView(
            children: <Widget>[
              TextField(
                decoration: InputDecoration(
                  hintText: "Nama ",
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10.0)),
                ),
                onChanged: (name) => person.name = name,
              ),
              Text(''),
              TextField(
                decoration: new InputDecoration(
                  hintText: "Age",
                  border: OutlineInputBorder(
                      borderRadius: new BorderRadius.circular(10.0)),
                ),
                onChanged: (age) => person.age = int.parse(age),
              ),
              Text(''),
              Text('Gender : '),
              RadioListTile<Gender>(
                title: const Text('male'),
                value: Gender.male,
                groupValue: gender,
                activeColor: Colors.blue,
                onChanged: (Gender value) {
                  setState(() {
                    gender = value;
                    person.gender =  value;
                  });
                },
              ),
              RadioListTile<Gender>(
                title: const Text('female'),
                value: Gender.female,
                groupValue: gender,
                activeColor: Colors.blue,
                onChanged: (Gender value) {
                  setState(() {
                    gender = value;
                    person.gender = value;
                  });
                },
              ),
              RaisedButton(
                onPressed: () {

                  PersonDao.addPerson(person) ;
                  Navigator.pop(context);
                },
                child: Text("Add member"),
                color: Colors.amber,
              ),
            ],
          ),
        ));
  }
}
