import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_card_member/add_card_member.dart';
import 'package:flutter_card_member/dao/persondao.dart';
import 'package:flutter_card_member/model/person.dart';

class CardMember extends StatefulWidget {
  static const routeName = 'carrdMember';

  @override
  State createState() {
    return _CardMemberState();
  }
}

class _CardMemberState extends State {
  @override
  Widget build(BuildContext context) {
    // addMember();
    return Scaffold(
        appBar: AppBar(title: new Text("Card Member")),
        body: Container(
          padding: EdgeInsets.all(20.0),
          child: MyCard(),
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            Navigator.pushNamed(context, AddCardMember.routeName);
          },
          tooltip: 'Increment',
          child: Icon(Icons.add),
        ));
  }
}

class  MyCard extends StatelessWidget {
   List<Person> members = PersonDao.persons ;
  @override
  Widget build(BuildContext context) {
    print(members.length);
    //TODO build ListView here
    return ListView.builder(
      itemCount: members.length,
      itemBuilder: (context, index) {
        return Container(
            padding: EdgeInsets.only(bottom: 16.0),
            child: Card(
                color: Colors.white,
                elevation: 5.0,
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15.0)),
                  child: Row(
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.all(10.0),
                        child: GestureDetector(
                          child: Container(
                            width: 100.0,
                            height: 100.0,
                            decoration: BoxDecoration(
                              color: Colors.white,
                              image: DecorationImage(
                                  image: AssetImage('asset/myaccount.png'),
                                  fit: BoxFit.cover),
                            ),
                          ),
                        ),
                      ),
                      Column(
                        children: <Widget>[
                          Text(members[index].name,
                              style: TextStyle(fontWeight: FontWeight.bold)),
                          Text(members[index].age.toString(),
                              style: TextStyle(fontWeight: FontWeight.bold)),
                          Text(describeEnum(members[index].gender) ,
                              style: TextStyle()),
                          Row(
                            children: <Widget>[
                              members[index].gender == Gender.male
                                  ? Image(
                                      image: AssetImage('asset/male.png'),
                                      width: 30,
                                      height: 30,
                                    )
                                  : Image(
                                      image: AssetImage('asset/female.png'),
                                      width: 30,
                                      height: 30)
                            ],
                          )
                        ],
                      ),
                    ],
                  ),
                ));
      },
    );
  }
}
