var lampu = method();

function method() {
    console.log("ini pemanggilan function");
}

console.log(lampu);

console.log("========= let and var ========");
//================ function

var num = 2;
anotherMethod();

function anotherMethod() {
    let num3 = num + 2;
    console.log(num3);
    methodMultiply(num3);
}

function methodMultiply(num4) {
    let result = num4 * 2;
    console.log(result);
}