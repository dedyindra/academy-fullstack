// unary ==> kalo cuma operator operannya satu
let numb = 1;
numb = ++ numb;
console.log(numb)

// binary
let numb1 = 2,
    numb2 = 1;
console.log(numb1 - numb2);
// perulangan
for (let index = 0; true; index++) {
    console.log(index);

}

let aple = "2";
let orange = "1";

console.log("3" * "3");
console.log(+ aple + + orange);
console.log(aple - orange);
console.log(13 & 5);
console.log('12' == '12');
console.log(12 === 12);

// konditional operator
function tebak(x) {
    if (x % 2 == 0) {
        console.log("genap")
    } else {
        console.log("ganjil")
    }
}
// while
i = 0
while (i < 10) {
    console.log(i)
    i ++
}

// Array
var a = ["dedy", 30, "indra"]
a[0]
// input array
a[4] = 34

// array dalam array
var a = ["dedy", 30, "indra"]
var b = ["ilham", "fatiri"]
a[1] = b
// pemenggilan array b di dalam a
a[1][0]
// hasil = ilham
// input function di array
function tebak() {
    console.log("genap")
}
a[2] = tebak

// menampung nilai terakhir di array
var a = ["dedy", 30, "indra"]
a.pop()
// 'indra'

// push
var a = ["dedy", 30, "indra"]
a.push('pushh')

// concat
var a = ["dedy", 30, "indra"]
var a = ["dedy", 30, "indra"]
a.concat(b)

// reverse
var a = ["dedy", 30, "indra"]
a.reverse()

// regex
a = 'I\'am'


// looping dan array
a = [3, 8, 9, 11]
for (let i = 0; i < a.length; i++) {
    a[i] = a[i] + 2
    console.log(a[i]);
}

// pakai foreach
a = [3, 8, 9, 11]
a.forEach(element => {
    console.log(element)
});

// Map
var a = new Map();
a.set("id", 1)
a.set("name", "dedy")

// ambil data di mat
a.get('id')


// Map punya key and value

// ambil key and value
for (let [key, values] of a) {
    console.log(key + "=" + values)
}
// ambil key
for (let key of a.keys) {
    console.log(key)
}
// ambil value
for (let values of a.values) {
    console.log(values)
}

// list punya index []
// set tidak punya index lngsung value
let b = new set()
b.add(30)
b.add("dedy")

// object

let people = {
    name: "dedy",
    age: 30,
    address: "jakarta"
}
Object.keys(people)
Object.values(people)

// ambil data
people.name
// distructur copy data
// asal nama keynya sama dengan key yang di object
let {name, age, address} = people


// pemanggilan for di object
let people = {
    name: "dedy",
    age: 30,
    address: "jakarta"
}

for (let key in people) {
    console.log(`${key}  ==  ${
        people[key]
    }`)
}

let people = {
    name: "dedy",
    age: 30,
    address: "jakarta"
}
const people5 = {
    name: "",
    age: null,
    address: ""
}
people5.name = "ganti"

people5 = {
    ... people
}

let person6 = {
    ... people,
    gender: "M"
}

const x = 8;
x = 7;

// object dalam object
let people6 = {
    name: "dedy",
    age: 30,
    address: {
        nation: "indonesia",
        city: "jakarta",
        street: "mampang"
    }
}
// ambil data
people6.address.city
var address1 = {
    ... people6.address
}
// share state
person1 = person6

// assign object
let people6 = {
    name: "dedy",
    age: 30,
    address: {
        nation: "indonesia",
        city: "jakarta",
        street: "mampang"
    }
}
let someone = Object.assign({}, people6)
someone.name = "dedy"


let people7 = {
    name: "dedy",
    age: 30,
    address: {
        nation: "indonesia",
        city: "jakarta",
        street: "mampang"
    }
}
let people6 = {
    name: "dedy",
    age: 30,
    address: {
        nation: "indonesia",
        city: "jakarta",
        street: "mampang"
    }
}

// nested spread
people6 = {
    ... people7,
    address: {
        ... people7
    }
}
// bila di ubah
people.address.city = "karawang"


Let array = ["angga", "reza", "tika"]
let [a, b, c] = array
a
array[0] = "budi anduk"
// hasilnya maka nilai value array di a tidak akan berubah


Let array1 = ["angga", "reza", "tika"]
Let array = ["angga", "reza", "tika"]
array1 = [...array]
// apabila ada perubahan
array[0] = "dedy"
// hasilnya tidak sama dengan nama di array1


// membuat object ke dalam json
let people6 = {
    name: "dedy",
    age: 30,
    address: {
        nation: "indonesia",
        city: "jakarta",
        street: "mampang"
    }
}
var jsonString = JSON.stringify(people6)
// parse json ke object
var objectStudent = JSON.parse(student)
