// //callback
// function greet(name, sebuahFunction) {
//     setTimeout(() => {
//         console.log("Hello " + name)
//         sebuahFunction()
//     }, 3000)
// }

// function greet1(name, sebuahFunction) {
//     setTimeout(() => {
//         console.log("Hello " + name)
//         sebuahFunction()
//     }, 2000)
// }

// function greet2(name, sebuahFunction) {
//     setTimeout(() => {
//         console.log("Hello " + name)
//         sebuahFunction()
//     }, 1000)
// }

// function cetakSemua() {
//     greet("ilham", () => {
//         greet1("dedy", () => {
//             greet2("Sayang", () => {})
//         })
//     });
// }
// cetakSemua();

// // factorial = 5 factorial adalah 5*4*3*2*1
// function factorial(n) {
//     if (n == 1) return 1
//     return n * factorial(n - 1)
// }
// console.log(factorial(5))


// promise
function greet(name) {

    return new Promise((resolve, reject) => {
        setTimeout(() => {
            console.log("Hello " + name)
            resolve()
        }, 3000)
    })

}

function greet1(name) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            console.log("Hello " + name)
            resolve()
        }, 1000)
    })
}

function greet2(name) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            console.log("Hello " + name)
            resolve()
        }, 500)
    })
}

// jika reject maka pakai catch bukan .then
function cetakSemua() {
    greet("dedy")
        .then(() => {
            greet1("reza")
                .then(() => {
                    greet2("ilham")
                })
        })

}
cetakSemua();

async function cetakSemua() {
    await greet("ayam")
    await greet1("kambing")
    await greet2("tika")
}