function greet() {
    console.log("hallo");
}
greet();

var arrowFunction = () => {
    console.log("hallo arrow function");
}
arrowFunction();


function greet(x) {
    return "halo" + x;
}
console.log(greet("dedy"));


function salam(name) {
    setTimeout(() => {
        console.log("hallo" + name)
    }, 3000)
}
salam("dedy")

//asyncronus
function salam1(name) {
    setTimeout(() => {
        console.log("hallo" + name)
    }, 3000)
}

function salam2(name) {
    setTimeout(() => {
        console.log("hallo" + name)
    }, 2000)
}

function salam3(name) {
    setTimeout(() => {
        console.log("hallo" + name)
    }, 500)
}

function cetaksemua() {
    salam1("dedy")
    salam2("indra")
    salam3("angga")
}
cetaksemua();



//callback
function tebak(x) {
    x()
}

function hallo() {
    console.log("hallo")
}
tebak(hallo)

//callback membawa parameter
function tebak(nama, x) {
    x(nama)
}

function hallo(nama) {
    console.log("hallo" + nama)
}
tebak("angga", hallo)
// hasilnya = hallo angga

implementasi callback menggunakan settimeout

function greet(name, sebuahfunction) {
    setTimeout(() => {
        console.log("Hello " + name)
        sebuahFunction()
    }, 3000)
}

function greet1(name, sebuahfunction) {
    setTimeout(() => {
        console.log("hallo" + name)
        sebuahfunction()
    }, 2000)
}

function greet2(name, sebuahfunction) {
    setTimeout(() => {
        console.log("hallo" + name)
        sebuahfunction()
    }, 500)
}

function printall() {
    greet("angga", () => {

    })
}
printall();