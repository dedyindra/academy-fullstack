import PackageService from "../service/package.service";
import Boom from '@hapi/boom';
const packageService = new PackageService();
const PackageRouter = [
    {
        method: 'GET',
        path: '/packages',
            handler: async (req, h) => {
                const packages = await packageService.findAllPackage();
                // let packages = {name:"dedy"};
                console.log(packages);
                if (!packages) {
                    throw  Boom.notFound('customer not found');
                } else {
                    return h.response(packages).code(200);
                }
            }
    },
    {
        method: 'GET',
        path: '/',
        handler: (req, h) => {
            return h.response({
                statusCode: 2090,
                message: 'hello word',
            }).code(200);
        }
    }
];
export default PackageRouter;