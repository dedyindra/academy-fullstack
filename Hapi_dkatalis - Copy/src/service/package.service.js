import {getRepository} from "typeorm";
import Package from "../model/package.model";
class PackageService {
    packageRepository(){
        return getRepository(Package);
    }
    async findPackageById(id){
        let packages = await this.packageRepository().findOne(id)
        if (!packages) throw { message:"data package is not found",status:404 }
        return packages;
    }
    async findAllPackage(){
        console.log("kapan throw");
        let packages = await this.packageRepository().find();
        console.log(packages);
        return packages;
    }
}
export default PackageService;