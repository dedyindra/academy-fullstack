import {createConnection as connect} from "typeorm";
import PackageSchema from "../schema/package.schema";

const createConnection = async () => {
    const connection = await connect({
        type: process.env.DB_DRIVER || "postgres",
        host: process.env.DB_HOST || "localhost",
        port: process.env.DB_PORT || 5432,
        username: process.env.DB_USERNAME || "postgres",
        password: process.env.DB_PASSWORD || "enigmacamp",
        database: process.env.DB_NAME || "flamingo",
        synchronize: process.env.DB_SYNC === "true" || true,
        logging: process.env.DB_LOGGING === "true" || true,
        entities:Object.values({PackageSchema})
    });
    return connection;
}
export default createConnection;