import {EntitySchema} from "typeorm";
import Package from "../model/package.model";

const PackageSchema = new EntitySchema({
    name :"Package",
    target:Package,
    tableName:"package",
    columns:{
        id:{
            primary:true,
            type:"uuid",
            generated:"uuid"
        },
        regNumber:{
            type: "int",
            unique:true,
            nullable:false
        },
        originLocation:{
          type:"varchar",
          nullable: false
        },
        destinationLocation:{
            type:"varchar",
            nullable:false
        },
        weight:{
            type:"int",
            nullable:false
        },
        cost:{
            type:"integer",
            nullable:false
        }
    }
})
export default PackageSchema;