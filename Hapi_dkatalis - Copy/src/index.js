import Hapi from "@hapi/hapi";
import configure from "./config";
import createConnection from './database/connection';
import routes from './routes'

process.on('unhandleRejection',(err)=>{
    console.log(err);
    process.exit(1);
});

export default async ()=>{
    configure();
    const connection = await createConnection();
    const server = Hapi.Server({
        port : process.env.APP_PORT,
        host:process.env.APP_HOST
    });

   // await server.register(require('@hapi/basic'));
    server.route(routes);
    if (connection.isConnected) {
        console.log('DATABASE CONNECTED');
        await server.start();
        console.log(`DB connection name ${connection.name}`);
        console.log('Server ', process.env.APP_NAME, ' running on ', server.info.uri)
    }
    return server.listener;
}
