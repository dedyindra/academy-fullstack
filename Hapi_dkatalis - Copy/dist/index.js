"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _hapi = _interopRequireDefault(require("@hapi/hapi"));

var _config = _interopRequireDefault(require("./config"));

process.on('unhandleRegection', function (err) {
  console.log(err);
  process.exit(1);
});

var _default =
/*#__PURE__*/
(0, _asyncToGenerator2["default"])(
/*#__PURE__*/
_regenerator["default"].mark(function _callee() {
  var server;
  return _regenerator["default"].wrap(function _callee$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          (0, _config["default"])();
          server = _hapi["default"].Server({
            port: process.env.APP_PORT,
            host: process.env.APP_HOST
          });
          _context.next = 4;
          return server.start();

        case 4:
          return _context.abrupt("return", server.listener);

        case 5:
        case "end":
          return _context.stop();
      }
    }
  }, _callee);
}));

exports["default"] = _default;