package com.enigma.dao;

import com.enigma.config.HibernateConfig;
import com.enigma.model.Student;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.util.List;

public class StudentService {

    public static void deleteStudent(int student_id){
        SessionFactory sessionFactory = HibernateConfig.getSessionFactory();
        Session session = sessionFactory.getCurrentSession();
        session.beginTransaction();
        Student student =  session.load(Student.class, new Integer(student_id));
        session.delete(student);
        session.getTransaction().commit();

    }

    public static List<Student> getAllStudent() {

        SessionFactory sessionFactory = HibernateConfig.getSessionFactory();
        Session session = sessionFactory.getCurrentSession();
        session.beginTransaction();
        List<Student> students = session.createQuery("from com.enigma.model.Student").getResultList();
        session.getTransaction().commit();
        return students;
    }


    public static void create(Student student) {
        SessionFactory sessionFactory = HibernateConfig.getSessionFactory();
        Session session = sessionFactory.getCurrentSession();
        session.beginTransaction();
        session.save(student);
        session.getTransaction().commit();
        session.close();
    }



    public  static List<Student>getById(Integer id){
        SessionFactory sessionFactory =HibernateConfig.getSessionFactory();
        Session session = sessionFactory.getCurrentSession();
        session.beginTransaction();
        List<Student>  students= session.createQuery("from com.enigma.model.Student where id ="+id+"").getResultList();
        session.getTransaction().commit();
        session.close();
        return students;
    }
    public static void update(Student student) {
        SessionFactory sessionFactory = HibernateConfig.getSessionFactory();
        Session session = sessionFactory.getCurrentSession();
        session.beginTransaction();
        session.update(student);
        session.getTransaction().commit();
        session.close();
    }



}
