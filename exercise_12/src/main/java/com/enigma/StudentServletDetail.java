package com.enigma;

import com.enigma.dao.StudentService;
import com.enigma.model.Student;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class StudentServletDetail extends HttpServlet {
    private static Logger logger = Logger.getLogger(StudentServletDetail.class.getName());

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.log(Level.INFO,"do get dipanggil");
        String id = req.getParameter("id");
        Integer hasil = Integer.parseInt(id);
        req.setAttribute("detail", StudentService.getById(hasil));
        req.getRequestDispatcher("student/student-detail.jsp").forward(req,resp);

    }
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Integer id = Integer.parseInt( req.getParameter("id"));
        String name = req.getParameter("name");
        String birth_place = req.getParameter("birth_place");
        Date birth_date = java.sql.Date.valueOf(req.getParameter("birth_date"));
        String gender = req.getParameter("gender");
        Student student = new Student(id,name,birth_place,birth_date,gender);
        StudentService.create(student);
        resp.sendRedirect("student.php");

    }

}
