<%@ page import="com.enigma.model.Student" %>
<%@ page import="java.util.List" %>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<html>
<head>
<title>enigma camp</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <a class="navbar-brand" href="index.jsp">HOME</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#menu" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="menu">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <a href="student.php" class="nav-link">Tugas CRUD</a>
      </li>
      <li class="nav-item">
        <a href="#>" class="nav-link">Latihan</a>
      </li>
    </ul>
  </div>
</nav>

<div class="container">
        <div class="card-body">
          <a href="input.php" class="btn btn-primary">Tambah data</a>
    </div>
  <table   class="table table-bordered">
          <tr >

              <th>Nama</th>
              <th>tempat lahir</th>
              <th>tanggal lahir</th>
              <th>gender</th>
                   <th>action</th>
          </tr>

<% List<Student> hasil = (List<Student>) request.getAttribute("hasil");
  for ( Student student :hasil ) { %>
               <tr >
                  <td><%= student.getName() %></td>
                  <td><%= student.getBirthPlace() %></td>
                  <td><%= student.getBirthDate() %></td>
                  <td> <%= student.getGender() %> </td>
                  <td> <a href="student-detail.php?id=<%= student.getId() %> "  class="btn btn-primary">Detail</a> </td>
              </tr>
   <%  } %>
 </table>
 </div>
</body>
</html>