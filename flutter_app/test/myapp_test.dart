import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_app/first_screen.dart';
import 'package:flutter_app/second_screen.dart';

void main(){

  testWidgets('one floating button', (WidgetTester tester)async{
    await tester.pumpWidget(MaterialApp(home: Myapp(),));
    final oneRaisednButton = find.byType(RaisedButton);
    expect(oneRaisednButton,findsNWidgets(4));
  });

//  testWidgets('one text widget', (WidgetTester tester)async{
//    await tester.pumpWidget(MaterialApp(home: Myapp(),));
//    final oneText= find.byType(Text);
//    expect(oneText,findsNWidgets(2));
//  });
//

testWidgets('description', (WidgetTester tester)async{
  await tester.pumpWidget(MaterialApp(home:Myapp()));
  await tester.enterText(find.byType(TextField), 'Apapun');
  await tester.tap(find.byKey(Key('Touch Me')));
  await tester.pump();
  expect(find.text('Apapun'), findsOneWidget);
});

}