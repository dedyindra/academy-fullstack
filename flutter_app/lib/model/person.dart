import 'package:flutter/material.dart';
class Person {
  final String title;
  final int age;
  Person(this.title, this.age);
}