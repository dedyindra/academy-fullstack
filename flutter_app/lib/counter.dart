import 'package:flutter/material.dart';

class Counter extends StatefulWidget {
  static const routeName = 'counter';

  @override
  State createState() {
    return _State();
  }
}

class _State extends State {
  int number = 0;

  add() {
    setState(() {
      number++;
    });
  }

  minus() {
    setState(() {
      if (number != 0) number--;
    });
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBar(title: new Text("Number Count")),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            RaisedButton(
              onPressed: add,
              child: Text("+"),
              color: Colors.amber,
            ),
            Text('$number'),
            RaisedButton(
              onPressed: minus,
              child: Text("-"),
              color: Colors.amber,
            )
          ],
        ),
      ),
    );
  }
}
