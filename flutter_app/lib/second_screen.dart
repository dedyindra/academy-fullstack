import 'package:flutter/material.dart';
import 'package:flutter_app/first_screen.dart';

import 'model/person.dart';

//container
class SecondScreen extends StatelessWidget {
  static const routeName = 'second';

  @override
  Widget build(BuildContext context) {
    //ini viev
    final Person person = ModalRoute.of(context).settings.arguments;
    return Container(
      color: Colors.amber,
      child: Column(
        children: <Widget>[
          Text('second screen'),
          Container(
            color: Colors.lightBlue,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[Text('dedy'),Text('tt')],
            ),
            width: 400,
            height: 200,
            padding: EdgeInsets.all(10.0),
          ),
          Row(
            children: <Widget>[
              Container(
                height: 40,
                width: 40,
                color: Colors.blue,
              )
            ],
          ),
          //ini view
          Text(person.title),
          Text(person.age.toString()),
          // diferent push and pop
          RaisedButton(
            child: Text('ke first'),
            onPressed: () {
              Navigator.pushNamed(context, Myapp.routeName);
            },
          ),
          RaisedButton(
            child: Text('ke second'),
            onPressed: () {
              Navigator.pop(context);
            },
          )
        ],
      ),
      //child: Center(child: Text('second screen'),),
    );
  }
}
