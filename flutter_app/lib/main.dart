import 'package:flutter/material.dart';
import 'package:flutter_app/card_member.dart';
import 'package:flutter_app/counter.dart';
import 'first_screen.dart';
import 'second_screen.dart';
import 'counter.dart';
import 'card_member.dart';

void main() => runApp(
      MaterialApp(
          debugShowCheckedModeBanner: false,
          title: 'something',
          initialRoute: Myapp.routeName,
          routes: {
            Myapp.routeName: (context) => Myapp(),
            SecondScreen.routeName: (context) => SecondScreen(),
            Counter.routeName: (context) => Counter(),
            CardMember.routeName: (context) => CardMember()
          }),
    );
