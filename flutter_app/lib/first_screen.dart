import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:flutter_app/card_member.dart';
import 'package:flutter_app/counter.dart';
import 'package:flutter_app/model/person.dart';
import 'package:flutter_app/second_screen.dart';

class Myapp extends StatefulWidget{
 static const routeName = 'first';
  @override
  State createState() {
    return MyAppState();
  }
}

class MyAppState extends State {
  String name = '';
  var response;
  String result ='';
  String buttonTouchMe = 'Touch Me';
  String buttonPressMe = 'Press Me';
  String buttonPushMe = 'Push Me';

  fetchData()async {
    response = await   http.get('http://10.0.2.2:9090/list-category');

    setState(() {
      result = response.body;
    });
  }
  fetchDataPost()async {
    response = await http.post('https://jsonplaceholder.typicode.com/posts/2',body:'{"name":"dedy"}');
    setState(() {
      result = response.body;
    });
  }
  kepencet(nameInput) {
    setState(() {
      name = null;
    });
    print(name);
  }
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return  Scaffold(
        appBar: AppBar(
          title: Text('whatsapp gaees'),elevation: 30,
        ),
        body:
        Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            Text('hallo $name'),
            TextField(key: Key('Touch Me'),onChanged: (text){ name =text;},),
            RaisedButton(
              key: Key('touchMe'),
              //peunulisan arrow function lainya tanpa =>
              onPressed: (){fetchData();},
              child: Text(buttonTouchMe),
              color: Colors.blueAccent,
            ),
            Text(result.toString()),
            RaisedButton(
              onPressed: (){Navigator.pushNamed (context,CardMember.routeName);},
              child: Text("Card member"),
              color: Colors.lightBlue,
            ),

            RaisedButton(
              onPressed: (){Navigator.pushNamed (context,Counter.routeName);},
              child: Text("To counter"),
              color: Colors.amber,
            ),
            RaisedButton(
           //   onPressed: (){Navigator.push(context,MaterialPageRoute(builder:(context)=> SecondScreen()));},
                onPressed: (){Navigator.pushNamed (context,SecondScreen.routeName,arguments: Person(
                  'Tony Blank',
                  30,
                ),);},

              child: Text("lets'go "),
              color: Colors.amber,
            ),
          ],
        ));

  }
}


//class Mycontainer extends StatelessWidget{
//  final String title;
//
//  Mycontainer({this.title});
//  @override
//  Widget build(BuildContext context) {
//    return  Container(
//      color: Colors.blueAccent,
//      height: 50,
//      width: 200,
//      child: Text(title),
//    );
//  }
//}
