package com.enigma.model;
import java.io.*;
import java.util.*;

public class PeopleGenerator {
    List<String> nama = new ArrayList<>();
    List<Integer> umur = new ArrayList<>();
    List<String> gender = new ArrayList<>();
private String path;
    public PeopleGenerator(String path) {
        this.path=path;
    }

    public void ReadFile(){
        File file = new File(this.path);
        List<String> texts = new ArrayList<>();
        try {
            FileReader fileReader = new FileReader(file);
            BufferedReader br = new BufferedReader(fileReader);
            while (true){
                String txt =br.readLine();
                if (txt==null){
                    break;
                }
                texts.add(txt);
            }

            for (int i = 0; i < texts.size() ; i++) {
                nama.add(texts.get(i).substring(0,30).trim()) ;
                umur.add(Integer.valueOf(texts.get(i).substring(30,33).trim()));
                gender.add(texts.get(i).substring(33,34).trim());
              //  System.out.print( "["+nama.get(i) +"]");
            }
            System.out.print("Jumlah teman adalah = ");
            System.out.println(texts.size());
        }catch (FileNotFoundException e){
            e.printStackTrace();
        }catch (IOException e){
            e.printStackTrace();
        }

    }
    public void getFriend(){
        File file = new File(this.path);
        List<String> texts = new ArrayList<>();
        try {
            FileReader fileReader = new FileReader(file);
            BufferedReader br = new BufferedReader(fileReader);
            while (true){
                String txt =br.readLine();
                if (txt==null){
                    break;
                }
                texts.add(txt);
            }
            int male = 0;
            int female = 0;

            for (int i = 0; i < texts.size() ; i++) {
                gender.add(texts.get(i).substring(33,34).trim());
                if (gender.get(i).equals("M")){
                    male++;
                }
                else {
                    female++;
                }

            }
            System.out.println("Jumlah teman pria : "+ male);
            System.out.println("Jumlah teman wanita : " + female);
        }catch (FileNotFoundException e){
            e.printStackTrace();
        }catch (IOException e){
            e.printStackTrace();
        }

    }

    public void getAverageAge(){
        Integer total=0;
        for (int i = 0; i <umur.size() ; i++) {
            total+=umur.get(i);
        }
        Integer average=total/umur.size();
        System.out.println("Rata Rata Umur adalah :"+average);
    }

//
//        public  int getMaxValue(){
//            int[] numbers = new int[] {1,2,4,6,7,4,87};
//            int maxValue = numbers[0];
//            for(int i=1;i < numbers.length;i++){
//                if(numbers[i] > maxValue){
//                    maxValue = numbers[i];
//                }
//            }
//            return maxValue;
//        }
//    public  int getMinValue(){
//        int[] numbers = new int[] {1,2,4,6,7,4,87};
//        int minValue = numbers[0];
//        for(int i=1;i<numbers.length;i++){
//            if(numbers[i] < minValue){
//                minValue = numbers[i];
//            }
//        }
//        return minValue;
//    }

    public void getUmurTer(){
        Object min=Collections.min(umur);
        System.out.println("Umur Termuda adalah  = "+min);
        Object max=Collections.max(umur);
        System.out.println("Umur Tertua adalah = "+max);
    }


    public String print() {
        return "PeopleGenerator{" +
                "nama=" + nama +
                ", umur=" + umur +
                ", gender=" + gender +
                '}';
    }
}
