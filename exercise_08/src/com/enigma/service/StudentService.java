package com.enigma.service;

import com.enigma.connection.DBConnection;
import com.enigma.model.Student;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class StudentService {
    static ResultSet  set;
    public static void create(Student student){
        Connection connection = DBConnection.letsConnect();
        try {
            Statement statement = connection.createStatement();
            String query = "insert into student(id,name,birth_place,birth_date,gender,major_id) values ("+student.getId()
                    +",'"+student.getName()
                    +"','"+student.getBirthPlace()
                    +"','"+student.getBirthDate()
                    +"','"+student.getGender()
                    +"',"+student.getMajor()+")";
            statement.execute(query);

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
    public static List<Student> getAll(){
        Connection connection = DBConnection.letsConnect();
        List<Student> get = new ArrayList<>();
        try {
        Statement statement = connection.createStatement();
        ResultSet set = statement.executeQuery("select * from student");
            while (set.next()){
                get.add( new Student(set.getInt("id"),
                        set.getString("name"),
                        set.getString("birth_place"),
                        set.getDate("birth_date"),
                        set.getString("gender"),
                        set.getInt("major_id")));
            }
            // Student student = new Student();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return get ;
    }

      public static  List<Student> search(String keyword) {
        Connection connection = DBConnection.letsConnect();
        List<Student> get = new ArrayList<>();
        try {
            Statement statement = connection.createStatement();
            ResultSet set = statement.executeQuery("select * from student where name like '%"+keyword+"%' ");
            while (set.next()){
                get.add( new Student(set.getInt("id"),
                        set.getString("name"),
                        set.getString("birth_place"),
                        set.getDate("birth_date"),
                        set.getString("gender"),
                        set.getInt("major_id")));
            }
            // Student student = new Student();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return get ;
    }


//     public static  List<Student> search(String keyword){
//        }
//        public static   List<Student> getAll(String keyword){
//        }

}
