import java.sql.*;

public class Main2 {
    public static void main(String[] args) {
        try {
            Connection connection = DriverManager.getConnection("jdbc:postgresql://127.0.0.1:5432/school","postgres","enigmacamp");
            Statement statement = connection.createStatement();
            // untuk insert data
            Integer id = 6;
            String name = "computer network";
            Integer sks = 3;
            String query = "insert into subject(id_subject,subject_name,sks) values ("+id+",'"+name+"',"+sks+")";
            statement.execute(query);

            // untuk select data
            ResultSet set = statement.executeQuery("select * from student");
            for (int i = 0; true; i++) {
                if (set.next()){
                    System.out.println(set.getString("name")+","+set.getString("gender"));
                }else {
                    break;
                }
            }
            set = statement.executeQuery("select * from subject");
            for (int i = 0; true; i++) {
                if (set.next()){
                    System.out.println(set.getString("subject_name"));
                }else {
                    break;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
