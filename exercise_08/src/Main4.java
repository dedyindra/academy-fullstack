import com.enigma.model.Student;
import com.enigma.service.StudentService;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class Main4 {
    public static void main(String[] args) throws Exception {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("Pencarian student nama :  ");
        String keyword = br.readLine() ;
        //StudentService.search(keyword);
        List<Student> search = StudentService.search(keyword);
        for (Student student :search) {
            System.out.println(student.toString());
        }

    }
}






