import 'package:flutter/material.dart';
import 'package:qrscan/qrscan.dart'as scanner;
void main() => runApp(MyHomePage());

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  String data = '';
  scan ()async{
    String cameraScanResult = await scanner.scan();
    setState(() {
      data = cameraScanResult;
    });
    return cameraScanResult;
  }
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: Container(
          child: Column(
            children: <Widget>[
              RaisedButton(
                child: Text('scan'),
                onPressed: scan,
              ),
              Text(data)
            ],
          ),
        ),

      ),
    );
  }
}
