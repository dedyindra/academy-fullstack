import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:bloc/bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_bloc_listbuilder/product_card.dart';
void main() => runApp(MyApp());


class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(home: BlocProvider(
        builder: (context)=>ProductBloc(),
        child: MainPage()));
  }
}

class MainPage extends StatelessWidget {
final Random r = Random();
  @override
  Widget build(BuildContext context) {
    ProductBloc bloc = BlocProvider.of<ProductBloc>(context);

    return Scaffold(
      appBar: AppBar(title: Text('demo lis bloc'),),
      body: Column(
        children: <Widget>[
          RaisedButton(
            child: Text('create produc',style: TextStyle(color: Colors.white),),
            onPressed: (){
              bloc.dispatch(r.nextInt(4) + 2);
            },
          ),
          SizedBox(height: 20,),
          BlocBuilder<ProductBloc,List<Product>>(
            builder: (context,products) => Expanded(
              child: ListView.builder(scrollDirection: Axis.horizontal,itemCount: products.length,itemBuilder: (context,index){
                return Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    (index == 0 ) ? SizedBox(width: 20,): Container(),
                    ProducCard(
                      name: products[index].name,
                      price: products[index].price.toString(),
                      onIncTap: (){},
                      onDecTap: (){},
                      onAddCart: (){},
                    ),
                    SizedBox(width: 20,)
                  ],
                );
              },),
            ),
          ),
        ],
      ),
    );
  }
}
class Product{
  String name;
  int price;

  Product({this.name = "", this.price = 0});

}

class ProductBloc extends Bloc<int,List<Product>>{
  @override
  // TODO: implement initialState
  List<Product> get initialState => [];

  @override
  Stream<List<Product>> mapEventToState(int event) async* {
    // TODO: implement mapEventToState
   List<Product> products = [];
   for(int i = 0; i < event ; i ++){
     products.add(Product(name: "Produk" + i.toString(),price: (i + 1) * 5000));
   }
   yield products;
  }

}