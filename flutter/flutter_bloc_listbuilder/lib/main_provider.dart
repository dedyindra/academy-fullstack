import 'package:flutter/material.dart';
import 'package:flutter_bloc_listbuilder/product_card.dart';
import 'package:provider/provider.dart';
void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          title: Text('produc card'),
          backgroundColor: firstColor,
        ),
        body: ChangeNotifierProvider<ProducState>(
          builder: (context)=>ProducState(),
          child: Container(
            margin: EdgeInsets.all(20),
            child: Align(
              alignment: Alignment.topCenter,
              child: Consumer<ProducState>(
                builder:(context,productState,_)=> ProducCard(
                  name: "buah-buahan",
                  price: ' rp. 25000',
                  onAddCart: (){},
                  quantity: productState.quantity,
                  notification: (productState.quantity >5 ) ? "diskon 10%" : null,
                  onDecTap: (){productState.quantity--;},
                  onIncTap: (){productState.quantity++;},
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class ProducState with ChangeNotifier{
  int _quantity = 0;
  int get quantity => _quantity;
  set quantity(int newValue){
    _quantity = newValue;
    notifyListeners();
  }
}