import 'package:chat_app_learn/chat_messege.dart';
import 'package:flutter/material.dart';

class ChatScreen extends StatefulWidget {
  @override
  _ChatScreenState createState() => _ChatScreenState();
}

class _ChatScreenState extends State<ChatScreen> {
  final TextEditingController _textController = new TextEditingController();
final List<ChatMessege> _messeges = <ChatMessege>[];

  void _handleSubmitted(String text) {
    _textController.clear();
    ChatMessege messege = new ChatMessege(
      text: text,
    );
    setState(() {
      _messeges.insert(0, messege);
    });
    scrollController.animateTo(double.infinity,curve: Curves.ease,duration: Duration(milliseconds:500 ));
  }

  Widget _textComposerWidget() {
    return IconTheme(
      data: new IconThemeData(
        color: Colors.blue
      ),
      child: new Container(
        margin: const EdgeInsets.symmetric(horizontal: 8.0),
        child: new Row(
          children: <Widget>[
            new Flexible(
              child: new TextField(
                decoration:
                    new InputDecoration.collapsed(hintText: "send a message"),
                controller: _textController,
                onSubmitted: _handleSubmitted,
              ),
            ),
            new Container(
              margin: const EdgeInsets.symmetric(horizontal: 4.0),
              child: new IconButton(
                icon: new Icon(Icons.send),
                onPressed: ()=>_textController.text.isEmpty ? null:  _handleSubmitted(_textController.text)
              ),
            )
          ],
        ),
      ),
    );
  }

  ScrollController scrollController;
  @override
  void initState() {
    scrollController = ScrollController();
  }

  @override
  Widget build(BuildContext context) {
    return new Column(
      children: <Widget>[
        new Flexible(child: new ListView.builder(
          controller: scrollController,
          padding: new EdgeInsets.all(8.0),
          reverse: true,
          itemBuilder: (_,int index)=>_messeges[index],
          itemCount: _messeges.length,
        )),
        new Divider(height: 1.0,),
        new Container(
          decoration: new BoxDecoration(
            color: Theme.of(context).cardColor,
          ),
          child: _textComposerWidget(),
        )

      ],
    );
  }
}
