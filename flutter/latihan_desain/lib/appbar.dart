import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class AppBars extends StatelessWidget {
  static const routeName = 'appBar';

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: CustomScrollView(
          slivers: <Widget>[
          SliverAppBar(
          expandedHeight: 155.0,
          leading: Icon(Icons.menu),
          elevation: 20,
          floating: true,
          pinned: true,
          //snap : true
          flexibleSpace: FlexibleSpaceBar(
            title: Text('Ap'),
//            stretchModes: [
//              StretchMode.zoomBackground,
//              StretchMode.blurBackground,
//              StretchMode.fadeTitle
//            ],
//            centerTitle: true,
//            collapseMode: CollapseMode.pin,
//            background: Container(
//              padding: EdgeInsets.all(10),
//                decoration: BoxDecoration(boxShadow: [BoxShadow(
//                    color: Colors.black, blurRadius: 20)
//                ], color: Colors.white, borderRadius: BorderRadius.circular(10)),
//                margin: EdgeInsets.only(left: 50, right: 50, top: 75, bottom:
//                20.0),
//            width: 100,
//            height: 100,
//            child: Text("Dedy Mahendra Desta"),
//          ),
        ),
        actions: <Widget>[
          IconButton(
            icon: const Icon(Icons.add_circle),
            tooltip: 'Add new entry',
            onPressed: () {
              /* ... */
            },
          ),
        ],
      ),
      SliverList(
          delegate: SliverChildListDelegate([
            ListTile(
                onTap: () {
                  print("onTap");
                },
                leading: Icon(Icons.volume_off), title: Text("Volume Off")),
            ListTile(
                leading: Icon(Icons.volume_off), title: Text("Volume Off")),
            ListTile(
                leading: Icon(Icons.volume_off), title: Text("Volume Off")),
            ListTile(
                leading: Icon(Icons.volume_off), title: Text("Volume Off")),
            ListTile(
                leading: Icon(Icons.volume_off), title: Text("Volume Off")),
            ListTile(
                leading: Icon(Icons.volume_off), title: Text("Volume Off")),
            ListTile(
                leading: Icon(Icons.volume_off), title: Text("Volume Off")),
            ListTile(
                leading: Icon(Icons.volume_off), title: Text("Volume Off")),
            ListTile(
                leading: Icon(Icons.volume_off), title: Text("Volume Off")),
            ListTile(
                leading: Icon(Icons.volume_off), title: Text("Volume Off")),
            ListTile(
                leading: Icon(Icons.volume_off), title: Text("Volume Off")),
            ListTile(
                leading: Icon(Icons.volume_off), title: Text("Volume Off")),
            ListTile(
                leading: Icon(Icons.volume_down), title: Text("Volume Down")),
            ListTile(
                leading: Icon(Icons.volume_mute), title: Text("Volume Mute")),
          ]))
      ],
    ),)
    ,
    );
  }
}
