import 'package:flutter/material.dart';

class Beranda extends StatefulWidget {
  static const routeName = 'latihan2';

  @override
  _BerandaState createState() => _BerandaState();
}

final SnackBar snackBar = const SnackBar(content: Text('Showing Snackbar'));

class _BerandaState extends State<Beranda> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
//      AppBar(
//        title: const Text('App Bar'),
//        elevation: 0,
//        actions: <Widget>[
//          IconButton(
//            icon: const Icon(Icons.add_alert),
//            tooltip: 'Show Snakbar',
//            onPressed: (){
//    scaffoldKey.currentState.showSnackBar(snackBar);
//            },
//          ),
//          IconButton(icon :const Icon(Icons.navigate_next),
//          tooltip: 'next page',
//          onPressed: (){ openPage(context);})
//        ],
//      ),
      home: Scaffold(
        body: CustomScrollView(
          slivers: <Widget>[
            SliverAppBar(
              expandedHeight: 200.0,
              leading: IconButton(
                  icon: Icon(Icons.menu),
                tooltip: 'Add new ',
                onPressed: (){},
              ),
              //floating: true,
              pinned: true,
              //snap : true
              flexibleSpace: const FlexibleSpaceBar(
                title: Text('Available seats'),
              ),
              actions: <Widget>[
                IconButton(
                  icon: const Icon(Icons.add_circle),
                  tooltip: 'Add new entry',
                  onPressed: () {
openPage(context);
                  },
                ),
              ],
            ),
            SliverList(delegate: SliverChildListDelegate([
              ListTile(leading: Icon(Icons.volume_off), title: Text("Volume Off")),
              ListTile(leading: Icon(Icons.volume_off), title: Text("Volume Off")),
              ListTile(leading: Icon(Icons.volume_off), title: Text("Volume Off")),
              ListTile(leading: Icon(Icons.volume_off), title: Text("Volume Off")),
              ListTile(leading: Icon(Icons.volume_off), title: Text("Volume Off")),
              ListTile(leading: Icon(Icons.volume_off), title: Text("Volume Off")),
              ListTile(leading: Icon(Icons.volume_off), title: Text("Volume Off")),
              ListTile(leading: Icon(Icons.volume_off), title: Text("Volume Off")),
              ListTile(leading: Icon(Icons.volume_off), title: Text("Volume Off")),
              ListTile(leading: Icon(Icons.volume_off), title: Text("Volume Off")),
              ListTile(leading: Icon(Icons.volume_off), title: Text("Volume Off")),
              ListTile(leading: Icon(Icons.volume_off), title: Text("Volume Off")),

              ListTile(leading: Icon(Icons.volume_down), title: Text("Volume Down")),
              ListTile(leading: Icon(Icons.volume_mute), title: Text("Volume Mute")),
            ]))

          ],
        ),
      )
    );
  }
}

void openPage(BuildContext context) {
  Navigator.push(context, MaterialPageRoute(
    builder: (BuildContext context) {
      return Scaffold(
        appBar: AppBar(
          title: const Text('Next page'),
        ),
        body: const Center(
          child: Text(
            'This is the next page',
            style: TextStyle(fontSize: 24),
          ),
        ),
      );
    },
  ));
}
