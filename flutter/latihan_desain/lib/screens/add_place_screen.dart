import 'package:flutter/material.dart';

class AddPlaceScrees extends StatefulWidget {
  @override
  _AddPlaceScreesState createState() => _AddPlaceScreesState();
}

class _AddPlaceScreesState extends State<AddPlaceScrees> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('add place'),
      ),
      body: Column(
        children: <Widget>[
          RaisedButton.icon(
              onPressed: () {}, icon: Icon(Icons.add), label: Text("add place"))
        ],
      ),
    );
  }
}
