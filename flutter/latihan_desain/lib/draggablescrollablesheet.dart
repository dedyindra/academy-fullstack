import 'package:flutter/material.dart';

class DraggableScrollable extends StatefulWidget {
  static const routeName = 'dragable';

  @override
  _DraggableScrollableState createState() => _DraggableScrollableState();
}

class _DraggableScrollableState extends State<DraggableScrollable> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(

        title: Text("Draggable Scrool"),
      ),
      body: Container(
        child: DraggableScrollableSheet(
          initialChildSize: 0.1,
          minChildSize: 0.1,
          maxChildSize: 1.0,
          builder: (BuildContext context , myscrollController){
            return Container(
              color: Colors.blueAccent,
              child: ListView(
                 controller: myscrollController,
                children: <Widget>[


                ],
             ),
            );
          },
        ),


      ),
    );
  }
}
