import 'package:flutter/material.dart';
import 'package:circle_wheel_scroll/circle_wheel_scroll_view.dart';
import 'package:flutter/widgets.dart';

class Scroll extends StatelessWidget {
  static const routeName = 'scroll';

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.red,
      ),
      home: WheelExample(),
    );
  }
}

class WheelExample extends StatelessWidget {
  Widget _buildItem(int i) {
    return Center(
      child: ClipRRect(
        borderRadius: BorderRadius.circular(40),
        child: Container(
          width: 80,
          padding: EdgeInsets.all(20),
          color: Colors.blue[100 * ((i % 8) + 1)],
          child: Center(
            child: Text(
              i.toString(),
            ),
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Wheel'),
      ),
      body: SafeArea(
        child: Center(
          child: Container(
            //  height: 260,
            // width: 160,
            // transform: Transform.rotate(angle: 1/5,),
            child: CircleListScrollView(
              physics: CircleFixedExtentScrollPhysics(),
              axis: Axis.horizontal,
              clipToSize: true,
             // renderChildrenOutsideViewport: true,
              controller: ScrollController(),
              itemExtent: 80,
              children: List.generate(20, _buildItem),
              radius: MediaQuery.of(context).size.flipped.width * 0.5,
            ),
          ),
        ),
      ),
    );
  }
}
