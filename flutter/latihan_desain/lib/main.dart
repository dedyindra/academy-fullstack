
import 'package:flutter/material.dart';
import 'package:latihan_desain/appbar.dart';
import 'package:latihan_desain/beranda.dart';
import 'package:latihan_desain/draggablescrollablesheet.dart';
import 'package:latihan_desain/myprofile.dart';
import 'package:latihan_desain/scrollviewinteraktif.dart';
import 'package:latihan_desain/sidebar.dart';

void main() => runApp(
  MaterialApp(
    debugShowCheckedModeBanner : false,
    title : 'Artis Management',
    initialRoute : Scroll.routeName ,
    onGenerateRoute : (RouteSettings settings) {
      var routes = <String, WidgetBuilder>{
      Beranda.routeName : (context) => Beranda(),
       AppBars.routeName : (context) => AppBars(),
        SideBar.routeName : (context)=> SideBar(),
        Scroll.routeName : (context)=> Scroll(),
        DraggableScrollable.routeName:(context) =>DraggableScrollable(),
        MyProfile.routName:(context)=> MyProfile()
      };
      WidgetBuilder widgetBuilder = routes[settings.name];
      return MaterialPageRoute(builder : (context) => widgetBuilder(context));
      },
  ),
);
