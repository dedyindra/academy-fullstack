import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_qrqode/home_page.dart';
import 'package:flutter_qrqode/service/profile_service.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';

class ProfileId extends StatefulWidget {
  static const routName = 'profile';
  final String code;
  FetchProfile fetchProfile;

  ProfileId(this.fetchProfile, this.code);

  @override
  _ProfileIdState createState() => _ProfileIdState(fetchProfile, code);
}

class _ProfileIdState extends State<ProfileId> {
  FetchProfile fetchProfile;
  String idProfile;
  var profilData;

  _ProfileIdState(this.fetchProfile, this.idProfile);


  getDataArtistById() async {
    var response = await fetchProfile.getDataProfile(idProfile);
    setState(() {
      profilData = jsonDecode(response.body);
    });
    print(profilData.toString() + 'in list');
  }

//  Timer timer = new Timer(new Duration(seconds: 5), () {
//    debugPrint("Print after 5 seconds");
//  });
  @override
  void initState() {
    getDataArtistById();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: profilData == null
          ? Container(
              child: Center(
                child: CircularProgressIndicator(),
              ),
            )
          : profilData["name"] == null ? Center(child: Text("data tidak ada")) :Center(
              child: Container(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      child: ClipRRect(
                        borderRadius: new BorderRadius.circular(8.0),
                        child: Image.network(
                          '${profilData['image']}',
                          fit: BoxFit.cover,
                          height: 300,
                          width: 300,
                        ),
                      ),
                    ),
                    Text(
                      'Name : ${profilData['name']}',
                      style: TextStyle(fontSize: 20),
                    ),
                    SizedBox(height: 20,),
                    Text(
                      'gender : ${profilData['gender']}',
                      style: TextStyle(fontSize: 20),
                    ),
                    RaisedButton(
                      color: Colors.amber,
                      child: Text('Submit'),
                      onPressed: () {
                        Navigator.pushNamed(context, HomePage.routeName);
                      },
                    )
                  ],
                ),
              ),
            ),
    );
  }
  @override
  void dispose() {
    super.dispose();
  }
}
