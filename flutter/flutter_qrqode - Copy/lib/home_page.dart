import 'package:flutter/material.dart';
import 'package:barcode_scan/barcode_scan.dart';
import 'package:flutter/services.dart';
import 'package:flutter_qrqode/generate_qr.dart';
import 'package:flutter_qrqode/scanner.dart';
import 'package:flutter_qrqode/profile_id.dart';
import 'dart:io';
import 'package:image_picker/image_picker.dart';
import 'package:qr_flutter/qr_flutter.dart';

class HomePage extends StatefulWidget {
  static const routeName = "homepage";

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  String data = "qrcode";


  scanbarcode() async {
    // imageSelectorGallery();
    try {
      await BarcodeScanner.scan().then((String id) {
        Navigator.pushNamed(context, ProfileId.routName, arguments: id);
      });
    } on PlatformException catch (e) {
      if (e.code == BarcodeScanner.CameraAccessDenied) {
        setState(() {
          this.data = 'camera need permission';
        });
      } else {
        setState(() {
          this.data = 'unkown erro $e';
        });
      }
    } on FormatException {
      setState(() {
        this.data =
            'null (User returned using the "back"-button before scanning anything. Result)';
      });
    } catch (e) {
      setState(() {
        this.data = 'unknow error: $e';
      });
    }
  }

  showDialogs(BuildContext context) {
    showDialog(builder: (BuildContext context) {
      return AlertDialog(
        title: Text('not scann'),
        actions: <Widget>[
          FlatButton(
            child: Text('back'),
            onPressed: () {
              Navigator.pop(context);
            },
          )
        ],
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            RaisedButton(
              onPressed: () {
                Navigator.pushNamed(context, GenerateQR.routeName);
              },
              child: Text("profile"),
            ),
            FlatButton(
              onPressed: (){
             //   scanbarcode();
                Navigator.pushNamed(context, Scanner.routeName);
              },
              color: Colors.blueAccent,
              child: Text('scanner'),
            ),
            Text(data)
          ],
        ),
      ),
    );
  }
}
