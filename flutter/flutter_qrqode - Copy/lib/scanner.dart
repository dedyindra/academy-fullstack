import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_qrqode/profile_id.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';
import 'package:qr_code_scanner/qr_scanner_overlay_shape.dart';

Color flash_on = Colors.blue;
Color flash_off = Colors.black;
Color front_camera = Colors.blue;
Color back_camera = Colors.black;

class Scanner extends StatefulWidget {
  static const routeName = 'scanner';

  @override
  _ScannerState createState() => new _ScannerState();
}

class _ScannerState extends State<Scanner> {
  var flashState = flash_off;
  var cameraState = back_camera;
  String data = "";
  QRViewController controller;
  final GlobalKey qrKey = GlobalKey(debugLabel: 'QR');

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: <Widget>[
          Expanded(
            child: QRView(
              key: qrKey,
              onQRViewCreated: _onQRViewCreated,
              overlay: QrScannerOverlayShape(
                borderColor: Colors.green,
                borderRadius: 8,
                borderLength: 25,
                borderWidth: 6,
                cutOutSize: 200,
              ),
            ),
            flex: 8,
          ),
          Expanded(
            child: FittedBox(
              fit: BoxFit.contain,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.all(8.0),
                        child: IconButton(
                          icon: Icon(Icons.arrow_back_ios),
                          color: Colors.blue,
                          onPressed: () {
                            Navigator.pop(context);
                          },
                        ),
                      ),
                      Container(
                          margin: EdgeInsets.all(8.0),
                          child: IconButton(
                            icon: Icon(Icons.flash_on),
                            color: flashState,
                            onPressed: () {
                              if (controller != null) {
                                controller.toggleFlash();
                                if (_isFlashOn(flashState)) {
                                  setState(() {
                                    flashState = flash_off;
                                  });
                                } else {
                                  setState(() {
                                    flashState = flash_on;
                                  });
                                }
                              }
                            },
                          )),
                      Container(
                        margin: EdgeInsets.all(8.0),
                        child: IconButton(
                          icon: Icon(Icons.camera),
                          color: cameraState,
                          onPressed: () {
                            if (controller != null) {
                              controller.flipCamera();
                              if (_isBackCamera(cameraState)) {
                                setState(() {
                                  cameraState = front_camera;
                                });
                              } else {
                                setState(() {
                                  cameraState = back_camera;
                                });
                              }
                            }
                          },
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            flex: 1,
          )
        ],
      ),
    );
  }

  _isFlashOn(Color current) {
    return flash_on == current;
  }

  _isBackCamera(Color current) {
    return back_camera == current;
  }

  void _onQRViewCreated(QRViewController controller) {
    this.controller = controller;
    try {
      controller.scannedDataStream.first.then((scanData) {
        if(scanData !=null){
          Navigator.pushNamed(context, ProfileId.routName, arguments:scanData);
        }
      });
    } catch (e) {
      print(e);
    }
  
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }
}
