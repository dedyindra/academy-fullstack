import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_qrqode/service/profile_service.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'dart:convert';

class GenerateQR extends StatefulWidget {
  static const routeName = "generate_qr";
  final String id;
  FetchProfile fetchProfile;

  GenerateQR(this.fetchProfile, this.id);

  @override
  _GenerateQRState createState() => _GenerateQRState(fetchProfile, id);
}

class _GenerateQRState extends State<GenerateQR> {
  GlobalKey globalKey = new GlobalKey();
  FetchProfile fetchProfile;
  String id;
  var profilId;

  _GenerateQRState(this.fetchProfile, this.id);

  getDataArtistById() async {
    var response = await fetchProfile.getProfile();
    setState(() {
      profilId = jsonDecode(response.body);
    });
  }

  @override
  void initState() {
    getDataArtistById();
//    print(profilId.toString() + 'oin list');
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Qr Generatot'),
      ),
      body: profilId == null
          ? Container(
              child: Center(
                child: CircularProgressIndicator(),
              ),
            )
          :SizedBox.expand(
        child:Stack(
          children: <Widget>[
            Align(
              alignment: Alignment.center,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Container(
                    child: ClipRRect(
                      borderRadius: new BorderRadius.circular(8.0),
                      child: Image.network(
                        '${profilId['image']}',
                        fit: BoxFit.cover,
                        height: 300,
                        width: 300,
                      ),
                    ),
                  ),
                  Text(
                    'Name : ${profilId['name']}',
                    style: TextStyle(fontSize: 20),
                  ),
                ],
              ),
            ),
            SizedBox.expand(
              child: DraggableScrollableSheet(
                initialChildSize: 0.1,
                minChildSize: 0.1,
                maxChildSize: 1.0,
                builder: (BuildContext context , myscrollController){
                  return Container(
                    padding: EdgeInsets.symmetric(
                      horizontal: 15,
                      vertical: 10
                    ),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(20.0),
                        topRight: Radius.circular(20.0)
                      ),
                      boxShadow: [BoxShadow(
                        color: Colors.grey,
                        blurRadius: 10.0
                      )]
                    ),
                    child: ListView(
                      controller: myscrollController,
                      children: <Widget>[
                        Center(
                          child: Container(
                            height: 8,
                            width: 50,
                            decoration: BoxDecoration(
                              color: Colors.grey,
                              borderRadius: BorderRadius.circular(5),
                            ),
                          ),
                        ),
                        SizedBox(height: 20,),
                        Center(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: <Widget>[
                              RepaintBoundary(
                                key: globalKey,
                                child: QrImage(
                                  version: 6,
                                  backgroundColor: Colors.white70,
                                  foregroundColor: Colors.black,
                                  errorCorrectionLevel: QrErrorCorrectLevel.M,
                                  padding: EdgeInsets.all(30),
                                  // embeddedImage: AssetImage('assets/images/logo.png'),
                                  // embeddedImageStyle : QrEmbeddedImageStyle(
                                  // size: Size.square(30),
                                  // color: Color.fromARGB(100, 10, 10, 10)
                                  // ),
                                  size: 300,
                                  data: "${profilId['id']}",
                                ),
                              ),
                              Text("nama : ${profilId['name']}"),
                              Text("gender : ${profilId['gender']}")
                            ],
                          ),
                        )

                      ],
                    ),
                  );
                },
              ),
            )
          ],
        ),
      ) ,
    );
  }
}
