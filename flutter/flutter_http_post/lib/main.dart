import 'package:flutter/material.dart';
import 'package:flutter_http_post/post_user_model.dart';

void main() => runApp(MyAppState());

class MyAppState extends StatefulWidget {
  @override
  _MyAppStateState createState() => _MyAppStateState();
}

class _MyAppStateState extends State<MyAppState> {
  String output = 'nodata';
  PostResult postResult = null;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text("post data"),
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Text((postResult != null)
                  ? postResult.id + "|" + postResult.name
                  : "Tidak ada data"),
              RaisedButton(
                onPressed: () {
                  PostResult.connect("dedy", "Dokter").then((value) {
                    postResult = value;
                    setState(() {});
                  });
                },
                child: Text('Post'),
              )
            ],
          ),
        ),
      ),
    );
  }
}
