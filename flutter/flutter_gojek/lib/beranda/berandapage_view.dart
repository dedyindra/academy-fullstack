import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_gojek/constant.dart';

class BerandaPage  extends StatefulWidget {
  @override
  _BerandaPageState  createState() => new _BerandaPageState ();
}
GlobalKey<ScaffoldState> scaffoldState = GlobalKey();

class _BerandaPageState  extends State<BerandaPage> {
  @override
  Widget build(BuildContext context) {
    var mediaQuery = MediaQuery.of(context);

    return new SafeArea(
      child: new Scaffold(
        key: scaffoldState,
        body: new Stack(
          children: <Widget>[
            _buildWidgetBackground(mediaQuery),
            _buildWidgetActionAppBar(mediaQuery), // tambahkan kode ini
            Container(
              child:  new ListView(
                physics: ClampingScrollPhysics(),
                children: <Widget>[
                  new Container(
                    padding: EdgeInsets.only(left: 16.0, right: 16.0, top: 100.0),
//                  color: Colors.green,
                    child: new Column(
                      children: <Widget>[
                        _buildGopayMenu(),
                      ],
                    ),
                  ),
                ],
              ),
            ),
           _buildWidgetListSong(mediaQuery)
          ],
        ),
      ),
    );
  }

  Widget _buildWidgetActionAppBar(MediaQueryData mediaQuery) {
    return Padding(
      padding: EdgeInsets.only(
        left: 40.0,
        top: mediaQuery.padding.top + 16.0,
        right: 40.0,
      ),
      child: Column(
        children: <Widget>[
          Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text("PAY / TOP UP "),
                Icon(
                  Icons.menu,
                  color: Colors.white,
                ),
              ],
            ),
          )



        ],
      ),
    );
  }

  Widget _buildWidgetBackground(MediaQueryData mediaQuery) {
    return Container(
      width: double.infinity,
      height: mediaQuery.size.height / 2.9,
      decoration: BoxDecoration(
        shape: BoxShape.rectangle,
        borderRadius: BorderRadius.only(
          bottomLeft: Radius.circular(13.0),
          bottomRight: Radius.circular(13.0),

        ),
        color: Colors.greenAccent,
      ),
    );
  }
  Widget _buildGojekServicesMenu() {
    return new SizedBox(
        width: double.infinity,
        height: 120.0,
        child: new Container(
            margin: EdgeInsets.only(top: 8.0, bottom: 8.0),
            child: GridView.builder(
                physics: ClampingScrollPhysics(),
                itemCount: 8,
                gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 4),
                itemBuilder: (context, position) {
                  return new Text("Gojek Menu");
                })));
  }}
Widget _buildWidgetListSong(MediaQueryData mediaQuery) {
  return Padding(
    padding: EdgeInsets.only(
      left: 20.0,
      top: mediaQuery.size.height / 3.5 + 150.0,
      right: 20.0,
      bottom: mediaQuery.padding.bottom + 16.0,
    ),
    child: Column(
      children: <Widget>[
        SizedBox(height: 18.0),
        Expanded(
          child: ListView.builder(
            itemCount: 3,
            itemBuilder: (context, index) {
              return Container(
                decoration: new BoxDecoration(
//                  boxShadow: <BoxShadow>[
//                    BoxShadow(
//                        color: Colors.black12,
//                        blurRadius: 5.0,
//                        spreadRadius: 0.5,
//                        offset: Offset(0.5, 0.5)),
//                  ],
                ),
                margin: EdgeInsets.only(left: 2.0,right: 2.0,top: 10),
                child:
                ButtonTheme(
                  height: 85.0,
                  child: RaisedButton(
                    onPressed: () {

                    },
                    color: Colors.white,
                    elevation: 2.0,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(6.0)),
                    child: Stack(
                      children: <Widget>[
                        ClipRRect(
                          child:
                        Text(
                      "dada",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: Colors.black,
                            fontFamily: 'CoralPen',
                            fontSize: 32.0,
                          ),
                        ),
                        )
                      ]
                    ),
                  ),
                ),
              );
            },
          ),
        ),
      ],
    ),
  );
}


Widget _buildGopayMenu() {
  return new Container(
      height: 230.0,
      decoration: new BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            colors: [Colors.white, Colors.white],
          ),
          borderRadius: new BorderRadius.all(new Radius.circular(3.0))),
      child: new Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,

        children: <Widget>[

          new Container(
            padding: EdgeInsets.only(left: 32.0, right: 32.0, top: 12.0),
            child: new Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                new Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                        ButtonTheme(
                          minWidth: 65.0,
                          height: 65.0,
                          child:Stack(
                            children: <Widget>[
                              RaisedButton(
                                onPressed: (){
                                },
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(6.0)),
                                elevation: 4.0,
                                color: Colors.white,
                                child:Column(
                                  children: <Widget>[
                                    new Padding(
                                      padding: EdgeInsets.only(top: 10.0),
                                    ),
                                    Image.asset(
                                      "assets/icons/icon_transfer.png",
                                      width: 32.0,
                                      height: 32.0,
                                    ),
                                    new Padding(
                                      padding: EdgeInsets.only(top:5.0),
                                    ),
                                    new Text(
                                      "GOPAY",
                                      textAlign: TextAlign.center,
                                      style: TextStyle(color: Colors.black, fontSize: 11.0),
                                    ),
                                    new Padding(
                                      padding: EdgeInsets.only(bottom: 5.0),
                                    ),

                                  ],
                                )

                              ),

                            ],
                          )
           ,
                        ),






                  ],
                ),
                new Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    ButtonTheme(
                      minWidth: 65.0,
                      height: 65.0,
                      child:Stack(
                        children: <Widget>[
                          RaisedButton(
                              onPressed: (){
                              },
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(6.0)),
                              elevation: 4.0,
                              color: Colors.white,
                              child:Column(
                                children: <Widget>[
                                  new Padding(
                                    padding: EdgeInsets.only(top: 10.0),
                                  ),
                                  Image.asset(
                                    "assets/icons/icon_transfer.png",
                                    width: 32.0,
                                    height: 32.0,
                                  ),
                                  new Padding(
                                    padding: EdgeInsets.only(top:5.0),
                                  ),
                                  new Text(
                                    "OVO",
                                    textAlign: TextAlign.center,
                                    style: TextStyle(color: Colors.black, fontSize: 12.0),
                                  ),
                                  new Padding(
                                    padding: EdgeInsets.only(bottom: 5.0),
                                  ),

                                ],
                              )

                          ),

                        ],
                      )
                      ,
                    ),






                  ],
                ),
                new Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    ButtonTheme(
                      minWidth: 65.0,
                      height: 65.0,
                      child:Stack(
                        children: <Widget>[
                          RaisedButton(
                              onPressed: (){
                              },
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(6.0)),
                              elevation: 4.0,
                              color: Colors.white,
                              child:Column(
                                children: <Widget>[
                                  new Padding(
                                    padding: EdgeInsets.only(top: 10.0),
                                  ),
                                  Image.asset(
                                    "assets/icons/icon_transfer.png",
                                    width: 32.0,
                                    height: 32.0,
                                  ),
                                  new Padding(
                                    padding: EdgeInsets.only(top:5.0),
                                  ),
                                  new Text(
                                    "PDAM",
                                    textAlign: TextAlign.center,
                                    style: TextStyle(color: Colors.black, fontSize: 12.0),
                                  ),
                                  new Padding(
                                    padding: EdgeInsets.only(bottom: 5.0),
                                  ),

                                ],
                              )

                          ),

                        ],
                      )
                      ,
                    ),






                  ],
                ),
                new Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    ButtonTheme(
                      minWidth: 65.0,
                      height: 65.0,
                      child:Stack(
                        children: <Widget>[
                          RaisedButton(
                              onPressed: (){
                              },
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(6.0)),
                              elevation: 4.0,
                              color: Colors.white,
                              child:Column(
                                children: <Widget>[
                                  new Padding(
                                    padding: EdgeInsets.only(top: 10.0),
                                  ),
                                  Image.asset(
                                    "assets/icons/icon_transfer.png",
                                    width: 32.0,
                                    height: 32.0,
                                  ),
                                  new Padding(
                                    padding: EdgeInsets.only(top:5.0),
                                  ),
                                  new Text(
                                    "Listrik",
                                    textAlign: TextAlign.center,
                                    style: TextStyle(color: Colors.black, fontSize: 12.0),
                                  ),
                                  new Padding(
                                    padding: EdgeInsets.only(bottom: 5.0),
                                  ),

                                ],
                              )

                          ),

                        ],
                      )
                      ,
                    ),






                  ],
                ),


              ],
            ),
          ),
          new Container(
            padding: EdgeInsets.only(left: 32.0, right: 32.0, top: 12.0),
            child: new Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                new Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    ButtonTheme(
                      minWidth: 65.0,
                      height: 65.0,
                      child:Stack(
                        children: <Widget>[
                          RaisedButton(
                              onPressed: (){
                              },
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(6.0)),
                              elevation: 4.0,
                              color: Colors.white,
                              child:Column(
                                children: <Widget>[
                                  new Padding(
                                    padding: EdgeInsets.only(top: 10.0),
                                  ),
                                  Image.asset(
                                    "assets/icons/icon_transfer.png",
                                    width: 32.0,
                                    height: 32.0,
                                  ),
                                  new Padding(
                                    padding: EdgeInsets.only(top:5.0),
                                  ),
                                  new Text(
                                    "DANYA",
                                    textAlign: TextAlign.center,
                                    style: TextStyle(color: Colors.black, fontSize: 12.0),
                                  ),
                                  new Padding(
                                    padding: EdgeInsets.only(bottom: 5.0),
                                  ),

                                ],
                              )

                          ),

                        ],
                      )
                      ,
                    ),






                  ],
                ),
                new Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    ButtonTheme(
                      minWidth: 65.0,
                      height: 65.0,
                      child:Stack(
                        children: <Widget>[
                          RaisedButton(
                              onPressed: (){
                              },
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(6.0)),
                              elevation: 4.0,
                              color: Colors.white,
                              child:Column(
                                children: <Widget>[
                                  new Padding(
                                    padding: EdgeInsets.only(top: 10.0),
                                  ),
                                  Image.asset(
                                    "assets/icons/icon_transfer.png",
                                    width: 32.0,
                                    height: 32.0,
                                  ),
                                  new Padding(
                                    padding: EdgeInsets.only(top:5.0),
                                  ),
                                  new Text(
                                    "DANYA",
                                    textAlign: TextAlign.center,
                                    style: TextStyle(color: Colors.black, fontSize: 12.0),
                                  ),
                                  new Padding(
                                    padding: EdgeInsets.only(bottom: 5.0),
                                  ),

                                ],
                              )

                          ),

                        ],
                      )
                      ,
                    ),






                  ],
                ),
                new Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    ButtonTheme(
                      minWidth: 65.0,
                      height: 65.0,
                      child:Stack(
                        children: <Widget>[
                          RaisedButton(
                              onPressed: (){
                              },
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(6.0)),
                              elevation: 4.0,
                              color: Colors.white,
                              child:Column(
                                children: <Widget>[
                                  new Padding(
                                    padding: EdgeInsets.only(top: 10.0),
                                  ),
                                  Image.asset(
                                    "assets/icons/icon_transfer.png",
                                    width: 32.0,
                                    height: 32.0,
                                  ),
                                  new Padding(
                                    padding: EdgeInsets.only(top:5.0),
                                  ),
                                  new Text(
                                    "DANYA",
                                    textAlign: TextAlign.center,
                                    style: TextStyle(color: Colors.black, fontSize: 12.0),
                                  ),
                                  new Padding(
                                    padding: EdgeInsets.only(bottom: 5.0),
                                  ),

                                ],
                              )

                          ),

                        ],
                      )
                      ,
                    ),






                  ],
                ),
                new Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    ButtonTheme(
                      minWidth: 65.0,
                      height: 65.0,
                      child:Stack(
                        children: <Widget>[
                          RaisedButton(
                              onPressed: (){
                              },
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(6.0)),
                              elevation: 4.0,
                              color: Colors.white,
                              child:Column(
                                children: <Widget>[
                                  new Padding(
                                    padding: EdgeInsets.only(top: 10.0),
                                  ),
                                  Image.asset(
                                    "assets/icons/icon_menu.png",
                                    width: 32.0,
                                    height: 32.0,
                                  ),
                                  new Padding(
                                    padding: EdgeInsets.only(top:5.0),
                                  ),
                                  new Text(
                                    "DANYA",
                                    textAlign: TextAlign.center,
                                    style: TextStyle(color: Colors.black, fontSize: 12.0),
                                  ),
                                  new Padding(
                                    padding: EdgeInsets.only(bottom: 5.0),
                                  ),

                                ],
                              )

                          ),

                        ],
                      )
                      ,
                    ),






                  ],
                ),


              ],
            ),
          )
        ],
      ));
}