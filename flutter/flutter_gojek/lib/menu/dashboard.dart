import 'package:flutter/material.dart';

class Dashboard extends StatefulWidget {
  @override
  _Dashboard createState() => _Dashboard();
}

class _Dashboard extends State<Dashboard> {
  List<Song> listSong = List();
  GlobalKey<ScaffoldState> scaffoldState = GlobalKey();
  @override
  void initState() {
    initListSong();
    super.initState();
  }
  void initListSong() {
    listSong.add(Song(title: '7 rings', duration: '2:58'));
    listSong.add(Song(title: '7 rings', duration: '2:58'));
    listSong.add(Song(title: 'Thank u, next', duration: '3:27'));
    listSong.add(Song(title: 'Thank u, next', duration: '3:27'));
    listSong.add(Song(title: 'Thank u, next', duration: '3:27'));
    listSong.add(Song(
        title: 'Break up with your girlfriend, i\'m bored', duration: '3:10'));
  }
  Widget _AtmCard() {
    return new Container(
        height: 230.0,
        decoration: new BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors: [ Colors.green , Colors.teal],
            ),
            borderRadius: new BorderRadius.all(new Radius.circular(15.0))),
        child: new Column(
          children: <Widget>[

            new Container(
              padding: EdgeInsets.only(left: 32.0, right: 32.0, top: 12.0),
              child: new Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  new Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      new Image.asset(
                        "assets/icons/icon_scan.png",
                        width: 32.0,
                        height: 32.0,
                      ),
                      new Padding(
                        padding: EdgeInsets.only(top: 10.0),
                      ),
                      new Text(
                        "Scan QR",
                        style: TextStyle(color: Colors.white, fontSize: 12.0),
                      )
                    ],
                  ),
                ],
              ),
            ),
            new Container(
              padding: EdgeInsets.only(left: 32.0, right: 32.0, top: 12.0),
              child: new Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  new Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      new Image.asset(
                        "assets/icons/icon_scan.png",
                        width: 32.0,
                        height: 32.0,
                      ),
                      new Padding(
                        padding: EdgeInsets.only(top: 10.0),
                      ),
                      new Text(
                        "Scan QR",
                        style: TextStyle(color: Colors.white, fontSize: 12.0),
                      )
                    ],
                  ),
                  new Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      new Image.asset(
                        "assets/icons/icon_scan.png",
                        width: 32.0,
                        height: 32.0,
                      ),
                      new Padding(
                        padding: EdgeInsets.only(top: 10.0),
                      ),
                      new Text(
                        "Scan QR",
                        style: TextStyle(color: Colors.white, fontSize: 12.0),
                      )
                    ],
                  ),
                ],
              ),
            ),
          ],
        ));
  }

  Widget _buildWidgetBackground(MediaQueryData mediaQuery) {
    return Container(
      width: double.infinity,
      height: mediaQuery.size.height / 2.9,
      decoration: BoxDecoration(
        shape: BoxShape.rectangle,
        borderRadius: BorderRadius.only(
          bottomLeft: Radius.circular(13.0),
          bottomRight: Radius.circular(13.0),

        ),
      color: Colors.greenAccent,
      ),
    );
  }

  Widget _buildWidgetActionAppBar(MediaQueryData mediaQuery) {
    return Padding(
      padding: EdgeInsets.only(
        left: 40.0,
        top: mediaQuery.padding.top + 40.0,
        right: 40.0,
      ),
      child: Column(
        children: <Widget>[
          Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Icon(
                  Icons.info_outline,
                  color: Colors.white,
                ),
                Icon(
                  Icons.menu,
                  color: Colors.white,
                ),
              ],
            ),
          ),
          Container(child:
            Text(''),),
          Container(
            child: Row(

              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text("DEDY INDRA SETIAWAN")
              ],
            ),
          )



        ],
      ),
    );
  }

  Widget _buildWidgetHeaderHistory() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Text(
          'Transaction',
          style: TextStyle(
            color: Colors.black,
            fontWeight: FontWeight.w600,
            fontSize: 24.0,
            fontFamily: 'Campton_Light',
          ),
        ),
        Text(
          'Show all',
          style: TextStyle(
            color: Color(0xFF7D9AFF),
            fontWeight: FontWeight.w600,
            fontFamily: 'Campton_Light',
          ),
        ),
      ],
    );
  }
  Widget _buildWidgetLisHistory(MediaQueryData mediaQuery) {
    return Padding(
      padding: EdgeInsets.only(
        left: 20.0,
        top: mediaQuery.size.height / 1.8 + 15.0,
        right: 20.0,
        bottom: mediaQuery.padding.bottom + 16.0,
      ),
      child: Column(
        children: <Widget>[
          _buildWidgetHeaderHistory(),
          SizedBox(height: 16.0),
          Expanded(
            child: ListView.separated(
              padding: EdgeInsets.zero,
              itemBuilder: (BuildContext context, int index) {
                Song song = listSong[index];
                return GestureDetector(
                  onTap: () {
                    // TODO: item song arahkan ke UI Music Player
                  },
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        child: Text(
                          song.title,
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                            fontFamily: 'Campton_Light',
                          ),
                          overflow: TextOverflow.ellipsis,
                        ),
                      ),
                      Text(
                        song.duration,
                        style: TextStyle(
                          color: Colors.grey,
                        ),
                      ),
                      SizedBox(width: 24.0),
                      Icon(Icons.more_horiz, color: Colors.grey,),
                    ],
                  ),
                );
              },
              separatorBuilder: (BuildContext context, int index) {
                return Opacity(
                  opacity: 0.5,
                  child: Padding(
                    padding: const EdgeInsets.only(top: 2.0),
                    child: Divider(
                      color: Colors.grey,
                    ),
                  ),
                );
              },
              itemCount: listSong.length,
            ),
          ),
        ],
      ),
    );
  }


  @override
  Widget build(BuildContext context) {
    var mediaQuery = MediaQuery.of(context);

    return (Scaffold(
      key: scaffoldState,
      body: Stack(
        children: <Widget>[
          _buildWidgetBackground(mediaQuery),
          _buildWidgetActionAppBar(mediaQuery), // tambahkan kode ini
          Container(
            child: new ListView(
              physics: ClampingScrollPhysics(),
              children: <Widget>[
                new Container(
                  padding: EdgeInsets.only(left: 16.0, right: 16.0, top: 115.0),
                  child: new Column(
                    children: <Widget>[
                      _AtmCard(),
                    ],
                  ),
                ),

              ],
            ),
          ),
          _buildWidgetLisHistory(mediaQuery),

        ],

      ),
    ));
  }
}
class Song {
  String title;
  String duration;

  Song({this.title, this.duration});

  @override
  String toString() {
    return 'Song{title: $title, duration: $duration}';
  }
}

