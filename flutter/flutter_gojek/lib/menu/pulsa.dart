import 'package:flutter/material.dart';

class Pulsa extends StatefulWidget {
  @override
  _Pulsa createState() => _Pulsa();
}
GlobalKey<ScaffoldState> scaffoldState = GlobalKey();

class _Pulsa extends State<Pulsa> {

  Widget PhoneNumberInput() {
    return (
        Container(
          padding: EdgeInsets.all(10.0),
          child: Row(
            children: <Widget>[
              Expanded(
                  child: TextField(),
                  flex: 6
              ),
              Expanded(
                  child: Icon(Icons.contact_phone),
                  flex: 1
              )
            ],
          ),
        )
    );
  }

  Widget _buildWidgetBackground(MediaQueryData mediaQuery) {
    return Container(
      width: double.infinity,
      height: mediaQuery.size.height / 4.9,
      decoration: BoxDecoration(
        shape: BoxShape.rectangle,
        borderRadius: BorderRadius.only(
          bottomLeft: Radius.circular(45.0),
//          bottomRight: Radius.circular(13.0),

        ),
        color: Colors.greenAccent,
      ),
    );
  }

  Widget _buildWidgetActionAppBar(MediaQueryData mediaQuery) {
    return Padding(
      padding: EdgeInsets.only(
        left: 40.0,
        top: mediaQuery.padding.top + 40.0,
        right: 40.0,
      ),
      child: Column(
        children: <Widget>[
          Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text("SEND"),
                Icon(
                  Icons.menu,
                  color: Colors.white,
                ),
              ],
            ),
          ),




        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    var mediaQuery = MediaQuery.of(context);

    return (
        Scaffold(
            body:
              Stack(
                children: <Widget>[
                  _buildWidgetBackground(mediaQuery),
                  _buildWidgetActionAppBar(mediaQuery), // tambahkan kode ini
                  Container(
                      width: double.infinity,
                      height: 775,
                      child: Column(
                        children: <Widget>[

                          Expanded(child: Container(
                            padding: EdgeInsets.only(left: 16.0, right: 16.0, top: 160.0),
                            child: Column(
                              children: <Widget>[
                                PhoneNumberInput()
                              ],
                            ),
                          ), flex: 6),
                        ],
                      )
                  ),
                ],

              ),


        )
    );
  }
}