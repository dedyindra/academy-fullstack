import 'package:flutter/material.dart';
import 'package:flutter_gojek/beranda/berandapage_view.dart';
import 'package:flutter_gojek/constant.dart';
import 'package:flutter_gojek/menu/dashboard.dart';
import 'package:flutter_gojek/menu/pulsa.dart';

class LandingPage  extends StatefulWidget {
  @override
  _LandingPageState  createState() => new _LandingPageState ();
}
class _LandingPageState extends State<LandingPage> {
  int _bottomNavCurrentIndex = 0;
  List<Widget> _container = [
    new Dashboard(),
    new Pulsa(),
    new BerandaPage(),
  ];
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        body: _container[_bottomNavCurrentIndex],
        bottomNavigationBar: _buildBottomNavigation()
    );
  }

  Widget _buildBottomNavigation(){
    return new BottomNavigationBar(
      type: BottomNavigationBarType.fixed,
      onTap: (index) {
        setState(() {
          _bottomNavCurrentIndex = index;
        });
      },
      currentIndex: _bottomNavCurrentIndex,
      items: [
        BottomNavigationBarItem(
          activeIcon: new Icon(
            Icons.home,
            color: GojekPalette.green,
          ),
          icon: new Icon(
            Icons.home,
            color: Colors.grey,
          ),
          title: new Text(
            'Beranda',
          ),
        ),
        BottomNavigationBarItem(
          activeIcon: new Icon(
            Icons.attach_money,
            color: GojekPalette.green,
          ),
          icon: new Icon(
            Icons.attach_money,
            color: Colors.grey,
          ),
          title: new Text('Send'),
        ),
        BottomNavigationBarItem(
          activeIcon: new Icon(
            Icons.payment,
            color: GojekPalette.green,
          ),
          icon: new Icon(
            Icons.payment,
            color: Colors.grey,
          ),
          title: new Text('Pay'),
        ),
        BottomNavigationBarItem(
          activeIcon: new Icon(
            Icons.mail,
            color: GojekPalette.green,
          ),
          icon: new Icon(
            Icons.mail,
            color: Colors.grey,
          ),
          title: new Text('inbox'),
        ),
        BottomNavigationBarItem(
          activeIcon: new Icon(
            Icons.person,
            color: GojekPalette.green,
          ),
          icon: new Icon(
            Icons.person,
            color: Colors.grey,
          ),
          title: new Text('Profile'),
        ),
      ],
    );
  }
}