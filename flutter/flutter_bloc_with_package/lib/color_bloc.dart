import 'package:flutter/material.dart';
import 'package:bloc/bloc.dart';

enum ColorEvent { to_amber, to_light_blue }

// jika menggunaan package akan meminta 2 overide method
// kalo disini state controller dengan event controller nya sudaj dibungkus djadi satu
class ColorBloc extends Bloc<ColorEvent, Color> {
  Color _color = Colors.amber;

  @override
  // TODO: implement initialState
  // ini initiak statenya
  Color get initialState => Colors.amber;

  @override
  // ini mapToState nya
  Stream<Color> mapEventToState(ColorEvent event) async* {
    // TODO: implement mapEventToState
    _color = (event == ColorEvent.to_amber) ? Colors.amber : Colors.lightBlue;
    // yield untuk memasukan data ke dalam stream nya
    yield _color;
  }
}
