# flutter_qrqode

Flutter project qr code.

## QR code
Qrcode / Quick Response is used to deliver information quickly and get a data fast response, QR Code is an image in the form of a box which is usually very easy to read using a QR code reader, which can be used free of charge. To find out the information in it, you only need to ``scan`` the code by taking a picture using the camera on your smartphone. Then, the code will automatically be changed to a information link.

## Getting Started
### generate Qr
 use depedencies
 ```
dependencies:
  flutter:
    sdk: flutter
  qr_flutter: ^3.0.1
```
import package 

```import 'package:qr_flutter/qr_flutter.dart'; ```\

To use 
 ```
QrImage(
       version: 4,
       backgroundColor: Colors.white70,
       foregroundColor: Colors.black,
       errorCorrectionLevel: QrErrorCorrectLevel.M,
       padding: EdgeInsets.all(30),
       embeddedImage: AssetImage('assets/images/logo.png'),
         embeddedImageStyle : QrEmbeddedImageStyle(
           size: Size.square(30),
           color: Color.fromARGB(100, 10, 10, 10)
              ),
               size: 300,
               data: "${profilId['id']}",
        )
```
``information`` link version for qrcode : [link disini](https://www.qrcode.com/en/about/version.html)

### Scanner Qr code

add dependencies
```
dependencies:
  flutter:
    sdk: flutter
  qr_code_scanner: ^0.0.12
```
### Flip Camera (Back/Front) 
The default camera is the back camera.
controller.flipCamera();

### Flash (Off/On) 
By default, flash is OFF.
controller.toggleFlash();

### Resume/Pause
Pause camera stream and scanner.
controller.pause();

Resume camera stream and scanner.
controller.resume();

## example result
![generate](generate.png )
  
![scanner](scanner.png)