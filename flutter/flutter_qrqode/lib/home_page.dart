import 'package:flutter/material.dart';
import 'package:flutter_qrqode/generate_qr.dart';
import 'package:flutter_qrqode/scanner.dart';

class HomePage extends StatefulWidget {
  static const routeName = "homepage";

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  String data = "qrcode";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            RaisedButton(
              onPressed: () {
                Navigator.pushNamed(context, GenerateQR.routeName);
              },
              child: Text("profile"),
            ),
            FlatButton(
              onPressed: (){
                Navigator.pushNamed(context, Scanner.routeName);
              },
              color: Colors.blueAccent,
              child: Text('scanner'),
            ),
            Text(data)
          ],
        ),
      ),
    );
  }
}
