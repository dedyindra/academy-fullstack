

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_qrqode/generate_qr.dart';
import 'package:flutter_qrqode/home_page.dart';
import 'package:flutter_qrqode/scanner.dart';
import 'package:flutter_qrqode/profile_id.dart';
import 'package:flutter_qrqode/service/profile_service.dart';
void main() => runApp(
  MaterialApp(
    debugShowCheckedModeBanner : false,
    title : 'Qr Qode',
    initialRoute : HomePage.routeName ,
    onGenerateRoute : (RouteSettings settings) {
      var routes = <String, WidgetBuilder>{
        HomePage.routeName : (context) => HomePage(),
        GenerateQR.routeName : (context) => GenerateQR(FetchProfile(),settings.arguments),
        ProfileId.routName : (context)=> ProfileId(FetchProfile(),settings.arguments),
        Scanner.routeName:(context)=> Scanner(),
      };
      WidgetBuilder widgetBuilder = routes[settings.name];
      return MaterialPageRoute(builder : (context) => widgetBuilder(context));
    },
  ),
);

