package com.enigma.repositories;

import com.enigma.entity.Artist;
import com.enigma.entity.Genre;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GenreRepository  extends JpaRepository<Genre,String> {
    public List<Genre> findAllByIdContains(String keyword);

}
