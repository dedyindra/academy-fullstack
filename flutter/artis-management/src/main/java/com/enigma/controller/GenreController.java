package com.enigma.controller;

import com.enigma.entity.Artist;
import com.enigma.entity.Genre;
import com.enigma.service.impl.GenreServices;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

@RestController
@CrossOrigin
public class GenreController
{
    @Autowired
    ObjectMapper objectMapper;
    @Autowired
    GenreServices genreServices;
    @GetMapping("/genre-list")
    public List<Genre> getGenre() {
        return genreServices.getAll();
    }

//    @PostMapping("/genres")
//    public Genre saveGenre(@RequestBody Genre genre) {
//        return genreServices.save(genre);
//    }

    @GetMapping("/genre/{id}")
    public Genre getGenreById(@PathVariable String id) {
        return genreServices.getGenreId(id);
    }

    @PostMapping("/genre")
    public Genre saveGenre(@RequestPart MultipartFile file, @RequestPart String genre) throws IOException {
        Genre dataGenre = genreServices.save(objectMapper.readValue(genre, Genre.class));
        try {
            // Get the file and save it somewhere
            byte[] bytes = file.getBytes();
            // file.getOriginalFilename(); untuk ambil nama aslinya
            Path path = Paths.get("E:/nginx/html/img/" + dataGenre.getId() + ".png" );
            Files.write(path, bytes);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return dataGenre;
    }

//    @GetMapping("/artis/{keyword}")
//    public List<Genre> getLisArtisttByIdGenre(@PathVariable String keyword){
//        return genreServices.getAllArtistByIdGenre(keyword);
//    }
}
