import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_artis_management/artisApp.dart';
import 'package:flutter_artis_management/service/fetch_genre_service.dart';
import 'package:flutter_artis_management/service/fetch_arttist_service.dart';
import 'package:flutter_artis_management/songApp.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

class GenreApp extends StatefulWidget {
  static const routeName = 'genreApp';
  FetchGenre fetchGenre;
  GenreApp(this.fetchGenre);
  @override
  State createState() {
    return GenreAppState(fetchGenre);
  }
}
class GenreAppState extends State {
FetchGenre fetchGenre;
var genreList;

GenreAppState(this.fetchGenre);
getDataGenreById() async {
  var response = await fetchGenre.getDataGenre();
  setState(() {
    genreList = List.of(jsonDecode(response.body));
  });
}

@override
void initState() {
  getDataGenreById();
}
@override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('Genre'),
        flexibleSpace: Container(
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.topLeft,
                  end: Alignment.bottomRight,
                  colors: <Color>[
                    Colors.red,
                    Colors.blue
                  ])
          ),
        ),
      ),
      body: Container(
        padding: EdgeInsets.only(right: 15.0, left: 15.0),
        child: Genre(genreList),
      ),
    );
  }
}

class Genre extends StatelessWidget {
  var genreList = List();
  Genre(this.genreList);
  @override
  Widget build(BuildContext context) {
    //TODO build ListView here
    return Container(
      child: ListView.builder(
        itemCount: genreList.length,
        itemBuilder: (context, index) {
          return Container(
            decoration: new BoxDecoration(
              boxShadow: <BoxShadow>[
                BoxShadow(
                    color: Colors.black12,
                    blurRadius: 5.0,
                    spreadRadius: 0.5,
                    offset: Offset(0.5, 0.5)),
              ],
            ),
            margin: EdgeInsets.all(15.0),
            child: RaisedButton(
              onPressed: () {
              //  FetchArtist.idGenre = genreList[index]["id"];
                Navigator.pushNamed(
                  context,
                  ArtisApp.routeName,
//                 arguments: GetDataGenre(genreList[index]["id"], genreList[index]["genre"])
                arguments: genreList[index]["id"]
                );
              },
              color: Colors.amber,
              elevation: 8.0,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(15.0)),
              child: Stack(
                children: <Widget>[
                  ClipRRect(
                    borderRadius: new BorderRadius.circular(8.0),
                    child: Row(
                      children: <Widget>[
                        Image.network(
                          'http://10.0.2.2/img/'+genreList[index]["id"]+'.png',
                        ),
                        Center(
                            child: Text(
                              genreList[index]['genre'],
                              style: TextStyle(
                                color: Colors.black,
                                fontFamily: 'CoralPen',
                                fontSize: 32.0,
                              ),
                            )
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}


