import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_artis_management/service/fetch_arttist_service.dart';
import 'package:flutter_artis_management/service/fetch_song_service.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';
class SongApp extends StatefulWidget {
  static const routeName = 'songApp';
  FetchSong fetchSong;
String artistId;
  SongApp(this.fetchSong,this.artistId);

  @override
  State createState() {
    return _SongAppState(fetchSong,artistId);
  }
}

class _SongAppState extends State {
 var songList = List();
 String artistId;
FetchSong fetchSong;
  _SongAppState(this.fetchSong,this.artistId);

 getDataSongById() async {
   var response = await fetchSong.getDataSong(artistId);
   setState(() {
     songList = List.of(jsonDecode(response.body));
   });
 }

  @override
  void initState(){
    getDataSongById(); }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        centerTitle: true,
        title: Text('Song'),
        flexibleSpace: Container(
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.topLeft,
                  end: Alignment.bottomRight,
                  colors: <Color>[
                    Colors.red,
                    Colors.blue
                  ])
          ),
        ),
      ),
      body:  ListView.builder(
        itemCount: songList == null ? 0 :songList.length,
        itemBuilder: (BuildContext context, int index){
          return  Card(
            margin: EdgeInsets.all(10.0),
            child: Container(
              width: 100,
              height: 100,
              padding: EdgeInsets.all(15.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  FloatingActionButton(
                    child: Icon(
                      Icons.play_arrow,
                      color: Colors.white,
                    ),
                    backgroundColor: Color(0xFF7D9AFF),
                    onPressed: () {
                    },
                  ),
                  Text(songList[index]["nameLagu"]),
                  Text("03 : 00 "),
                ],
              ),
            )
          );
        },
      ),
    );
  }

}
