import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_artis_management/service/fetch_song_service.dart';
import 'package:flutter_artis_management/service/fetch_arttist_service.dart';
import 'package:flutter_artis_management/songApp.dart';

class ArtisApp extends StatefulWidget {
  static const routeName = 'artisApp';
  FetchArtist fetchArtist;
  String idGenre;
  ArtisApp(this.fetchArtist,this.idGenre);

  @override
  State createState() {
    return _ArtisAppState(fetchArtist,idGenre);
  }
}

class _ArtisAppState extends State {
  FetchArtist fetchArtist;
  var artistList = List();
  String id='';
  String genre;
String idGenre;
  _ArtisAppState(this.fetchArtist,this.idGenre);

  getDataArtistById() async {
    var response = await fetchArtist.getDataArtist(idGenre);
    setState(() {
      artistList = List.of(jsonDecode(response.body));
    });
  }

  @override
  void initState() {
    getDataArtistById();
  }

  @override
  Widget build(BuildContext context) {

//    if (id == '') {
//      final GetDataGenre genre = ModalRoute.of(context).settings.arguments;
//      setState(() {
//        this.id = genre.id;
////        this.id = genre.genre;
//      });
//      initState();
//    }

    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text('Artist'),
          flexibleSpace: Container(
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.topLeft,
                    end: Alignment.bottomRight,
                    colors: <Color>[Colors.red, Colors.blue])),
          ),
        ),
        body: Container(
          child: Artist(artistList),
        ));
  }
}

class GetDataGenre{
  final String id;
  final String genre;
  GetDataGenre(this.id, this.genre);
}

class Artist extends StatelessWidget {
  var artistList = List();

  Artist(this.artistList);

  @override
  Widget build(BuildContext context) {
    //TODO build ListView here
    return Container(

        child: GridView.count(
            primary: false,
            padding: const EdgeInsets.all(20),
            crossAxisSpacing: 10,
            mainAxisSpacing: 10,
            crossAxisCount: 2,
            children: List.generate(artistList.length, (index) {
              return Container(
                child: RaisedButton(
                  onPressed: () {
               //    FetchSong.artistIds = artistList[index]["id"];
                    Navigator.pushNamed(context,SongApp.routeName,arguments: artistList[index]["id"]
                    );
                  },
                  color: Colors.amber,
                  elevation: 8.0,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15.0)),
                  child: Column(
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.all(10.0),
                        child: GestureDetector(
                          child: ClipRRect(
                            borderRadius: new BorderRadius.circular(8.0),

                            child: Image.network('http://10.0.2.2/img/' +
                                artistList[index]["id"] +
                                '.png'),
                          ),
                        ),
                      ),
                      Text(artistList[index]["name"]),
                    ],
                  ),
                  padding: const EdgeInsets.all(8),
                ),
              );
            })));
  }
}
