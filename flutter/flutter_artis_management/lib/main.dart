import 'package:flutter/material.dart';
import 'package:flutter_artis_management/artisApp.dart';
import 'package:flutter_artis_management/service/fetch_arttist_service.dart';
import 'package:flutter_artis_management/service/fetch_genre_service.dart';
import 'package:flutter_artis_management/service/fetch_song_service.dart';
import 'package:flutter_artis_management/genreApp.dart';
import 'package:flutter_artis_management/songApp.dart';

void main() => runApp(
      MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Artis Management',
        initialRoute: GenreApp.routeName,
//          routes: {
//            GenreApp.routeName: (context) => GenreApp(FetchGenre()),
//            ArtisApp.routeName: (context) => ArtisApp(FetchArtist()),
//            SongApp.routeName:  (context) => SongApp(FetchSong()),
//          }
        onGenerateRoute: (RouteSettings settings) {
          var routes = <String, WidgetBuilder>{
            GenreApp.routeName: (context) => GenreApp(FetchGenre()),
            ArtisApp.routeName: (context) => ArtisApp(FetchArtist(), settings.arguments),
            SongApp.routeName: (context) => SongApp(FetchSong(),settings.arguments),
          };

          WidgetBuilder widgetBuilder = routes[settings.name];
         return MaterialPageRoute(builder: (context) => widgetBuilder(context));
        },
      ),
    );
