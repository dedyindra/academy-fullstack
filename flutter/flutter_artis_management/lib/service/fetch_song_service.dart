import 'package:flutter/material.dart';
import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;

class FetchSong {
  //static String artistIds;
  getDataSong(artistIds) async{
    return await http.get(
      Uri.encodeFull('http://10.0.2.2:7070/songs/$artistIds'),
    );
  }

}
