import 'dart:core';

import 'package:chat_app/Constant/Constant.dart';
import 'package:chat_app/Screens/SplashScreen.dart';
import 'package:flutter/material.dart';

import 'Screens/ChatScreen.dart';

main() {
  runApp(new MaterialApp(
    title: 'Fluter',
    debugShowCheckedModeBanner: false,
    theme: new ThemeData(
      accentColor: Colors.blue,
      primaryColor: Colors.white,
      primaryColorDark: Colors.white,
      fontFamily: 'Gamja Flower',
    ),
    home: new SplashScreen(),
    routes: <String, WidgetBuilder>{
      ANIMATED_SPLASH: (BuildContext context) => new SplashScreen(),
      CHAT_SCREEN: (BuildContext context) => new MyChatScreen()
    },
  ));
}
