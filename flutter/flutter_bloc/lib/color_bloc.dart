import 'dart:async';

import 'package:flutter/material.dart';

enum ColorEvent { to_amber, to_light_blue }

class ColorBloc {
  //initial state pertama kali
  Color _color = Colors.amber;

  // stream controller pertama
  StreamController<ColorEvent> _eventController =
      StreamController<ColorEvent>();

  //getter buat event shink nya dan harus public
  StreamSink<ColorEvent> get eventShink => _eventController.sink;

  //stream controller ke 2
  StreamController<Color> _stateController = StreamController<Color>();

  // getter buat state shink yang masuk dan harus public
  StreamSink<Color> get _stateShink => _stateController.sink;

  //ini selangnya
  Stream<Color> get stateStream => _stateController.stream;

// method map event to state , sebagai proses perubahannya
// ketika ui lalu event di klik melalui shink stream akan masuk ke map to event
  void _mapEventToState(ColorEvent colorEvent) {

    // kemudian state akan berubah disini

    if (colorEvent == ColorEvent.to_amber)
      _color = Colors.amber;
    else
      _color = Colors.lightBlue;

    // ini penghubung nya mapToState ke state controller akan masuk ke dalam keran state shink
    _stateShink.add(_color);
  }
  // cara menghubungkan event controller biar masuk kedalam map to state
  // ini sebuah constructor
  ColorBloc(){
    _eventController.stream.listen(_mapEventToState);
  }
  // ini untuk menyelesaikan stream atau selang2 nya
  void dispose()
  {
    _eventController.close();
    _stateController.close();
  }
}
