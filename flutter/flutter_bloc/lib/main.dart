import 'package:flutter/material.dart';
import 'package:flutter_bloc/color_bloc.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {

  ColorBloc bloc = ColorBloc();
  @override
  Widget build(BuildContext context) {

    return MaterialApp(
      home: Scaffold(
        floatingActionButton: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            FloatingActionButton(
              // event
              onPressed: () {
                bloc.eventShink.add(ColorEvent.to_amber);
              },
              backgroundColor: Colors.amber,
            ),
            SizedBox(
              width: 10,
            ),
            FloatingActionButton(
              // event
              onPressed: () {
                bloc.eventShink.add(ColorEvent.to_light_blue);
              },
              backgroundColor: Colors.lightBlue,
            )
          ],
        ),
        appBar: AppBar(
          title: Text('Bloc'),
        ),
        body: Center(
            child: StreamBuilder(
          // ini ambil dari bloc stream
          stream: bloc.stateStream,
          initialData: Colors.amber,
          // ini adalah method yang menerima context dan data yang di kirim dari staatestreams
          builder: (context, snapshot) {
            return AnimatedContainer(
              duration: Duration(milliseconds: 500),
              width: 100,
              height: 100,
              // ini di ambil dari snapshot nya dari bloc setelah update akan di build ulang
              color: snapshot.data,
            );
          },
        )),
      ),
    );
  }

  @override
  void dispose() {
    bloc.dispose();
    super.dispose();
  }
}
