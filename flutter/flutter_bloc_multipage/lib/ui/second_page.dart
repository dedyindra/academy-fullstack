import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_bloc_multipage/bloc/color_bloc.dart';
import 'package:flutter_bloc_multipage/bloc/counter_bloc.dart';
import 'package:flutter_bloc_multipage/ui/draf_page.dart';

class SecondPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // dispatcher
    ColorBloc colorBloc = BlocProvider.of<ColorBloc>(context);
    CounterBloc counterBloc = BlocProvider.of<CounterBloc>(context);

    return BlocBuilder<ColorBloc,Color>(
      builder:(context,color)=> DraftPage(
        backgroundColor: color,
        body: Center(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              BlocBuilder<CounterBloc,int>(
                builder: (context,number) => GestureDetector(
                  onTap:() {counterBloc.dispatch(number + 1);},
                  child: Text(
                  number.toString(),
                  style: TextStyle(fontSize: 40, fontWeight: FontWeight.bold),
                ),)
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  RaisedButton(
                    onPressed: () {colorBloc.dispatch(ColorEvent.toPink);},
                    color: Colors.pink,
                    child: Text(
                      "clik to change",
                      style: TextStyle(color: Colors.white),
                    ),
                    shape: (color == Colors.pink )? CircleBorder(side: BorderSide(color: Colors.black,width: 3)):CircleBorder(),
                  ),
                  RaisedButton(
                    onPressed: () {colorBloc.dispatch(ColorEvent.toAmber);},
                    color: Colors.amber,
                    child: Text(
                      "clik to change",
                      style: TextStyle(color: Colors.white),
                    ),
                    shape: (color == Colors.amber )? CircleBorder(side: BorderSide(color: Colors.black,width: 3)):CircleBorder(),
                  ),
                  RaisedButton(
                    onPressed: () {colorBloc.dispatch(ColorEvent.toPurple);},
                    color: Colors.purple,
                    child: Text(
                      "clik to change",
                      style: TextStyle(color: Colors.white),
                    ),
                    shape: (color == Colors.purple )? CircleBorder(side: BorderSide(color: Colors.black,width: 3)):CircleBorder(),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
