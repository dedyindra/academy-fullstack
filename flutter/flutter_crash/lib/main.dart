import 'dart:async';
import 'dart:io';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/material.dart';
import 'package:catcher/catcher_plugin.dart';
import 'package:flutter/services.dart';
import 'package:stack_trace/stack_trace.dart';

void main() {
  Crashlytics.instance.enableInDevMode = true;
  Crashlytics.instance.setUserEmail('dedyindra120@gmail.com');
  Crashlytics.instance.setUserName('dedy');
  Crashlytics.instance.setUserIdentifier('145');
  FlutterError.onError = Crashlytics.instance.recordFlutterError;

  runZoned(() {
    runApp(MyApp());
  }, onError: Crashlytics.instance.recordError);
}

class MyApp extends StatefulWidget {
  static const routname = 'maypp';

  @override
  _MyAppState createState() => _MyAppState();
}

final List<int> list = <int>[];

class _MyAppState extends State<MyApp> {
  int length = 0;
  String name = "data";

  int number = 0;

  onSubmit(value) {
    setState(() {
      number = value;
      print(value);
    });
  }

  @override
  void initState() {
    super.initState();
  }

  inputZero(a) {
    if (a == 0) {
      throw new IntegerDivisionByZeroException();
    }
    setState(() {
      number = a;
      print(a);
    });
  }

  final List<int> list = <int>[];

  void loop() {
    for (int b = 1; b > 0; b++) {
      print(list[-1]);
    }
  }

  changeName(nameInput) {
    setState(() {
      if (nameInput == null) {
        throw IOException;
      }
      setState(() {
        name = nameInput;
      });
    });

  }

  int names=0;
  forceClose(a){
    setState(() {
      names = a;
    });
  }
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Crashlytics example app'),
        ),
        body: Center(
          child: Column(
            children: <Widget>[
              TextField(
                  onChanged: (text){ number = int.parse(text);}
              ),
              FlatButton(
                child: const Text('input zero'),
                onPressed: () {
                  try {
                    inputZero(number);
                  } catch (exception, stack) {
                    Crashlytics.instance.recordError(exception, stack,
                        context: 'fail input zero');
                  }
                },
              ),
              Text(number.toString()),
              Text(name),
              RaisedButton(
                  color: Colors.amber,
                  child: const Text('change name'),
                  onPressed: ()  {
                    try {
                       changeName(null);
                    } catch (exception, stack) {
                      print(DateTime.now());
                      Crashlytics.instance.recordError(exception, stack,
                          context: 'error changeName');
                    }
                  }),
              RaisedButton(
                color: Colors.teal,
                  child: const Text('force closee'),
                  onPressed: ()  {
                    try {
                      forceClose(1000);
                    } catch (exception, stack) {
                      print(DateTime.now());
                      Crashlytics.instance.log('crash reported');
                      Crashlytics.instance.recordError(exception, stack,
                          context: 'error changeName');
                    }
                  }
              ),
              FlatButton(
                  child: const Text('get Image'),
                  onPressed: () {
                    try {
                      Image.asset('assets/DOES_NOT_EXIST.jpg');
                    } catch (exception, stack) {
                      print('THIS NEVER GETS PRINTED');
                      //  print(DateTime.now());
                      Crashlytics.instance.recordError(exception, stack,
                          context: 'error changeName');
                    }
                  }),
              FlatButton(
                  child: const Text('Throw Error'),
                  onPressed: () {
                    print(DateTime.now());
                    print('Throw error');
                    throw StateError('Uncaught error thrown by app.');
                  }),
              FlatButton(
                  child: const Text('Async out of bounds'),
                  onPressed: () {
                    print(DateTime.now());
                    print('Async out of bounds');
                    Future<void>.delayed(const Duration(seconds: 2), () {
                      final List<int> list = <int>[];
                      print(list[90]);
                    });
                  }),
              RaisedButton(
                  color: Colors.amber,
                  child: const Text('Record Error'),
                  onPressed: () {
                    try {
                      throw 'error_example';
                    } catch (exception, stack) {
                      Crashlytics.instance.recordError(exception, stack,
                          context: 'error as an example');
                    }
                  }),

//              Container(
//                height: 50,
//                child: Image.asset('assets/1.jpg'),
//              ),
            ],
          ),
        ),
      ),
    );
  }
}
