"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "ProfileService", {
  enumerable: true,
  get: function get() {
    return _profile["default"];
  }
});

var _profile = _interopRequireDefault(require("./profile.service"));