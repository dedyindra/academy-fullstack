"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _typeorm = require("typeorm");

var _profile = _interopRequireDefault(require("../model/profile.model"));

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { (0, _defineProperty2["default"])(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

var request = require('request');

var ProfileService =
/*#__PURE__*/
function () {
  function ProfileService() {
    (0, _classCallCheck2["default"])(this, ProfileService);
  }

  (0, _createClass2["default"])(ProfileService, [{
    key: "profileRepository",
    value: function profileRepository() {
      return (0, _typeorm.getRepository)(_profile["default"]);
    }
  }, {
    key: "findProfileById",
    value: function () {
      var _findProfileById = (0, _asyncToGenerator2["default"])(
      /*#__PURE__*/
      _regenerator["default"].mark(function _callee(id) {
        var profile;
        return _regenerator["default"].wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.next = 2;
                return this.profileRepository().findOne(id);

              case 2:
                profile = _context.sent;

                if (profile) {
                  _context.next = 5;
                  break;
                }

                throw {
                  message: "data package is not found",
                  status: 404
                };

              case 5:
                return _context.abrupt("return", profile);

              case 6:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      function findProfileById(_x) {
        return _findProfileById.apply(this, arguments);
      }

      return findProfileById;
    }()
  }, {
    key: "createProfile",
    value: function () {
      var _createProfile = (0, _asyncToGenerator2["default"])(
      /*#__PURE__*/
      _regenerator["default"].mark(function _callee2(profile) {
        return _regenerator["default"].wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _context2.next = 2;
                return this.profileRepository().save(profile);

              case 2:
                return _context2.abrupt("return", _context2.sent);

              case 3:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, this);
      }));

      function createProfile(_x2) {
        return _createProfile.apply(this, arguments);
      }

      return createProfile;
    }()
  }, {
    key: "findAllProfile",
    value: function () {
      var _findAllProfile = (0, _asyncToGenerator2["default"])(
      /*#__PURE__*/
      _regenerator["default"].mark(function _callee3() {
        var profile;
        return _regenerator["default"].wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                _context3.next = 2;
                return this.profileRepository().find();

              case 2:
                profile = _context3.sent;
                console.log(profile);
                return _context3.abrupt("return", profile);

              case 5:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3, this);
      }));

      function findAllProfile() {
        return _findAllProfile.apply(this, arguments);
      }

      return findAllProfile;
    }()
  }, {
    key: "getDataTypeOrm",
    value: function () {
      var _getDataTypeOrm = (0, _asyncToGenerator2["default"])(
      /*#__PURE__*/
      _regenerator["default"].mark(function _callee4() {
        var api;
        return _regenerator["default"].wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                api = callExternalApiUsingRequest();
                return _context4.abrupt("return", api);

              case 2:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee4);
      }));

      function getDataTypeOrm() {
        return _getDataTypeOrm.apply(this, arguments);
      }

      return getDataTypeOrm;
    }()
  }]);
  return ProfileService;
}();

var callExternalApiUsingRequest =
/*#__PURE__*/
function () {
  var _ref = (0, _asyncToGenerator2["default"])(
  /*#__PURE__*/
  _regenerator["default"].mark(function _callee5(callback) {
    var something;
    return _regenerator["default"].wrap(function _callee5$(_context5) {
      while (1) {
        switch (_context5.prev = _context5.next) {
          case 0:
            something = {};
            _context5.prev = 1;
            _context5.next = 4;
            return request.get('https://jsonplaceholder.typicode.com/todos/1', function (error, reponse, body) {
              if (!error && reponse.statusCode == 200) {
                something = _objectSpread({}, JSON.parse(body));
                console.log(something, 'hahahaha');
                return something;
              } else {
                console.log(reponse.statusCode + reponse.body);
              }
            });

          case 4:
            return _context5.abrupt("return", something);

          case 7:
            _context5.prev = 7;
            _context5.t0 = _context5["catch"](1);
            print(_context5.t0);

          case 10:
          case "end":
            return _context5.stop();
        }
      }
    }, _callee5, null, [[1, 7]]);
  }));

  return function callExternalApiUsingRequest(_x3) {
    return _ref.apply(this, arguments);
  };
}();

var _default = ProfileService;
exports["default"] = _default;