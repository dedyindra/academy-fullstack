"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _typeorm = require("typeorm");

var _profile = _interopRequireDefault(require("../model/profile.model"));

var ProfileSchema = new _typeorm.EntitySchema({
  name: "Profile",
  target: _profile["default"],
  tableName: "profile",
  columns: {
    id: {
      type: 'uuid',
      generated: 'uuid',
      unique: true,
      nullable: false,
      primary: true
    },
    name: {
      type: "varchar",
      nullable: false
    },
    address: {
      type: "varchar",
      nullable: false
    },
    gender: {
      type: "varchar",
      nullable: false
    },
    numberPhone: {
      type: "int",
      nullable: false
    },
    status: {
      type: "varchar",
      nullable: false
    },
    image: {
      type: "varchar",
      nullable: false
    }
  }
});
var _default = ProfileSchema;
exports["default"] = _default;