"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var Profile = function Profile(id, name, address, gender, numberPhone, status, image) {
  (0, _classCallCheck2["default"])(this, Profile);
  this.id = id;
  this.name = name;
  this.address = address;
  this.gender = gender;
  this.numberPhone = numberPhone;
  this.status = status;
  this.image = image;
};

var _default = Profile;
exports["default"] = _default;