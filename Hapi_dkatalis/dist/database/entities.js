"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _profile = _interopRequireDefault(require("../schema/profile.schema"));

var _default = {
  ProfileSchema: _profile["default"]
};
exports["default"] = _default;