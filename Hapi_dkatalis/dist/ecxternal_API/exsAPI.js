"use strict";

var request = require('request');

var EXTERNAL_URL = 'https://jsonplaceholder.typicode.com/todos';

var callExternalApiUsingRequest = function callExternalApiUsingRequest(callback) {
  request(EXTERNAL_URL, {
    json: true
  }, function (err, res, body) {
    if (err) {
      return callback(err);
    }

    return callback(body);
  });
};

module.exports.callApi = callExternalApiUsingRequest;