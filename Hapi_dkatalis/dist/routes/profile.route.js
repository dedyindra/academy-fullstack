"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _service = require("../service");

var _boom = _interopRequireDefault(require("@hapi/boom"));

var _fileSystem = _interopRequireDefault(require("file-system"));

var apiCall = require('../ecxternal_API/exsAPI');

var request = require('request');

var profileService = new _service.ProfileService();
var ProfileRouter = [{
  method: 'GET',
  path: '/profile',
  handler: function () {
    var _handler = (0, _asyncToGenerator2["default"])(
    /*#__PURE__*/
    _regenerator["default"].mark(function _callee(req, h) {
      var profile;
      return _regenerator["default"].wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              _context.next = 2;
              return profileService.findAllProfile();

            case 2:
              profile = _context.sent;
              // let packages = {name:"dedy"};
              console.log(profile);

              if (profile) {
                _context.next = 8;
                break;
              }

              throw _boom["default"].notFound('customer not found');

            case 8:
              return _context.abrupt("return", h.response(profile).code(200));

            case 9:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    }));

    function handler(_x, _x2) {
      return _handler.apply(this, arguments);
    }

    return handler;
  }()
}, {
  method: 'GET',
  path: '/api',
  config: {
    handler: function () {
      var _handler2 = (0, _asyncToGenerator2["default"])(
      /*#__PURE__*/
      _regenerator["default"].mark(function _callee2(req, res) {
        return _regenerator["default"].wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                apiCall.callApi(function (response) {
                  var data = JSON.stringify(response);
                  return data;
                });

              case 1:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2);
      }));

      function handler(_x3, _x4) {
        return _handler2.apply(this, arguments);
      }

      return handler;
    }()
  }
}, {
  method: 'GET',
  path: '/profile/{id}',
  config: {
    handler: function () {
      var _handler3 = (0, _asyncToGenerator2["default"])(
      /*#__PURE__*/
      _regenerator["default"].mark(function _callee3(req, h) {
        var params, profil;
        return _regenerator["default"].wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                params = req.params;
                _context3.next = 3;
                return profileService.findProfileById(params.id);

              case 3:
                profil = _context3.sent;

                if (profil) {
                  _context3.next = 8;
                  break;
                }

                throw _boom["default"].notFound('artist not found');

              case 8:
                return _context3.abrupt("return", profil);

              case 9:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3);
      }));

      function handler(_x5, _x6) {
        return _handler3.apply(this, arguments);
      }

      return handler;
    }()
  }
}, {
  method: 'POST',
  path: '/profil',
  config: {
    handler: function () {
      var _handler4 = (0, _asyncToGenerator2["default"])(
      /*#__PURE__*/
      _regenerator["default"].mark(function _callee4(req, h) {
        var profile;
        return _regenerator["default"].wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                profile = req.payload;
                console.log(profile);
                _context4.next = 4;
                return profileService.createProfile(profile);

              case 4:
                profile = _context4.sent;

                if (profile) {
                  _context4.next = 9;
                  break;
                }

                throw _boom["default"].notFound('song not found');

              case 9:
                return _context4.abrupt("return", profile);

              case 10:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee4);
      }));

      function handler(_x7, _x8) {
        return _handler4.apply(this, arguments);
      }

      return handler;
    }()
  }
}, {
  method: 'GET',
  path: '/atss',
  config: {
    handler: function () {
      var _handler5 = (0, _asyncToGenerator2["default"])(
      /*#__PURE__*/
      _regenerator["default"].mark(function _callee5(req, res) {
        var profile;
        return _regenerator["default"].wrap(function _callee5$(_context5) {
          while (1) {
            switch (_context5.prev = _context5.next) {
              case 0:
                _context5.next = 2;
                return profileService.getDataTypeOrm();

              case 2:
                profile = _context5.sent;
                console.log(profile);

                if (profile) {
                  _context5.next = 8;
                  break;
                }

                throw _boom["default"].notFound('customer not found');

              case 8:
                return _context5.abrupt("return", res.response(profile).code(200));

              case 9:
              case "end":
                return _context5.stop();
            }
          }
        }, _callee5);
      }));

      function handler(_x9, _x10) {
        return _handler5.apply(this, arguments);
      }

      return handler;
    }()
  }
}, {
  method: 'POST',
  path: '/pos',
  config: {
    handler: function () {
      var _handler6 = (0, _asyncToGenerator2["default"])(
      /*#__PURE__*/
      _regenerator["default"].mark(function _callee6(req, h) {
        var data;
        return _regenerator["default"].wrap(function _callee6$(_context6) {
          while (1) {
            switch (_context6.prev = _context6.next) {
              case 0:
                h.setRequestHeader('Content-Type', 'application/json');
                data = req.payload;
                _context6.next = 4;
                return profileService.createProfile(data);

              case 4:
                data = _context6.sent;

                if (data) {
                  _context6.next = 9;
                  break;
                }

                throw _boom["default"].notFound('song not found');

              case 9:
                return _context6.abrupt("return", data);

              case 10:
              case "end":
                return _context6.stop();
            }
          }
        }, _callee6);
      }));

      function handler(_x11, _x12) {
        return _handler6.apply(this, arguments);
      }

      return handler;
    }()
  }
}];
var _default = ProfileRouter;
exports["default"] = _default;