class Profile {
    constructor(id, name, address, gender, numberPhone, status,image) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.gender = gender;
        this.numberPhone = numberPhone;
        this.status= status;
        this.image=image;
    }
}
export default Profile;