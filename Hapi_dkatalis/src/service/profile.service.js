import {getRepository} from "typeorm";
import Profile from "../model/profile.model";

var request = require('request');

class ProfileService {
    profileRepository() {
        return getRepository(Profile);
    }

    async findProfileById(id) {
        let profile = await this.profileRepository().findOne(id);
        if (!profile) throw {message: "data package is not found", status: 404}
        return profile;
    }

    async createProfile(profile) {
        return await this.profileRepository().save(profile)
    }

    async findAllProfile() {
        let profile = await this.profileRepository().find();
        console.log(profile);
        return profile;
    }

}

export default ProfileService;
