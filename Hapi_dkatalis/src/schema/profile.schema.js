import {EntitySchema} from "typeorm";
import Profile from "../model/profile.model";

const ProfileSchema = new EntitySchema({
    name: "Profile",
    target: Profile,
    tableName: "profile",
    columns: {
        id: {
            type:'uuid',
            generated:'uuid',
            unique:true,
            nullable:false,
            primary:true
        },
        name: {
            type: "varchar",
            nullable: false
        },
        address: {
            type: "varchar",
            nullable: false
        },
        gender: {
            type: "varchar",
            nullable: false
        },
        numberPhone: {
            type: "int",
            nullable: false
        },
        status: {
            type: "varchar",
            nullable: false
        },
        image:{
            type:"varchar",
            nullable:false
        }
    }
})
export default ProfileSchema;