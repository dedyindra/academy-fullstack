import {ProfileService} from "../service";
import Boom from '@hapi/boom';

const profileService = new ProfileService();
const ProfileRouter = [
    {
        method: 'GET',
        path: '/profile',
        handler: async (req, h) => {
            const profile = await profileService.findAllProfile();
            // let packages = {name:"dedy"};
            console.log(profile);
            if (!profile) {
                throw  Boom.notFound('customer not found');
            } else {
                return h.response(profile).code(200);
            }
        }
    },
    {
        method: 'GET',
        path: '/profile/{id}',
        config: {
            handler: async (req, h) => {
                const {params} = req;
                const profil = await profileService.findProfileById(params.id);
                if (!profil) {
                    throw  Boom.notFound('artist not found');
                } else {
                    return profil;
                }
            }
        }
    },
    {
        method: 'POST',
        path: '/profile',
        config: {
            handler: async (req, h) => {
                let profile = req.payload;
                console.log(profile);
                profile = await profileService.createProfile(profile)
                if (!profile) {
                    throw  Boom.notFound('song not found');
                } else {
                    return profile;
                }

            },
        }
    },

];
export default ProfileRouter;