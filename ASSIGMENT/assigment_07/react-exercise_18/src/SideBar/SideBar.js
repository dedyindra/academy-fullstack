import React from 'react';
import '../App.css';
import '../materialize.min.css';
import {Link} from "react-router-dom";
class SideBar extends React.Component {

    render() {
        return (

                <div className="collection">
                    <Link to="/artis-form/"  className="collection-item">Artis-form</Link>
                    <Link to="/artis-list/"  className="collection-item">Artist-list</Link>
                     <Link to="/song-form/"  className="collection-item">Song-form</Link>
                </div>

        );
    }
}

export default SideBar;
