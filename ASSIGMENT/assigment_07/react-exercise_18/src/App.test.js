import React from 'react';
import App from './App';
import {shallow} from 'enzyme';
//jelaskan
describe('App component', () => {
    const appContainer = shallow(<App/>)
    //pengujian render div
    describe('render', ()=> {
        it('should contain one div ', () => {
            expect(appContainer.find('div')).toHaveLength(1)
        })

      it('should contain one header as a child to div ', () => {
        expect(appContainer.find('div').children('header')).toHaveLength(1)
      })
      it('should contain one header as a child to div ', () => {
        expect(appContainer.find('div').children('header').prop('className')).toEqual("App-header")
      })
        it('should contain one button ', () => {
            expect(appContainer.find('button')).toHaveLength(1)
        })
        it('should contain button with click ', () => {
            expect(appContainer.find('button').text()).toBe('Click')
        })
        it('should contain one label ', () => {
            expect(appContainer.find('label')).toHaveLength(1)
        })

// pengujian function
        describe('function', ()=> {
            it('input name should contain string angga wjen onchange perform ', () => {
                appContainer.find('input').simulate('change',{target:{value:'angga'}})
                //bisa di console
                console.log(appContainer.find('input').prop('value'))
                expect(appContainer.find('input').prop('value')).toEqual('angga')
            })
            //jika banyak inputannya pakai index
            it('input name should contain string  onchange perform ', () => {
                appContainer.find('input').at(0).simulate('change',{target:{value:'angga'}})
                //bisa di console
                console.log(appContainer.find('input').at(0).prop('value'))
                expect(appContainer.find('input').at(0).props())
            })
            it('label name should contain string  onchange perform ', () => {
                appContainer.find('input').at(0).simulate('change',{target:{value:'angga'}})
                //bisa di console
                console.log(appContainer.find('input').at(0).prop('value'))
                appContainer.find('button').at(0).simulate('click',{})
                expect(appContainer.find('label').at(0).text()).toEqual('angga')
            })
        });



    });
});
