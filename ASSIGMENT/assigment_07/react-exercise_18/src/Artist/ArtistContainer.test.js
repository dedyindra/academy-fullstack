import React from 'react';
import {shallow} from 'enzyme';
import ArtistContainer from "./ArtistContainer";
import ArtistCard from "./ArtistCard";
//jelaskan
describe('Artist Container', () => {
    //pengujian render div
    describe('render', ()=> {
        it('if you have 2 card  ', () => {
            const asrtist=[{name:"dedy1"},{name: "dedy2"}]
            const wraper  = shallow(<ArtistContainer artists={asrtist}/>)
            expect(wraper.find('ArtistCard')).toHaveLength(1)
        })
        //test props
        it('testing isi props  ', () => {
            let artis = {name: "zain"}
            const wraper = shallow(<ArtistCard artis={artis}></ArtistCard>)
            expect(wraper.find("div").children("h1")).text().toBe("Zain")
        })

        it('should have 2 card ', () => {
            const wraper = shallow(<ArtistCard ></ArtistCard>)
            wraper.instance().setState({})
            expect(wraper.find("div").children("h1")).text().toBe("Zain")
        })



    });
});
