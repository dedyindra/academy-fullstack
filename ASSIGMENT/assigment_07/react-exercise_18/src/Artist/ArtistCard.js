import React from 'react';
import artist from '../img/artist.jpg';
import '../App.css'
import {Link} from "react-router-dom";
class ArtistCard extends React.Component {
    render() {
        return (
            <div className="custom-row">
                <div className="card-panel teal">
                    <div className="card-image waves-effect waves-block waves-light">
                        <img className="activator" className='App-logo' src={`http://localhost/img/${this.props.artist.id}.jpg`} ></img>
                    </div>
                    <h5><b>{this.props.artist.name}</b></h5>
                    <p><b>{this.props.artist.bornPlace}</b></p>
                    <p>{this.props.artist.debut}</p>
                    <Link  className="waves-effect waves-light btn" to={`/artis-list/${this.props.artist.id}`}>List of Song</Link>
                </div>
            </div>
        );
    }
}
export default ArtistCard;
