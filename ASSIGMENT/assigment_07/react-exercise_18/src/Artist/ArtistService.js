 export async  function fetchData(pageNumber) {
    const data =  await  fetch(`http://localhost:7070/Artist?size=3&page=${pageNumber}`, {method:'GET'})
        .then((res)=>{
            console.log(res)
            return res.json()
        })
     return data;
}
 export async  function fetchDataDetail(id) {
     const data =  await  fetch(`http://localhost:7070/artist/${id}/song`, {method:'GET'})
         .then((res)=>{
             console.log(res)
             return res.json()
         })
     return data;
 }

export async  function PostData(artist) {
    return await fetch('http://localhost:7070/Artist', {
         method: 'POST',
         headers: {
             Accept: 'application/json',
             "Content-type": "application/json"
         },
         body: JSON.stringify(artist)
    })
         .then((res) => {
             return res.json()
             console.log(res.json())
         }).catch(err => err);
 }

 export async  function PostDataWithImage(artist,picture) {
     const formData = new FormData();
     let body = JSON.stringify(artist)
     formData.append('file', picture);
     formData.append('artis',body);
     console.log(formData)
     return await fetch(`http://localhost:7070/artist-image`, {
         method: 'POST', body: formData
     })
         .then((res) => {
             return res.json()
             console.log(res.json())
         }).catch(err => err);
 }


 export async  function PostDataSong(song) {
     return await fetch('http://localhost:7070/song', {
         method: 'POST',
         headers: {
             Accept: 'application/json',
             "Content-type": "application/json"
         },
         body: JSON.stringify(song)
     })
         .then((res) => {
             return res.json()
             console.log(res.json())
         }).catch(err => err);
 }
