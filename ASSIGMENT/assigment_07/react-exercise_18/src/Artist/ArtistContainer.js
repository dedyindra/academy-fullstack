import React from 'react';
import logo from '../logo.svg';
import ArtistCard from "./ArtistCard";
import {fetchData} from "./ArtistService";
import '../materialize.min.css'
import '../App.css';

class ArtistContainer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {artist: {
                content: [],
                total: null,
                per_page: null,
                current_page: 0
            }
        }
    }

    componentDidMount() {
        this.fetchAtis(0)
        // setInterval(this.fetchAtis,2000)
    }
    fetchAtis = async (pageNumber) => {
        const data = await fetchData(pageNumber);
        console.log(data)
        if (!(data === undefined)) {
            this.setState({
                artist: {content: data.content}, total: data.totalElements,
                per_page: data.size,
                current_page: data.number
            })
        }
    }
    render() {
        let artists, renderPageNumbers;
        if (this.state.artist.content !== null) {
            artists = this.state.artist.content.map(artist => {
                return <ArtistCard artist={artist}/>
            });
        }
        const pageNumbers = [];
            if (this.state.total !== null) {
            for (let i = 0; i <= Math.ceil(this.state.total / this.state.per_page -1); i++) {
                pageNumbers.push(i);
            }
            renderPageNumbers = pageNumbers.map(numbers => {
                let classes = this.state.current_page === numbers ? 'active' : '';
                return (
                    <span key={numbers} className={classes}
                          onClick={() => this.fetchAtis(numbers)}>{numbers+1}</span>
                );
            });
        }
        return (
            <div>
                {artists}
                <div className='pagination'>
                    <span onClick={() => this.fetchAtis(0)}>&laquo;</span>
                    {renderPageNumbers}
                    <span onClick={() => this.fetchAtis(0)}>&raquo;</span>

                </div>
            </div>
        );
    }
}

export default ArtistContainer;
