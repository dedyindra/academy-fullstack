import React from 'react';
import '../App.css';
import '../materialize.min.css';
import music from '../img/music.png';
import {fetchData, fetchDataDetail} from "./ArtistService";

class ListOfSong extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            artist: []
        }
    }

    componentDidMount() {
        this.fetchDetail(this.props.match.params.id)
    }

    fetchDetail = async (id) => {
        const data = await fetchDataDetail(id);
        console.log(data)
        if (!(data === undefined)) {
            this.setState({ artist: data  })
        }
    }
    //
    // getData = () => {
    //     fetch(`http://localhost:7070/artist/${this.props.match.params.id}/song`)
    //         .then(res => {
    //             return res.json();
    //         })
    //         .then(resJson => {
    //             console.log(resJson, "DATA")
    //             this.setState({artist: resJson})
    //         }).catch(err => {
    //         throw  err
    //     })
    // }

    render() {
        let no = 1;
        // let items = this.props.kirim;
        return (
            <div className="card-panel teal">
                <table className="striped">
                    <tr>
                        <th>id</th>
                        <th>artist name</th>
                        <th>judul lagu</th>
                    </tr>
                    {this.state.artist.map(item => {
                        return (
                            <tr>
                                <td>{no++}</td>
                                <td>{item.artistIdTrasient}</td>
                                <td>{item.nameLagu}</td>
                            </tr>
                        );
                    })}
                </table>
            </div>
        );
    }
}

export default ListOfSong;



