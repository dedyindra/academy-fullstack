import React from 'react';
import '../App.css';
import '../materialize.min.css';
import music from '../img/music.png';
class  Navbar extends React.Component  {

    render() {
        return (
                <nav>
                    <div class="nav-wrapper">
                        <a href="#!" className="brand-logo"><i className="material-icons"> <img src={music} className="music-logo"></img></i>Music App</a>
                        <ul class="right hide-on-med-and-down">
                            <li><a href="sass.html"></a></li>
                            <li><a href="badges.html"></a></li>
                        </ul>
                    </div>
                </nav>
        );
    }
}
export default Navbar;



