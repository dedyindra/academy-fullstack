import React from 'react';
import {PostDataSong, PostDataWithImage} from "../Artist/ArtistService";
import '../materialize.min.css'
class SongAdd extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            SongForm: {}
        }
    }

    handleOnSubmit = (event) => {
        event.preventDefault();
        let song = this.state.SongForm;
        song.nameLagu = event.target.name.value;
        song.artistIdTrasient = event.target.idArtist.value;
        this.setState({SongForm: song})
        console.log(this.state.SongForm)
       PostDataSong( this.state.SongForm)
    }
    render() {
        return (
            <div className="card-panel teal">
                <form  onSubmit={this.handleOnSubmit}>
                    <label>Title lagu : </label><br/>
                    <input type="text" name="name" />
                    <label>Artis : </label><br/>
                    <input type="text" name="idArtist"/>
                        <select className="input-field">
                            <option value="" disabled selected>Choose your option</option>
                            <option value="1">Option 1</option>
                            <option value="2">Option 2</option>
                            <option value="3">Option 3</option>
                        </select>


                    <button type="submit">Click</button>
                </form>
            </div>
        );
    }
}

export default SongAdd;
