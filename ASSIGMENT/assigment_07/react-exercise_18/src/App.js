import React from 'react';
import logo from './logo.svg';
import './App.css';
import './materialize.min.css';
import ArtistContainer from "./Artist/ArtistContainer";
import ArtistAdd from "./Artist/ArtistAdd";
import Navbar from "./Navbar/Navbar";
import SideBar from "./SideBar/SideBar";
import {BrowserRouter as Router, Route, Switch, Link} from "react-router-dom";
import ListOfSong from "./Artist/ListOfSong";
import SongAdd from "./Song/SongAdd";

class App extends React.Component {

    render() {
        return (
            <Router>
              <div className="row">
                <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"/>
                <div className="col s12 "><Navbar/></div>


                    <div className="col s2  ">
                        <SideBar/>
                    </div>

                    <div className="col s10 ">
                        <Switch>
                            <Route path="/artis-form"><ArtistAdd/></Route>
                            <Route path="/song-form"><SongAdd/></Route>
                            <Route path="/artis-list" exact> <ArtistContainer/></Route>
                            <Route path="/artis-list/:id"  component={ListOfSong} />
                        </Switch>
                    </div>
              </div>


            </Router>


        );
    }
}

export default App;
