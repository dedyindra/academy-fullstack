package com.enigma.entites;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.h2.engine.User;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Purchased {
    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    private String id;
    private BigDecimal totalPrice;

     @ManyToOne()
     @JoinColumn(name = "user_id")
     @JsonIgnore
     private Users usersId;

    @OneToMany(mappedBy = "purchasedId",cascade=CascadeType.PERSIST)

    List<PurchasedDetail> purchasedIdDetails = new ArrayList<>();

     @Transient
    private String userTrasient;

    public Purchased(BigDecimal totalPrice, Users usersId, List<PurchasedDetail> purchasedIdDetails, String userTrasient) {
        this.totalPrice = totalPrice;
        this.usersId = usersId;
        this.purchasedIdDetails = purchasedIdDetails;
        this.userTrasient = userTrasient;
    }

    public Purchased() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(BigDecimal totalPrice) {
        this.totalPrice = totalPrice;
    }

    public Users getUsersId() {
        return usersId ;
    }

    public void setUsersId(Users usersId) {
        this.usersId = usersId;
    }

    public List<PurchasedDetail> getPurchasedIdDetails() {
        return purchasedIdDetails;
    }

    public void setPurchasedIdDetails(List<PurchasedDetail> purchasedIdDetails) {
        this.purchasedIdDetails = purchasedIdDetails;
    }

    public String getUserTrasient()
    { if (userTrasient == null){
     return    usersId.getName();
    }
        return userTrasient;
    }

    public void setUserTrasient(String userTrasient) {
        this.userTrasient = userTrasient;
    }
}
