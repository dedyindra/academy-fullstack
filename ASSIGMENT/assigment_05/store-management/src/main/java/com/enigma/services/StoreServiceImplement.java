package com.enigma.services;

import com.enigma.entites.Product;
import com.enigma.entites.Store;
import com.enigma.repositories.StoreRepository;
import com.enigma.services.impl.StoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class StoreServiceImplement implements StoreService {

    @Autowired
    StoreRepository storeRepository;

    @Override
    public Store save(Store store) {
        for (Product product: store.getProducts()) {
            product.setStoreId(store);
        }
       return storeRepository.save(store);
    }

    @Override
    public Store getStore(Integer id) {
        if (!storeRepository.findById(id).isPresent()){
            return new Store();
        }
        return storeRepository.findById(id).get();
    }

    @Override
    public Page<Store> GetAll(Pageable pageable,String keyword) {
        return storeRepository.findAllByStoreNameContainsOrAddressContainsOrDescriptionContainsOrPhoneNumberContains ( keyword,keyword,keyword,keyword,pageable);
    }

    @Override
    public Page<Store> GetAllStore(Pageable pageable, Example<Store> exampleMatcher) {
        return storeRepository.findAll(exampleMatcher,pageable);
    }

    @Override
    public void delete(Integer id) {
        storeRepository.deleteById(id);
    }

    @Override
    public void update(Store store) {

    }
}
