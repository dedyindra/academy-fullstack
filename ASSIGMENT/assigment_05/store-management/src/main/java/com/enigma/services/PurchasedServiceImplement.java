package com.enigma.services;

import com.enigma.entites.*;
import com.enigma.repositories.PurchasedRepository;
import com.enigma.services.impl.ProductService;
import com.enigma.services.impl.PurchasedService;
import com.enigma.services.impl.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

@Service
public class PurchasedServiceImplement implements PurchasedService {
    @Autowired
    PurchasedRepository purchasedRepository;
@Autowired
    UsersService usersService;
@Autowired
ProductService productService;
    @Override
    public Purchased save(String id,Purchased purchased) {
        Users users = usersService.getUserById(id);
        purchased.setUsersId (users);
        BigDecimal sum =new BigDecimal(0);
        for (PurchasedDetail purchasedDetail: purchased.getPurchasedIdDetails()) {
        Product product =  productService.getProduct(purchasedDetail.getProdukTrasient());
            purchasedDetail.setPurchasedId(purchased);
            purchasedDetail.setProductId (product);
            purchasedDetail.setSubTotal(product.getPrice().multiply(new BigDecimal(purchasedDetail.getQuantity())));
            sum = sum.add(purchasedDetail.getSubTotal());
            productService.deduct(purchasedDetail.getProdukTrasient() ,purchasedDetail.getQuantity());
        }
        purchased.setTotalPrice(sum);
        return  purchasedRepository.save(purchased);
    }
    @Override
    public Purchased getPurchased(String id) {
        if (!purchasedRepository.findById(id).isPresent()){
            return new Purchased();
        }
        return purchasedRepository.findById(id).get();
    }
    @Override
    public List<Purchased> GetAll() {
        return purchasedRepository.findAll();
    }

    @Override
    public List<Purchased> getPurchasedfromuser(String id) {
        Users users = usersService.getUserById(id);
        return users.getPurchaseds();
    }


}
