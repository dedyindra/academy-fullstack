package com.enigma.services.impl;

import com.enigma.entites.Product;
import com.enigma.entites.Purchased;
import com.enigma.entites.PurchasedDetail;
import com.enigma.entites.Store;

import java.util.List;

public interface PurchasedService {
    Purchased save(String id,Purchased purchased);
    Purchased getPurchased(String  id);
    List<Purchased> GetAll();
    List<Purchased> getPurchasedfromuser(String id);
}
