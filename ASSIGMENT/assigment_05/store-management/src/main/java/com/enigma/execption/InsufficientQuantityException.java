package com.enigma.execption;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.FORBIDDEN)
public class InsufficientQuantityException extends RuntimeException{
    public  InsufficientQuantityException(String massage)  {
       super(massage);
    }

    public  InsufficientQuantityException()  {
        super("ini error message");
    }

}
