package com.enigma.entites;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "purchased_detail")
public class PurchasedDetail {
    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    private String id;
    private Integer quantity;
    private BigDecimal subTotal;

    @ManyToOne()
    @JoinColumn(name = "purchased_id")
    @JsonIgnore
    private Purchased purchasedId;

    @ManyToOne()
    @JsonIgnore
    @JoinColumn(name = "product_id")
    private Product productId;

    @Transient
    private  String produkTrasient;

    public PurchasedDetail(Integer quantity, BigDecimal subTotal, Purchased purchasedId, Product productId, String produkTrasient) {
        this.quantity = quantity;
        this.subTotal = subTotal;
        this.purchasedId = purchasedId;
        this.productId = productId;
        this.produkTrasient = produkTrasient;
    }

    public PurchasedDetail() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public BigDecimal getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(BigDecimal subTotal) {
        this.subTotal = subTotal;
    }

    public Purchased getPurchasedId() {
        return purchasedId;
    }

    public void setPurchasedId(Purchased purchasedId) {
        this.purchasedId = purchasedId;
    }

    public Product getProductId() {
        return productId;
    }

    public void setProductId(Product productId) {
        this.productId = productId;
    }

    public String getProdukTrasient()
    {
        if (produkTrasient == null){
            return    productId.getId();
        }
        return produkTrasient;
    }

    public void setProdukTrasient(String produkTrasient) {
        this.produkTrasient = produkTrasient;
    }
}
