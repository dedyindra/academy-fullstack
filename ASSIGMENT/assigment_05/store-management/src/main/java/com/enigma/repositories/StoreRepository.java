package com.enigma.repositories;

import com.enigma.entites.Store;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StoreRepository extends JpaRepository<Store,Integer> {
   Page<Store> findAllByStoreNameContainsOrAddressContainsOrDescriptionContainsOrPhoneNumberContains ( String authorName, String address, String description, String phone,Pageable pageable);
//@Query(nativeQuery = true,value = "select * from mst_store where store_name like '%?%' OR address like '%?%' or description like '%?%'")
//    public Page<Store> cari(Pageable pageable,String keyword);
    }
