package com.enigma.services.impl;

import com.enigma.entites.Purchased;
import com.enigma.entites.Store;
import com.enigma.entites.Users;

import java.util.List;

public interface UsersService {
    Users save(Users users);
    Users getUserById(String id);
    List<Users> GetAll();
    void delete(String id);

}
