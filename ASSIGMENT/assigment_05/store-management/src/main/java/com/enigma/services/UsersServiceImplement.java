package com.enigma.services;

import com.enigma.entites.Product;
import com.enigma.entites.Purchased;
import com.enigma.entites.Store;
import com.enigma.entites.Users;
import com.enigma.repositories.UsersRepository;
import com.enigma.services.impl.PurchasedService;
import com.enigma.services.impl.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UsersServiceImplement implements UsersService {
    @Autowired
    UsersRepository usersRepository;
    @Autowired
    PurchasedService purchasedService;
    @Override
    public Users save(Users users) {
        return usersRepository.save(users);
    }

    @Override
    public Users getUserById(String id) {
        if (!usersRepository.findById(id).isPresent()) {
            return new Users();
        }

        return usersRepository.findById(id).get();
    }

    @Override
    public void delete(String id) {
usersRepository.deleteById(id);
    }

    @Override
    public List<Users> GetAll() {
        return usersRepository.findAll();
    }


}
