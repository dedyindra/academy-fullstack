package com.enigma.services.impl;

import com.enigma.entites.Store;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface StoreService {
     Store save(Store store);
     Store getStore(Integer id);
     Page<Store> GetAll(Pageable pageable, String keyword);
     Page<Store> GetAllStore(Pageable pageable, Example<Store> exampleMatcher);
     void delete(Integer id);
     void update(Store store);

}
