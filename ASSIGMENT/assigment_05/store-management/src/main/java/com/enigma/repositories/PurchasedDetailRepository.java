package com.enigma.repositories;

import com.enigma.entites.PurchasedDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PurchasedDetailRepository extends JpaRepository<PurchasedDetail,String> {
}
