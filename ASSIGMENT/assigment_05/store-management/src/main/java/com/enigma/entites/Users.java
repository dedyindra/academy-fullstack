package com.enigma.entites;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "mst_user")
public class Users {
    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    private String  id;
    private String name;
    private String bitrhPlace;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date BirthDate;
    private String gender;
    @OneToMany(mappedBy = "usersId",cascade=CascadeType.PERSIST)
    List<Purchased> purchaseds = new ArrayList<>();
    public Users(String name, String bitrhPlace, Date birthDate, String gender) {
        this.name = name;
        this.bitrhPlace = bitrhPlace;
        BirthDate = birthDate;
        this.gender = gender;
    }
    public Users() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBitrhPlace() {
        return bitrhPlace;
    }

    public void setBitrhPlace(String bitrhPlace) {
        this.bitrhPlace = bitrhPlace;
    }

    public Date getBirthDate() {
        return BirthDate;
    }

    public void setBirthDate(Date birthDate) {
        BirthDate = birthDate;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Users users = (Users) o;
        return Objects.equals(id, users.id) &&
                Objects.equals(name, users.name) &&
                Objects.equals(bitrhPlace, users.bitrhPlace) &&
                Objects.equals(BirthDate, users.BirthDate) &&
                Objects.equals(gender, users.gender);
    }

    public List<Purchased> getPurchaseds() {
        return purchaseds;
    }

    public void setPurchaseds(List<Purchased> purchaseds) {
        this.purchaseds = purchaseds;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, bitrhPlace, BirthDate, gender);
    }
}
