package com.enigma.controllers;

import com.enigma.entites.Product;
import com.enigma.entites.Purchased;
import com.enigma.services.impl.PurchasedService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class PurchasedController {
@Autowired
    PurchasedService purchasedService;
@CrossOrigin
@GetMapping("/purchased")
    public List<Purchased> getAllPurchased(){
    return purchasedService.GetAll();
}

    @PostMapping("/purchasing")
    public Purchased save(@RequestBody Purchased purchased){
        return purchasedService.save(purchased.getUserTrasient(), purchased);
    }
    @GetMapping("/purchased/{id}")
    public Purchased getPurcashedById(@PathVariable String id){
        return purchasedService.getPurchased(id);
    }
    @GetMapping("user/{id}/purchased")
    public List<Purchased> getstorrebyidandProduct(@PathVariable String id){
        return  purchasedService.getPurchasedfromuser(id);
    }


}

