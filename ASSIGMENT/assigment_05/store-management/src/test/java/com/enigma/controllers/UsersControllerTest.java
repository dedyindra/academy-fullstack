package com.enigma.controllers;

import com.enigma.entites.Store;
import com.enigma.entites.Users;
import com.enigma.repositories.UsersRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Date;

import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class UsersControllerTest {
    @Autowired
    MockMvc mockMvc;
    @Autowired
    UsersRepository usersRepository;
    @Test
    public void saveUser_is_ok()throws  Exception  {
        ObjectMapper mapper = new  ObjectMapper();
        Users users = new Users("dedy","jakarta",new Date(),"L") ;
        mockMvc.perform(post("/user")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(users))).andExpect(status().isOk());
    }
    @Test
    public void saveUser_should_existinDb() throws  Exception {
        ObjectMapper mapper = new  ObjectMapper();
        Users users = new Users("dedy","jakarta",new Date(),"L") ;
        String response=    mockMvc.perform(post("/user")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(users))).andReturn().getResponse().getContentAsString();
        users = new ObjectMapper().readValue(response,Users.class);
        Assert.assertEquals(users, usersRepository.findById(users.getId()).get());
    }
    @Test
    public void getUserById_isok() throws Exception
    {
        Users users = new Users("dedy","jakarta",new Date(),"L") ;
        mockMvc.perform( MockMvcRequestBuilders
                .get("/user/{id}", 1)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(users.getId()));
    }

}