import React from 'react';
import '../materialize.min.css'
import {connect} from "react-redux"
import {changeNameLagu} from "../artist/ArtistAction";
class SongForm extends React.Component {
    render() {
        return (
            <div >
                <form>
                    <label>Judul lagu :</label><br/>
                    <input onChange={(event)=>{ this.props.dispatch({...changeNameLagu,index:this.props.index,newNameLagu : event.target.value})}}  type="text"/><br/>
                </form>
            </div>
        );
    }
}
export default connect()(SongForm);
