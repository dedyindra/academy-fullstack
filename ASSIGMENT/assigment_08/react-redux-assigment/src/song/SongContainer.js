import React from 'react';
import '../App.css';
import '../materialize.min.css';
import {fetchDataDetail} from "../artist/Service";
import {getArtist, getSong} from "../artist/ArtistAction";
import {connect} from "react-redux"
class SongContainer extends React.Component {
    componentDidMount() {
        this.fetchDetail(this.props.match.params.id)
    }
    fetchDetail = async (id) => {
        const data = await fetchDataDetail(id);
         this.props.dispatch({...getSong,payload:data});
    }
    render() {
        console.log(this.props,"asasa")
        let no = 1;
        return (
            <div className="card-panel teal">
                <table className="striped">
                    <tr>
                        <th>id</th>
                        <th>artist name</th>
                        <th>judul lagu</th>
                    </tr>
                    {this.props.song.map(item => {
                        return (
                            <tr>
                                <td>{no++}</td>
                                <td>{item.artistIdTrasient}</td>
                                <td>{item.nameLagu}</td>
                            </tr>
                        );
                    })}
                </table>
            </div>
        );
    }
}
const mapStateToProps = (state) => {
    return {...state}
}
export default connect(mapStateToProps)(SongContainer) ;



