import React from 'react';
import '../materialize.min.css'
import {connect} from "react-redux"
import {addElemenAction, addSongAction, changebornplace, changedebut, changeName} from "./ArtistAction";
import SongForm from "../song/SongForm";
import {PostData} from "./Service";

class ArtistForm extends React.Component {
    handleAddSong = (event) => {
        event.preventDefault()
        this.props.dispatch({...addSongAction})
    }
        handleOnSubmit = () => {
        let artist = this.props.artist;
        PostData(artist)
    }
    render() {
        console.log(this.props.artist, "ini songx")
        return (
            <div className="card-panel teal">
                <form onSubmit={this.handleOnSubmit}>
                    <label>Name Artist :</label><br/>
                    <input onChange={(event) => {
                        this.props.dispatch({
                            ...changeName, index: this.props.index,
                            newName: event.target.value
                        })
                    }} value={this.props.name} type="text"/><br/>
                    <label>born Place :</label><br/>
                    <input onChange={(event) => {
                        this.props.dispatch({
                            ...changebornplace,
                            index: this.props.index,
                            newBornPlace: event.target.value
                        })
                    }} value={this.props.bornPlace} type="text"/><br/>
                    <label>debut :</label><br/>
                    <input onChange={(event) => {
                        this.props.dispatch({
                            ...changedebut, index: this.props.index,
                            newDebut: event.target.value
                        })
                    }} value={this.props.debut} type="date"/><br/>
                    <button className="waves-effect waves-light btn" onClick={this.handleAddSong}> Add song
                    </button> <br/>
                    {this.props.artist.songs.map((element, index) => {
                        return <div className="row">
                        <div className="col s12">
                            {console.log(element.songs)}
                            <SongForm index={index} element={element.songs} key={index}/>
                        </div>
                        </div>
                    })}
                    <br/>
                    <button className="waves-effect waves-light btn" type="submit">submit</button>
                </form>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {...state}
}
export default connect(mapStateToProps)(ArtistForm);
