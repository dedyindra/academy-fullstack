import React from 'react';
import './App.css';
import {BrowserRouter as Router, Route, Switch, Link} from "react-router-dom";
import {Provider} from "react-redux";
import {createStore} from "redux";
import ArtistContainer from "./artist/ArtistContainer";
import './materialize.min.css';
import SongContainer from "./song/SongContainer";
import Navbar from "./navbar/Navbar";
import artist from "./artist/reducer/ArtistReducer";
import ArtistForm from "./artist/ArtistForm";
function App() {
  return (
<Router>
  <div className="row">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"/>
    <div className="col s12 "></div>
    {/*navbar*/}
    <Navbar/>
    <div className="col s2  ">
    {/*sidebar*/}
      <div className="collection">
      <Link to="/artis/"  className="collection-item">Artist-list</Link>
      <Link to="/artis-form/"  className="collection-item">Artist-form</Link>
      </div>
    </div>
    <div className="col s10 ">
      <Switch>
        <Provider store={createStore(artist)} >
        <Route path="/artis">
            <ArtistContainer/>
        </Route>
        <Route path="/artis-form">
            <ArtistForm/>
        </Route>
        <Route path="/list-song/:id"  component={SongContainer}/>
        </Provider>
      </Switch>
    </div>
  </div>
</Router>
  );
}
export default App;
