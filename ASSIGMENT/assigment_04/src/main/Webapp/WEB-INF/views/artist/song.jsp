<%--
  Created by IntelliJ IDEA.
  User: Enigmacamp
  Date: 10/7/2019
  Time: 4:00 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Title</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">

</head>
<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="/">HOME</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#menu" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="menu">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a href="artist" class="nav-link">Artist</a>
            </li>
            <li class="nav-item">
                <a href="song" class="nav-link">Song</a>
            </li>
        </ul>
    </div>
</nav>
<div class="container">
    <div class="card-body">
        <a href="song-form" class="btn btn-primary">Tambah data</a>
    </div>
    <table class="table table-bordered">
        <thead>
        <tr>
            <th>id</th>
            <th>Lagu</th>
            <th>Artist</th>

        </tr>
        </thead>
        <tbody>
        <c:forEach items="${song}" var="song">
            <tr>
                <td><c:out value="${song.id}"></c:out></td>
                <td><c:out value="${song.lagu}"></c:out></td>
                <td><c:out value="${song.artist.name}"></c:out></td>
            </tr>
        </c:forEach>

        </tbody>
    </table>
</div>
</body>
</html>
