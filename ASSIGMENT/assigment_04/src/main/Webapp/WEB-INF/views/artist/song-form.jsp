<%--
  Created by IntelliJ IDEA.
  User: Enigmacamp
  Date: 10/7/2019
  Time: 4:00 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>assigment 4</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">

</head>
<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="/">HOME</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#menu" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="menu">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a href="artist" class="nav-link">Artist</a>
            </li>
            <li class="nav-item">
                <a href="song" class="nav-link">Song</a>
            </li>
        </ul>
    </div>
</nav>
<div class="container">
    <h1>Input data Song </h1>
    <div class="card">
        <div class="card-body">
<fmt:form modelAttribute="song" action="song" method="post">

    <fmt:label path="id">id</fmt:label>
    <fmt:input class="form-control" type="text" path="id" placeholder="id.."  readonly="true"/>
    <fmt:label path="lagu">name</fmt:label>
    <fmt:input class="form-control" type="text" path="lagu" placeholder="judul lagu.."  />
    <label>artist</label>
    <fmt:select class="form-control" path="artist">
        <fmt:option value="#">--pilih artist--</fmt:option>
        <c:forEach items="${artist_List}" var="artist">

        <fmt:option value="${artist.id}">${artist.name}</fmt:option>
        </c:forEach>
    </fmt:select>
    <br>
    <input type="submit"  class="btn btn-primary"  >

</fmt:form>
        </div>
    </div>
</div>
</body>
</html>
