package com.enigma.model;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
public class Artist {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String name;
    private String artistFrom;
    @DateTimeFormat(pattern = "yyyy-mm-dd")
    private Date debut;

    @OneToMany(mappedBy = "artist")
    private List<Song> songs;


    public Artist() {
    }

    public Artist(Integer id, String name, String artistFrom, Date debut) {
        this.id = id;
        this.name = name;
        this.artistFrom = artistFrom;
        this.debut = debut;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getArtistFrom() {
        return artistFrom;
    }

    public void setArtistFrom(String artistFrom) {
        this.artistFrom = artistFrom;
    }

    public Date getDebut() {
        return debut;
    }

    public void setDebut(Date debut) {
        this.debut = debut;
    }
}
