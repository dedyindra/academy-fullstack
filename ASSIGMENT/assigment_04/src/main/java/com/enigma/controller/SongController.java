package com.enigma.controller;

import com.enigma.model.Artist;
import com.enigma.model.Song;
import com.enigma.repositories.ArtistRepository;
import com.enigma.repositories.SongRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
public class SongController {
    @Autowired
    SongRepository songRepository;
    @Autowired
    ArtistRepository artistRepository;
    @GetMapping("/song")
    public String toViewSong(Model model){
        List<Song> songs = songRepository.findAll();
        model.addAttribute("song", songs);
        return "/artist/song";
    }

    @GetMapping("/song-form")
    public ModelAndView toformSong(Model model) {
        List<Artist> artists = artistRepository.findAll();
        model.addAttribute("artist_List", artists);
        return new ModelAndView("artist/song-form", "song", new Song());
    }
    @PostMapping("/song")
    public String toViewSong(@ModelAttribute("song") Song song, Model model) {
        songRepository.save(song);
        return "redirect:/song";
    }


}
