import expess from 'express'
import LocationRouter from "./location.route";
import PackageRouter from "./package.route";
import TransitionRouter from "./transition.route";

export default expess.Router()
.use(PackageRouter)
.use(LocationRouter)
.use(TransitionRouter)