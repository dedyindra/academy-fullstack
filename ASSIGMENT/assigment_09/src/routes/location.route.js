import {Router} from "express";
import {LocationService} from "../services";

const locationService = new LocationService();
const LocationRouter = Router()
    .get('/location',async (req,res)=>{
        try {
            const location = await locationService.findAllLocation();
            res.status(200).json(location)
        }catch (e) {
            res.status(200).json({message:e.message})
        }
    })
    .post('/location',async (req,res)=>{
        try {
            let{body}=req;
            let location = body;
            location = await locationService.createLocation(location)
            res.status(201).json(location)
            }catch (e) {
            res.status(500).json({message:e.message})
        }
    })
    .put('/location',async (req,res)=> {
        try {
            let {body} = req;
            let location = body;
            location = await locationService.updateLocation(location)
            res.status(201).json(location)
        } catch (e) {
            res.status(e.status).send(e.message)
        }
    })
       .get('/location/:id',async (req,res)=>{
           try {
               const {params} = req;
               const location = await locationService.findLocationById(params.id)
                res.status(200).json(location)
           }catch (e) {
               res.status(e.status).send(e.message)
           }
        })
    .delete('/location/:id',async (req,res)=>{
        try {
            let {params} = req;
            await locationService.deleteLocation(params.id)
            res.status(200).json({message:"delete success"})
        }catch (e) {
            res.status(e.status).send(e.message)
        }
    })


export default LocationRouter;