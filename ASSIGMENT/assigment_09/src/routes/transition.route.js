import {Router} from "express";
import {TransitionService} from "../services"
const transitionService = new TransitionService();
const TransitionRouter = Router()
    .get('/transition',async (req,res)=>{
        try {
            const transition = await transitionService.findAllTransition();
            res.status(200).json(transition)
        }catch (e) {
            res.status(500).json({message:e.message})
        }

    })
    .post('/transition',async (req,res)=>{
        try {
            let {body} =req;
            let transition = body;
            transition = await transitionService.createTransition(transition)
            res.status(201).json(transition)
        }catch (e) {
            res.status(500).json({message:e.message})
        }
    })
    .put('/transition',async (req,res)=>{
        try {
            let {body} = req;
            let transition = body;
            transition = await  transitionService.updateTransition(transition)
            res.status(200).json(transition)
        }catch (e) {
            res.status(e.status).send(e.message)
        }
    })
    .get('/transition/:id',async (req,res)=>{
        try {
            const {params} = req;
            const transition = await transitionService.findTransitionById(params.id)
            res.status(200).json(transition)
        }catch (e) {
            res.status(e.status).send(e.message)
        }
    })
    .delete('transition/:id',async (req,res)=>{
        try {
            let {params} = req;
            await transitionService.deleteTransition(params.id)
            res.status(200).json({message:" delete succes"})
        } catch (e) {
            res.status(e.status).send(e.message)
        }
    });

export default TransitionRouter;
