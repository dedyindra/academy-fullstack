import {Router} from "express";
import {PackageService} from '../services';

const packageService = new PackageService();
const PackageRouter = Router()
    .get('/package',async (req,res)=>{
        try {
            const packages = await packageService.findAllPackage();
            res.status(200).json(packages)
        }catch (e) {
            res.status(200).json({message:e.message})
        }
    })
    .post('/package',async (req,res)=>{
        try {
            let {body} = req;
            let packages = body;
            packages =await  packageService.createPackage(packages)
            res.status(201).json(packages)
        }catch (e) {
            res.status(500).json({message:e.message})
        }
    })
    .put('/package',async (req,res)=>{
        try {
            let {body} = req;
            let packages = body;
            packages = await packageService.updatePackage(packages);
            res.status(200).json(packages);
        }catch (e) {
            res.status(e.status).send(e.message)
        }
    })
    .get('/package/:id',async (req,res)=>{
        try {
            const {params} = req;
            const packages = await packageService.findPackageById(params.id)
            res.status(200).json(packages)
            }catch (e) {
            res.status(e.status).send(e.message)
        }
    })
    .delete('/package/:id',async (req,res)=>{
        try {
            let {params} = req;
            await packageService.deletePackage(params.id)
            res.status(200).json({message:"delete success"})
        }catch (e) {
            res.status(e.status).send(e.message)
        }
    })


export default PackageRouter;
