import {getRepository} from "typeorm";
import Location from "../models/location.model";

class LocationService {
    locationRepository() {
        return getRepository(Location);
    }
    async createLocation(location){
        return await this.locationRepository().save(location)
    }

    async updateLocation(location){
        let locationToUpdate = await this.findLocationById(location.id)
        this.locationRepository().merge(locationToUpdate,location)
        return await this.locationRepository().save(locationToUpdate)
    }

    async findLocationById(id){
        let location = await this.locationRepository().findOne(id)
        if (!location) throw {message:"data location is not found" , status:404}
        return location;
    }
    async findAllLocation(){
        return await this.locationRepository().find();
    }
    async deleteLocation(id){
        let location = await this.findLocationById(id)
        return await this.locationRepository().delete(location)
    }
}

export default LocationService;