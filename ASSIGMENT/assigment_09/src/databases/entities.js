import LocationSchema from "./schema/location.schema";
import PackageSchema from "./schema/package.schema";
import TransitionSchema from "./schema/transition.schema";
export default {
    LocationSchema,
    PackageSchema,
    TransitionSchema
}





