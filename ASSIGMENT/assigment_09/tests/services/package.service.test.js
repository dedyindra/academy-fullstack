import createDbConnection from "../../src/databases/connection";
import {PackageService,LocationService} from '../../src/services';
import {fail,deepEqual,equal,throws} from 'assert';
import configure from "../../src/config";

let connection;
let packageService;
let locationService;


describe('Package Service',()=> {
    beforeAll(async () => {
        configure();
        connection = await createDbConnection();
        if (!connection.isConnected) fail('unable to create database connection')
        packageService = new PackageService();
        locationService = new LocationService();
        await packageService.packageRepository().clear();
        await locationService.locationRepository().clear();

    });
    beforeEach(async () => {
        await packageService.packageRepository().clear();
        await locationService.locationRepository().clear();

    });
    it('createPackage, should able to create a package', async ()=>{
        let expectedLocation = {
            name:"jne",
            address:"jakarta"
        };
        expectedLocation =  {... await locationService.locationRepository().save(expectedLocation)};
        const expected ={
            regNumber:"20121997-JAKARTA",
            originLocation:expectedLocation.id,
            destinationLocation:expectedLocation.id,
            weight:12,
            cost:1000
        };
        const actual = {...await packageService.createPackage(expected)};
        equal(actual.regNumber,expected.regNumber)
    });
    it('getPackageById, should able to get a package', async ()=>{
        let expectedLocation = {
            name:"jne",
            address:"jakarta"
        };
        expectedLocation =  {... await locationService.locationRepository().save(expectedLocation)};
        let expected ={
            regNumber:"20121997-JAKARTA",
            originLocation:expectedLocation.id,
            destinationLocation:expectedLocation.id,
            weight:12,
            cost:1000
        };
        expected = {...await packageService.packageRepository().save(expected)};
        let actual = await packageService.findPackageById(expected.id);
        deepEqual(actual.regNumber,expected.regNumber)
    });
    it('update ,should able to update a package',async ()=>{
        let expectedLocation = {
            name:"jne",
            address:"jakarta"
        };
        expectedLocation =  {... await locationService.locationRepository().save(expectedLocation)};
        let expected ={
            regNumber:"20121997-JAKARTA",
            originLocation:expectedLocation.id,
            destinationLocation:expectedLocation.id,
            weight:12,
            cost:1000
        };
        expected =  {... await packageService.packageRepository().save(expected)};
        let dataUpdate = {...expected, regNumber:"20121997-JAKARTA"};
        let actual = await  packageService.updatePackage(dataUpdate);
        deepEqual(actual.regNumber,dataUpdate.regNumber);
    })
    it('throw exception, should be able throw when not denied data package', async ()=> {
        let data = {
            regNumber:"20121997-JAKARTA",
            weight:12,
            cost:1000
        };
        const expected = {message:"data package is not found",status:404}
        try {
            await  packageService.updatePackage(data);
        }catch (e) {
            deepEqual(expected,e)
        }
    });

    it(' should a delete  package', async ()=> {
        let expectedLocation = {
            name:"jne",
            address:"jakarta"
        };
        expectedLocation =  {... await locationService.locationRepository().save(expectedLocation)};
        let expected ={
            regNumber:"20121997-JAKARTA",
            originLocation:expectedLocation.id,
            destinationLocation:expectedLocation.id,
            weight:12,
            cost:1000
        };
        expected =  {... await packageService.packageRepository().save(expected)};
        const expectedlength = 0;
        await packageService.deletePackage(expected.id)
        let actual = await packageService.packageRepository().find();
        equal(actual.length,expectedlength)
    });
    it('get All  should a package', async ()=> {
        let expectedLocation = {
            name:"jne",
            address:"jakarta"
        };
        expectedLocation =  {... await locationService.locationRepository().save(expectedLocation)};
        let expected ={
            regNumber:"20121997-JAKARTA",
            originLocation:expectedLocation.id,
            destinationLocation:expectedLocation.id,
            weight:12,
            cost:1000
        };
        expected =  {... await packageService.packageRepository().save(expected)};
        let expectedlength = 1;
        await packageService.packageRepository().save(expected);
        let actual = await packageService.findAllPackage();
        deepEqual(actual.length,expectedlength)
    });
});