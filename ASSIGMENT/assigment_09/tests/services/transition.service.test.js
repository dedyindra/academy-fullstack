import createDbConnection from "../../src/databases/connection";
import {TransitionService,LocationService,PackageService} from "../../src/services";
import {fail,deepEqual,equal} from "assert";
import configure from "../../src/config";
import {getRepository} from "typeorm";
import Location from "../../src/models/location.model";
let connection;
let transitionService;
let packageService;
let locationService;

describe('transition Service',()=> {
    beforeAll(async () => {
        configure();
        connection = await createDbConnection();
        if (!connection.isConnected) fail('unable to create database connection')
        transitionService = new TransitionService();
        packageService = new PackageService();
        locationService = new LocationService();
        await transitionService.transitionRepository().clear();
        await locationService.locationRepository().clear();
        await packageService.packageRepository().clear();

    });
    beforeEach(async () => {
        await transitionService.transitionRepository().clear();
        await locationService.locationRepository().clear();
        await packageService.packageRepository().clear();

    });


    it('createTransition should able to create a Transition', async ()=>{
        let expectedLocation = {
            name:"jne",
            address:"jakarta"
        };
        expectedLocation =  {... await locationService.locationRepository().save(expectedLocation)};
        let expectedPackage ={
            regNumber:"20121997-JAKARTA",
            originLocation:expectedLocation.id,
            destinationLocation:expectedLocation.id,
            weight:12,
            cost:1000
        };
        expectedPackage = {...await packageService.packageRepository().save(expectedPackage)};
        const expected ={
            packageItem:expectedPackage.id,
            location:expectedLocation.id
        };
        const actual = await  transitionService.createTransition(expected);
        equal(actual,expected)
    });


    it('get by id should success', async ()=>{
        let expectedLocation = {
            name:"jne",
            address:"jakarta"
        };
        expectedLocation =  {... await locationService.locationRepository().save(expectedLocation)};
        let expectedPackage ={
            regNumber:"20121997-JAKARTA",
            originLocation:expectedLocation.id,
            destinationLocation:expectedLocation.id,
            weight:12,
            cost:1000
        };
        expectedPackage = {...await packageService.packageRepository().save(expectedPackage)};
        const expected ={
            packageItem:expectedPackage.id,
            location:expectedLocation.id
        };
        const expectedResult = await  transitionService.createTransition(expected);
        let actual = await transitionService.findTransitionById(expected.id);
        equal(actual.date,expectedResult.date)
    });

});