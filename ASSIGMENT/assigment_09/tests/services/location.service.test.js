import createDbConnection from "../../src/databases/connection";
import {LocationService} from '../../src/services';
import {fail,deepEqual,equal} from 'assert';
import configure from "../../src/config";

let connection;
let service;

describe('Location Service',()=> {
    beforeAll(async () => {
        configure();
        connection = await createDbConnection();
        if (!connection.isConnected) fail('unable to create database connection')
        service = new LocationService();
        await service.locationRepository().clear();
    });
    beforeEach(async () => {
        await service.locationRepository().clear();
       // await connection.synchronize(true)
    });
    it('post should create a location', async ()=>{
        const expected ={
            name:"jne",
            address:"jakarta"
        };
        const actual = await service.createLocation(expected);
        equal(actual.name,expected.name)
    });
    it('get application should a location', async ()=> {
        let expected ={
            name:"jne",
            address:"jakarta"
        };
        expected = {...await service.locationRepository().save(expected)};
        let actual = await service.findLocationById(expected.id);
        deepEqual(actual,expected)
    });
    it('get All application should a location', async ()=> {
        let expected ={
            name:"jne",
            address:"jakarta"
        };
        let expectedlength = 1;
        await service.locationRepository().save(expected);
        let actual = await service.findAllLocation();
        deepEqual(actual.length,expectedlength)
    });
    it('update ,should able to update a location',async ()=>{
        let data = {
            name:"jne",
            address:"jakarta"
        };
        data =  {... await service.locationRepository().save(data)};
        let dataUpdate = {...data, name:"dedy.indra"};
        let actual = await  service.updateLocation(dataUpdate);
        deepEqual(actual,dataUpdate);
    });
    it('throw exception, should be able throw when not denied data location', async ()=> {
        let data = {
            name:"jne",
            address:"jakarta"
        };
        const expected = {message:"data location is not found",status:404}
        try {
            await  service.updateLocation(data);
        }catch (e) {
            deepEqual(expected,e)
        }
    });

    it('delete application should a location', async ()=> {
        let expected ={
            name:"jne",
            address:"jakarta"
        };
        const expectedlength = 0;
        expected =   {...await service.createLocation(expected)};
         await service.deleteLocation(expected.id)
        let actual = await service.locationRepository().find();
        equal(actual.length,expectedlength)
    });

});
