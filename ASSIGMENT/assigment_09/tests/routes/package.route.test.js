import mockRequest from 'supertest'
import {app} from '../../src/app';
import {PackageService, LocationService} from "../../src/services";

let appTest;
let packageService;
let locationService;

describe('package route', () => {

    beforeAll(async () => {
        appTest = await app();
        packageService = new PackageService();
        locationService = new LocationService();
        await packageService.packageRepository().clear();
    })

    beforeEach(async () => {
        await packageService.packageRepository().clear();
    })


    it('package ,should create a package', async () => {
        let expected = {
            name: "jne",
            address: "jakarta"
        };
        let expectedLocation = {...await locationService.locationRepository().save(expected)}
        const response = await mockRequest(appTest)
            .post('/package').set('Accept', 'application/json')
            .send({
                regNumber: "20121997-JAKARTA",
                originLocation: expectedLocation.id,
                destinationLocation: expectedLocation.id,
                weight: 12,
                cost: 1000
            });
        expect(response.statusCode).toEqual(201);
        expect(response.body).toHaveProperty('id');
        expect(response.body.regNumber).toEqual('20121997-JAKARTA')
    });

    it('get should get a location by id', async () => {
        let expectedLocation = {
            name:"jne",
            address:"jakarta"
        };
        expectedLocation =  {... await locationService.locationRepository().save(expectedLocation)};
        let expected ={
            regNumber:"20121997-JAKARTA",
            originLocation:expectedLocation.id,
            destinationLocation:expectedLocation.id,
            weight:12,
            cost:1000
        };
        expected = {...await packageService.packageRepository().save(expected)};
        const response = await mockRequest(appTest)
            .get(`/package/${expected.id}`);
        expect(response.statusCode).toEqual(200);
        expect(response.body.regNumber).toEqual(expected.regNumber);
    });


    it('should update package', async ()=> {
        const location ={
            name:"jne",
            address:"jakarta"
        };
        const new_location = {...await locationService.locationRepository().save(location)}
        let expected ={
            regNumber:"20121997-JAKARTA",
            originLocation:new_location.id,
            destinationLocation:new_location.id,
            weight:12,
            cost:1000
        };
        expected = {...await packageService.packageRepository().save(expected)};
        const response = await mockRequest(appTest)
            .put('/package').set('Accept', 'application/json')
            .send({id: expected.id ,regNumber: "20121997-LAMPUNG"});
        expect(response.statusCode).toEqual(200);
        expect(response.body.regNumber).toEqual('20121997-LAMPUNG');
    });

    it('should delete all package', async ()=> {
        let location ={
            name:"jne",
            address:"jakarta"
        };
        location = {...await locationService.locationRepository().save(location)}
        let expected ={
            regNumber:"20121997-JAKARTA",
            originLocation:location.id,
            destinationLocation:location.id,
            weight:12,
            cost:1000
        };
        expected = {...await packageService.packageRepository().save(expected)};
        const response = await mockRequest(appTest)
            .delete(`/package/${expected.id}`).set('Accept', 'application/json')
        expect(response.statusCode).toEqual(200);
    });
});