import mockRequest from 'supertest'
import {app} from "../../src/app";
import LocationService from "../../src/services/location.service";

let appTest;
let service;


describe('user route', () => {
    beforeAll(async () => {
        appTest = await app();
        service = new LocationService();
        await service.locationRepository().clear();
    })

    beforeEach(async () => {
        await service.locationRepository().clear();
    })

    it('should should create a location', async ()=> {
        const response = await mockRequest(appTest)
            .post('/location').set('Accept', 'application/json')
            .send({name: "dedy.indaaa", address: "dedyindra351@gmail.com"});
        expect(response.statusCode).toEqual(201);
        expect(response.body).toHaveProperty('id');
        expect(response.body.name).toEqual('dedy.indaaa')

    });

    it('get should get a location by id', async () => {
        let expectedUser ={
            name:"jne",
            address:"jakarta"
        };
        expectedUser = {...await service.locationRepository().save(expectedUser)};
        const response = await mockRequest(appTest)
            .get(`/location/${expectedUser.id}`);
        expect(response.statusCode).toEqual(200);
        expect(response.body).toEqual(expectedUser);
    });
    it('get should get all location', async () => {
        let expectedUser ={
            name:"jne",
            address:"jakarta"
        };
      await service.locationRepository().save(expectedUser);
        const response = await mockRequest(appTest)
            .get('/location').set('Accept', 'application/json');
        expect(response.statusCode).toEqual(200);
        expect(response.body).toHaveLength(1);
    });
    it('should update location', async ()=> {
        const location ={
            name:"jne",
            address:"jakarta"
        };
        const new_location = {...await service.locationRepository().save(location)}
        const response = await mockRequest(appTest)
            .put('/location').set('Accept', 'application/json')
            .send({id: new_location.id ,name: "jne lampung"});
        expect(response.statusCode).toEqual(201);
        expect(response.body.name).toEqual('jne lampung');
    });
    it('should delete all location', async ()=> {
        let location ={
            name:"jne",
            address:"jakarta"
        };
         location = {...await service.locationRepository().save(location)}
        const response = await mockRequest(appTest)
            .delete(`/location/${location.id}`).set('Accept', 'application/json')
        expect(response.statusCode).toEqual(200);
    });
});