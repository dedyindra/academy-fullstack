import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_hapi/artist_app.dart';
import 'package:flutter_hapi/service/fetch_genre_service.dart';


GlobalKey<ScaffoldState> scaffoldState = GlobalKey();


class GenreApp extends StatefulWidget {
  static const routeName = 'genreApp';
  FetchGenre fetchGenre;

  GenreApp(this.fetchGenre);

  @override
  _GenreAppState createState() => _GenreAppState(fetchGenre);
}

class _GenreAppState extends State<GenreApp> with SingleTickerProviderStateMixin {
  FetchGenre fetchGenre;
  var genreList;
  _GenreAppState(this.fetchGenre);

  getDataGenreById() async {
    var response = await fetchGenre.getDataGenre();
    setState(() {
      genreList = List.of(jsonDecode(response.body));
    });
  }
  TabController controller;

  @override
  void initState() {
    controller = new TabController(length: 3, vsync: this);
    getDataGenreById();
  }


  @override
  void dispose() {
    controller.dispose();
  }

  Widget _buildWidgetHeaderSong() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Text(
          'Popular',
          style: TextStyle(
            color: Colors.black,
            fontWeight: FontWeight.w600,
            fontSize: 24.0,
            fontFamily: 'Campton_Light',
          ),
        ),
        Text(
          'Show all',
          style: TextStyle(
            color: Color(0xFF7D9AFF),
            fontWeight: FontWeight.w600,
            fontFamily: 'Campton_Light',
          ),
        ),
      ],
    );
  }

  Widget _buildWidgetListSong(MediaQueryData mediaQuery) {
    return Padding(
      padding: EdgeInsets.only(
        left: 20.0,
        top: mediaQuery.size.height / 3.5 + 48.0,
        right: 20.0,
        bottom: mediaQuery.padding.bottom + 16.0,
      ),
      child: Column(
        children: <Widget>[
          _buildWidgetHeaderSong(),
          SizedBox(height: 16.0),
          Expanded(
            child: ListView.builder(
              itemCount: genreList.length,
              itemBuilder: (context, index) {
                return Container(
                  decoration: new BoxDecoration(
                    boxShadow: <BoxShadow>[
                      BoxShadow(
                          color: Colors.black12,
                          blurRadius: 5.0,
                          spreadRadius: 0.5,
                          offset: Offset(0.5, 0.5)),
                    ],
                  ),
                  margin: EdgeInsets.all(15.0),
                  child: RaisedButton(
                    onPressed: () {
                      //  FetchArtist.idGenre = genreList[index]["id"];
                      Navigator.pushNamed(
                          context,
                          ArtistApp.routeName,
//                 arguments: GetDataGenre(genreList[index]["id"], genreList[index]["genre"])
                          arguments: genreList[index]["id"]
                      );
                    },
                    color: Colors.blueAccent,
                    elevation: 8.0,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(15.0)),
                    child: Stack(
                      children: <Widget>[
                        ClipRRect(
                          borderRadius: new BorderRadius.circular(8.0),
                           child:    Image.network(
                             'http://10.0.2.2/img/'+genreList[index]["id"]+'.png',fit: BoxFit.cover,height: 150,width: 500,),
                        ),
                        Text(
                          genreList[index]['genre'],
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: Colors.black,
                            fontFamily: 'CoralPen',
                            fontSize: 32.0,
                          ),
                        )
                      ],
                    ),
                  ),
                );
              },
            ),
          ),
        ],
      ),
    );
  }


  Widget _buildWidgetAlbumCover(MediaQueryData mediaQuery) {
    return Container(
      width: double.infinity,
      height: mediaQuery.size.height / 3.5,
      decoration: BoxDecoration(
        shape: BoxShape.rectangle,
        borderRadius: BorderRadius.only(
          bottomLeft: Radius.circular(48.0),
        ),
        color: Colors.blue,
        image: DecorationImage(
          image: NetworkImage('http://10.0.2.2/img/where-to-buy-spotify-gift-card-1.png'),
          fit: BoxFit.cover,
        ),
      ),
    );
  }

  Widget _buildWidgetActionAppBar(MediaQueryData mediaQuery) {
    return Padding(
      padding: EdgeInsets.only(
        left: 16.0,
        top: mediaQuery.padding.top + 16.0,
        right: 16.0,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Icon(
            Icons.menu,
            color: Colors.white,
          ),
          Icon(
            Icons.info_outline,
            color: Colors.white,
          ),
        ],
      ),
    );
  }

  Widget _buildWidgetArtistName(MediaQueryData mediaQuery) {
    return SizedBox(
      height: mediaQuery.size.height / 3.3,
      child: Padding(
        padding: const EdgeInsets.only(left: 20.0),
        child: LayoutBuilder(
          builder: (BuildContext context, BoxConstraints constraints) {
            return Stack(
              children: <Widget>[
                Positioned(
                  child: Text(
                    'Album',
                    style: TextStyle(
                      color: Colors.white,
                      fontFamily: 'CoralPen',
                      fontSize: 42.0,
                    ),
                  ),
                  top: constraints.maxHeight - 60.0,
                  right: constraints.maxWidth - 140.0,
                ),
//                Positioned(
//                  child: Text(
//                    'Album',
//                    style: TextStyle(
//                      color: Colors.white,
//                      fontFamily: 'CoralPen',
//                      fontSize: 42.0,
//                    ),
//                  ),
//                  top: constraints.maxHeight - 140.0,
//                ),

              ],
            );
          },
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    var mediaQuery = MediaQuery.of(context);

    return Scaffold(
        key: scaffoldState,
        body: Stack(
          children: <Widget>[
            _buildWidgetAlbumCover(mediaQuery),
            _buildWidgetActionAppBar(mediaQuery),
            _buildWidgetArtistName(mediaQuery),
            _buildWidgetListSong(mediaQuery),

          ],

        ),
        bottomNavigationBar: new Material(
        color: Colors.blue,
      child: new TabBar(
        controller: controller,
          tabs: <Widget>[
            Tab(icon: Icon(Icons.radio),),
            Tab(icon: Icon(Icons.radio),),
            Tab(icon: Icon(Icons.radio),)
          ]
      ) ,
    ) ,
    );
  }
}
