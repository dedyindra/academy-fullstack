import 'package:flutter/material.dart';
import 'package:flutter_hapi/artist_app.dart';
import 'package:flutter_hapi/genre_app.dart';
import 'package:flutter_hapi/login_app.dart';
import 'package:flutter_hapi/service/fetch_arttist_service.dart';
import 'package:flutter_hapi/service/fetch_genre_service.dart';
import 'package:flutter_hapi/service/fetch_song_service.dart';
import 'package:flutter_hapi/song_app.dart';

void main() => runApp(
  MaterialApp(
    debugShowCheckedModeBanner: false,
    title: 'Artis Management',
    initialRoute: LoginPage.routeName,
    onGenerateRoute: (RouteSettings settings) {
      var routes = <String, WidgetBuilder>{
        GenreApp.routeName:(context)=> GenreApp(FetchGenre()),
        ArtistApp.routeName :(context)=> ArtistApp(FetchArtist(),settings.arguments),
        SongApp.routeName: (context)=> SongApp(FetchSong(),settings.arguments),
        LoginPage.routeName:(context)=>LoginPage()
      };

      WidgetBuilder widgetBuilder = routes[settings.name];
      return MaterialPageRoute(builder: (context) => widgetBuilder(context));
    },
  ),
);
