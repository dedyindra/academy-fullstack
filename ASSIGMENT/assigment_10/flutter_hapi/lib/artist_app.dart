import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_hapi/service/fetch_arttist_service.dart';
import 'package:flutter_hapi/song_app.dart';


GlobalKey<ScaffoldState> scaffoldState = GlobalKey();


class ArtistApp extends StatefulWidget {
  static const routeName = 'artistApp';
  FetchArtist fetchArtist;
  String idGenre;
  ArtistApp(this.fetchArtist,this.idGenre);

  @override
  _ArtistAppState createState() => _ArtistAppState(fetchArtist,idGenre);
}

class _ArtistAppState extends State<ArtistApp> {
  FetchArtist fetchArtist;
  var artistList = List();
  String idGenre;
  _ArtistAppState(this.fetchArtist,this.idGenre);

  getDataArtistById() async {
    var response = await fetchArtist.getDataArtist(idGenre);
    setState(() {
      artistList = List.of(jsonDecode(response.body));
    });
  }

  @override
  void initState() {
    getDataArtistById();
  }



  Widget _buildWidgetHeaderSong() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Text(
          'Popular',
          style: TextStyle(
            color: Colors.black,
            fontWeight: FontWeight.w600,
            fontSize: 24.0,
            fontFamily: 'Campton_Light',
          ),
        ),
        Text(
          'Show all',
          style: TextStyle(
            color: Color(0xFF7D9AFF),
            fontWeight: FontWeight.w600,
            fontFamily: 'Campton_Light',
          ),
        ),
      ],
    );
  }

  Widget _buildWidgetListSong(MediaQueryData mediaQuery) {
    return Padding(
      padding: EdgeInsets.only(
        left: 20.0,
        top: mediaQuery.size.height / 3.5 + 48.0,
        right: 20.0,
        bottom: mediaQuery.padding.bottom + 16.0,
      ),
      child: Column(
        children: <Widget>[
          _buildWidgetHeaderSong(),
          SizedBox(height: 16.0),
          Expanded(
              child: GridView.count(
                  primary: false,
                  padding: const EdgeInsets.all(20),
                  crossAxisSpacing: 10,
                  mainAxisSpacing: 10,
                  crossAxisCount: 2,
                  children: List.generate(artistList.length, (index) {
                    return Container(
                      child: RaisedButton(
                        onPressed: () {
                          //    FetchSong.artistIds = artistList[index]["id"];
                          Navigator.pushNamed(context,SongApp.routeName,arguments: artistList[index]["id"]
                          );
                        },
                        color: Colors.blue,
                        elevation: 8.0,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(15.0)),
                        child: Stack(
                          children: <Widget>[
                            ClipRRect(
                                  borderRadius: new BorderRadius.circular(8.0),
                                  child: Image.network('http://10.0.2.2/img/' +
                                      artistList[index]["id"] +
                                      '.png',fit: BoxFit.cover,height: 150,width: 500,) ,

                                ),
                            Text(artistList[index]["name"], textAlign: TextAlign.center,
                              style: TextStyle(
                              fontSize: 27.0,color: Colors.black,
                            ),),
                          ],
                        ),
                        padding: const EdgeInsets.all(8),
                      ),
                    );
                  }))
          ),
        ],
      ),
    );
  }


  Widget _buildWidgetAlbumCover(MediaQueryData mediaQuery) {
    return Container(
      width: double.infinity,
      height: mediaQuery.size.height / 3.5,
      decoration: BoxDecoration(
        shape: BoxShape.rectangle,
        borderRadius: BorderRadius.only(
          bottomLeft: Radius.circular(48.0),
        ),
        color: Colors.blue,
        image: DecorationImage(
          image: NetworkImage('http://10.0.2.2/img/' +
              idGenre +
              '.png') ,
          fit: BoxFit.cover,
        ),
      ),
    );
  }

  Widget _buildWidgetActionAppBar(MediaQueryData mediaQuery) {
    return Padding(
      padding: EdgeInsets.only(
        left: 16.0,
        top: mediaQuery.padding.top + 16.0,
        right: 16.0,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Icon(
            Icons.menu,
            color: Colors.white,
          ),
          Icon(
            Icons.info_outline,
            color: Colors.white,
          ),
        ],
      ),
    );
  }

  Widget _buildWidgetArtistName(MediaQueryData mediaQuery) {
    return SizedBox(
      height: mediaQuery.size.height / 3.3,
      child: Padding(
        padding: const EdgeInsets.only(left: 20.0),
        child: LayoutBuilder(
          builder: (BuildContext context, BoxConstraints constraints) {
            return Stack(
              children: <Widget>[
                Positioned(
                  child: Text(
                    'Music',
                    style: TextStyle(
                      color: Colors.white,
                      fontFamily: 'CoralPen',
                      fontSize: 42.0,
                    ),
                  ),
                  top: constraints.maxHeight - 100.0,
                  right: constraints.maxWidth - 250.0,
                ),
                Positioned(
                  child: Text(
                    'Artist',
                    style: TextStyle(
                      color: Colors.white,
                      fontFamily: 'CoralPen',
                      fontSize: 42.0,
                    ),
                  ),
                  top: constraints.maxHeight - 140.0,
                  right: constraints.maxWidth - 240.0,

                ),

              ],
            );
          },
        ),
      ),
    );
  }


  @override
  Widget build(BuildContext context) {
    var mediaQuery = MediaQuery.of(context);

    return Scaffold(
        key: scaffoldState,
        body: Stack(
          children: <Widget>[
            _buildWidgetAlbumCover(mediaQuery),
            _buildWidgetActionAppBar(mediaQuery),
            _buildWidgetArtistName(mediaQuery),
            _buildWidgetListSong(mediaQuery),
          ],
        )
    );
  }
}

