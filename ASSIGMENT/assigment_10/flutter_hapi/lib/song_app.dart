import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_hapi/service/fetch_song_service.dart';


GlobalKey<ScaffoldState> scaffoldState = GlobalKey();


class SongApp extends StatefulWidget {
  static const routeName = 'songApp';
  FetchSong fetchSong;
  String artistId;
  SongApp(this.fetchSong,this.artistId);

  @override
  _SongAppState createState() => _SongAppState(fetchSong,artistId);
}

class _SongAppState extends State<SongApp> {
  var songList = List();
  String artistId;
  FetchSong fetchSong;
  _SongAppState(this.fetchSong,this.artistId);

  getDataSongById() async {
    var response = await fetchSong.getDataSong(artistId);
    setState(() {
      songList = List.of(jsonDecode(response.body));
    });
  }

  @override
  void initState(){
    getDataSongById(); }

  Widget _buildWidgetHeaderSong() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Text(
          'Popular',
          style: TextStyle(
            color: Colors.black,
            fontWeight: FontWeight.w600,
            fontSize: 24.0,
            fontFamily: 'Campton_Light',
          ),
        ),
        Text(
          'Show all',
          style: TextStyle(
            color: Color(0xFF7D9AFF),
            fontWeight: FontWeight.w600,
            fontFamily: 'Campton_Light',
          ),
        ),
      ],
    );
  }

  Widget _buildWidgetListSong(MediaQueryData mediaQuery) {
    return Padding(
      padding: EdgeInsets.only(
        left: 20.0,
        top: mediaQuery.size.height / 1.8 + 48.0,
        right: 20.0,
        bottom: mediaQuery.padding.bottom + 16.0,
      ),
      child: Column(
        children: <Widget>[
          _buildWidgetHeaderSong(),
          SizedBox(height: 16.0),
          Expanded(
            child: ListView.separated(
              padding: EdgeInsets.zero,
              itemBuilder: (BuildContext context, int index) {

                return GestureDetector(
                  onTap: () {
                    // TODO: item song arahkan ke UI Music Player
                  },
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        child: Text(
                    songList[index]["nameLagu"],
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                            fontFamily: 'Campton_Light',
                          ),
                          overflow: TextOverflow.ellipsis,
                        ),
                      ),
                      Text(
                        songList[index]["duration"],
                        style: TextStyle(
                          color: Colors.grey,
                        ),
                      ),
                      SizedBox(width: 24.0),
                      Icon(Icons.more_horiz, color: Colors.grey,),
                    ],
                  ),
                );
              },
              separatorBuilder: (BuildContext context, int index) {
                return Opacity(
                  opacity: 0.5,
                  child: Padding(
                    padding: const EdgeInsets.only(top: 2.0),
                    child: Divider(
                      color: Colors.grey,
                    ),
                  ),
                );
              },
              itemCount: songList.length,
            ),
          ),
        ],
      ),
    );
  }


  Widget _buildWidgetAlbumCover(MediaQueryData mediaQuery) {
    return Container(
      width: double.infinity,
      height: mediaQuery.size.height / 1.8,
      decoration: BoxDecoration(
        shape: BoxShape.rectangle,
        borderRadius: BorderRadius.only(
          bottomLeft: Radius.circular(48.0),
        ),
        color: Colors.blue,
        image: DecorationImage(
          image: NetworkImage('http://10.0.2.2/img/' +
              artistId +
              '.png') ,
          fit: BoxFit.cover,
        ),
      ),
    );
  }

  Widget _buildWidgetActionAppBar(MediaQueryData mediaQuery) {
    return Padding(
      padding: EdgeInsets.only(
        left: 16.0,
        top: mediaQuery.padding.top + 16.0,
        right: 16.0,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Icon(
            Icons.menu,
            color: Colors.white,
          ),
          Icon(
            Icons.info_outline,
            color: Colors.white,
          ),
        ],
      ),
    );
  }

  Widget _buildWidgetArtistName(MediaQueryData mediaQuery) {
    return SizedBox(
      height: mediaQuery.size.height / 1.8,
      child: Padding(
        padding: const EdgeInsets.only(left: 20.0),
        child: LayoutBuilder(
          builder: (BuildContext context, BoxConstraints constraints) {
            return Stack(
              children: <Widget>[
                Positioned(
                  child: Text(
                    'App',
                    style: TextStyle(
                      color: Colors.white,
                      fontFamily: 'CoralPen',
                      fontSize: 42.0,
                    ),
                  ),
                  top: constraints.maxHeight - 100.0,
                ),
                Positioned(
                  child: Text(
                    'Music',
                    style: TextStyle(
                      color: Colors.white,
                      fontFamily: 'CoralPen',
                      fontSize: 42.0,
                    ),
                  ),
                  top: constraints.maxHeight - 140.0,
                ),
                Positioned(
                  child: Text(
                    'enigma',
                    style: TextStyle(
                      color: Color(0xFF7D9AFF),
                      fontSize: 14.0,
                      fontFamily: 'Campton_Light',
                      fontWeight: FontWeight.w800,
                    ),
                  ),
                  top: constraints.maxHeight - 160.0,
                ),
              ],
            );
          },
        ),
      ),
    );
  }


  Widget _buildWidgetFloatingActionButton(MediaQueryData mediaQuery) {
    return Align(
      alignment: Alignment.topRight,
      child: Padding(
        padding: EdgeInsets.only(
          top: mediaQuery.size.height / 1.8 - 32.0,
          right: 32.0,
        ),
        child: FloatingActionButton(
          child: Icon(
            Icons.play_arrow,
            color: Colors.white,
          ),
          backgroundColor: Color(0xFF7D9AFF),
          onPressed: () {
            // TODO: arahkan ke UI Music Player
          },
        ),
      ),
    );
  }
  @override
  Widget build(BuildContext context) {
    var mediaQuery = MediaQuery.of(context);

    return Scaffold(
        key: scaffoldState,
        body: Stack(
          children: <Widget>[
            _buildWidgetAlbumCover(mediaQuery),
            _buildWidgetActionAppBar(mediaQuery),
            _buildWidgetArtistName(mediaQuery),
            _buildWidgetFloatingActionButton(mediaQuery),
            _buildWidgetListSong(mediaQuery),
          ],
        )
    );
  }
}
