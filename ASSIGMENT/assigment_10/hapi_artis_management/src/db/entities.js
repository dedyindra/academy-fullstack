import ArtistSchema from "../schema/artist.schema";
import GenreSchema from "../schema/genre.schema";
import SongSchema from "../schema/song.schema";
import LoginSchema from "../schema/login.schema";

export default {
    ArtistSchema,
    GenreSchema,
    SongSchema,
    LoginSchema
}