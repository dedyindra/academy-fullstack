class ArtistModel {

    constructor(id,bornPlace,debut,name,genreId) {
    this.id = id;
    this.bornPlace = bornPlace;
    this.debut = debut;
    this.name=name;
    this.genreId=genreId;
    }
}
export default ArtistModel;