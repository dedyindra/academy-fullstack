class SongModel {

    constructor(id,duration,songName,artisId) {
    this.id = id;
    this.duration = duration;
    this.songName = songName;
    this.artisId=artisId;
    }
}

export default SongModel;