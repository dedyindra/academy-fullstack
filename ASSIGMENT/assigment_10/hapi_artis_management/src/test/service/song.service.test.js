import SongService from "../../service/song.service";
import ArtistService from "../../service/artist.service";
import GenreService from "../../service/genre.service";
import init from '../../index';

const songService = new SongService();
const genreService = new GenreService();
const artistService = new ArtistService();
const dataGenre ={
    genre: "POP"
}
const dataSongs =[{
    nameLagu: "rindu kamu",
    duration :"05:00"
},{
    nameLagu: "kenangan terindah",
    duration :"05:00"
}];

let  server;
describe('create song',()=>{
    beforeEach(async () => {
        server = await init();
        //   await locationService.locationRepository().query('SET FOREIGN_KEY_CHECKS=0')
        await songService.songRepository().clear();
        // await locationService.locationRepository().query('SET FOREIGN_KEY_CHECKS=1')
    });
    it('should post data song', async ()=> {
        const genre = await genreService.genreRepository().save(dataGenre);
        const dataArtist = {
            bornPlace: "jak",
            debut: "2019-12-03 00:00:00",
            name: "st 13",
            genre:genre.id
        };
        const artist = await artistService.artistRepository().save(dataArtist);
        const dataSong ={
            nameLagu: "rindu kamu",
            duration :"05:00",
            artist:artist.id
        };
        const song = await songService.createSong(dataSong);
        const result = await songService.songRepository().findOne(song.id);
        expect(result.nameLagu).toEqual(song.nameLagu)
    });

    afterEach(async () => {
        if(server){
            server.close();
        }
    });
})
