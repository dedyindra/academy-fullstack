import GenreService from "../../service/genre.service";
import init from '../../index'
const genreService = new GenreService();

const dataGenre ={
    genre: "POP"
};
const dataGenres =[{
    genre: "POP"
},{
    genre: "ROCK"
}];
let  server;
describe('create genre',()=>{
    beforeEach(async () => {
        server = await init();
        //   await locationService.locationRepository().query('SET FOREIGN_KEY_CHECKS=0')
        await genreService.genreRepository().clear();
        // await locationService.locationRepository().query('SET FOREIGN_KEY_CHECKS=1')
    });
    it('should post data genre', async ()=> {
        const genre = await genreService.createGenre(dataGenre);
        const result = await genreService.genreRepository().findOne(genre.id);
        expect(result).toMatchObject(genre)
    });

    afterEach(async () => {
        if(server){
            server.close();
        }
    });
})

describe('find genre',()=>{
    beforeEach(async () => {
      //  server = await init();
        await genreService.genreRepository().clear();
    });
    it('should get data genre by id', async ()=> {
        const  {id,genre} = await genreService.genreRepository().save(dataGenre);
        const genres = await genreService.findGenreById(id);
        expect(genres).toMatchObject({
            id,
            genre
        })
    });

    it('should get all data genre', async ()=> {
        let saveGenre=[];
        for (let i = 1; i < dataGenres.length ; i++) {
            saveGenre.push(await genreService.genreRepository().save(dataGenres[i]))
        }
        const genre = await genreService.findAllGenre();
        expect(genre).toHaveLength(saveGenre.length);
    });
    afterEach(async () => {
        if(server){
            server.close();
        }
    });
})