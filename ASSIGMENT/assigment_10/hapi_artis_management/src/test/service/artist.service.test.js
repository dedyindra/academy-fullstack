import ArtistService from "../../service/artist.service";
import GenreService from "../../service/genre.service";
import init from '../../index'
const artistService =new ArtistService();
const genreService = new GenreService();
const dataGenre ={
    genre: "POP"
};
let server;
describe('create artist',()=>{
    beforeEach(async () => {
        server = await init();
        await artistService.artistRepository().clear();
    });
    it('should post data artist', async ()=> {
        const genre = await genreService.genreRepository().save(dataGenre);
        const dataArtist = {
            bornPlace: "jakarta",
            debut: "2019-12-03 00:00:00",
            name: "st 13",
            genre:genre.id
        };
        const artist = await artistService.createArtist(dataArtist);
        const result = await artistService.artistRepository().findOne(artist.id);
        expect(result.name).toEqual(artist.name)
    });

    afterEach(async () => {
        if(server){
            server.close();
        }
    });
})

describe('find artist',()=>{
    beforeEach(async () => {
        //  server = await init();
        await artistService.artistRepository().clear();
    });


    it('should get data artist by id', async ()=> {
        const genres = await genreService.genreRepository().save(dataGenre);
        const dataArtist = {
            bornPlace: "jakarta",
            debut: "2019-12-03 00:00:00",
            name: "st 13",
            genre:genres.id
        };
        const  {id,name} = await artistService.artistRepository().save(dataArtist);
        const artist = await artistService.findArtistById(id);
        expect(artist).toMatchObject({
            id,
            name})
    });

    it('should get all data artist', async ()=> {
        let saveArtist=[];
        const genres = await genreService.genreRepository().save(dataGenre);
        const dataArtist = [{
            bornPlace: "jakarta",
            debut: "2019-12-03 00:00:00",
            name: "st 13",
            genre:genres.id
        },
            {
                bornPlace: "jakarta",
                debut: "2019-12-03 00:00:00",
                name: "st 13",
                genre:genres.id
            }];
        for (let i = 1; i < dataArtist.length ; i++) {
            saveArtist.push(await artistService.artistRepository().save(dataArtist[i]))
        }
        const artist = await artistService.findAllArtist();
        expect(artist).toHaveLength(saveArtist.length);
    });
    afterEach(async () => {
        if(server){
            server.close();
        }
    });
})
