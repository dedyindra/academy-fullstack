import request from 'supertest';
import mockRequest from  'supertest'
import GenreService from "../../service/genre.service";
import init from "../../index"


const genreService = new GenreService();
const dataGenre ={
    genre: "POP"
};
let server;
describe('genre route',()=>{
    beforeAll(async () => {
        server = await init();
        await genreService.genreRepository().clear();
    });

    beforeEach(async () => {
       // server = await init();
        await genreService.genreRepository().clear();
    });

    it('post should create a genre', async () => {
       // const response = await request(server).post(`/genre`) .send(dataGenre);
        const response = await mockRequest(server)
            .post('/genre').set('Accept', 'application/x-www-form-urlencoded')
            .send(dataGenre);
        expect(response.statusCode).toEqual(200);
        expect(response.payload).toHaveProperty('id');
        expect(response.payload.genre).toEqual('POP')
    });

    afterEach(async () => {
        if(server){
            server.close();
        }
    });
});