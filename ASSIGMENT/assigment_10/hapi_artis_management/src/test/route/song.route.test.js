import request from 'supertest';
import mockRequest from  'supertest'
import ArtistService from "../../service/artist.service";
import GenreService from "../../service/genre.service";
import SongService from "../../service/song.service";
import init from "../../index"

const genreService = new GenreService();
const artistService = new ArtistService();
const songService = new SongService();
const dataGenre ={
    genre: "POP"
};
let server;
describe('song sould create route',()=>{
    beforeAll(async () => {
        server = await init();
        await songService.songRepository().clear();
    });

    beforeEach(async () => {
        // server = await init();
        await songService.songRepository().clear();
    });

    it('post should create a song', async () => {
        const genre = await genreService.genreRepository().save(dataGenre);
        const dataArtist = {
            bornPlace: "jakarta",
            debut: "2019-12-03 00:00:00",
            name: "st 12",
            genre:genre.id
        };
        const artist = await artistService.artistRepository().save(dataArtist);
        const dataSong ={
            nameLagu: "kisah klasik",
            duration :"05:00",
            artist:artist.id
        };
        const response = await request(server).post(`/song`) .send(dataSong);
        expect(response.statusCode).toEqual(200);
        // expect(response.payload).toHaveProperty('id');
         expect(response.payload.nameLagu).toEqual('kisah klasik')
    });

    afterEach(async () => {
        if(server){
            server.close();
        }
    });
});