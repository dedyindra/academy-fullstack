import request from 'supertest';
import mockRequest from  'supertest'
import ArtistService from "../../service/artist.service";
import GenreService from "../../service/genre.service";
import init from "../../index"

const genreService = new GenreService();
const artistService = new ArtistService();
const dataGenre ={
    genre: "POP"
};
let server;
describe('artist create route',()=>{
    beforeAll(async () => {
        server = await init();
        await artistService.artistRepository().clear();
    });

    beforeEach(async () => {
        // server = await init();
        await artistService.artistRepository().clear();
    });

    it('post should create a artist', async () => {
        const genre = await genreService.genreRepository().save(dataGenre);
        const dataArtist = {
            bornPlace: "jakarta",
            debut: "2019-12-03 00:00:00",
            name: "st 12",
            genre:genre.id
        };
         const response = await request(server).post(`/artist`) .send(dataArtist);
        expect(response.statusCode).toEqual(200);
        expect(response.payload).toHaveProperty('id');
        expect(response.payload.name).toEqual('st 12')
    });

    afterEach(async () => {
        if(server){
            server.close();
        }
    });
});