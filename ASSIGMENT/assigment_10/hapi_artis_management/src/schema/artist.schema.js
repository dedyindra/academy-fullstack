import {EntitySchema} from "typeorm";
import ArtistModel from "../models/artist.model";
const ArtistSchema = new EntitySchema({
    name:'Artist',
    target: ArtistModel,
    tableName:'artist',
    columns:{
        id:{
            primary:true,
            type:"uuid",
            generated:"uuid",
            nullable:false
        },
        name:{
            type: "varchar",
            nullable:false
        },
        bornPlace:{
            type: "varchar",
            nullable:false
        },
        debut:{
            type:"datetime",
            nullable:false
        }
    },
    relations: {
        genre: {
            target: "GenreModel",
            type: "many-to-one",
            joinColumn: true,
            eager: true,
            nullable:false
        },
        song:{
            target:"SongModel",
            type:"one-to-many",
            inverseSide:"artist",
            joinColumn:true,
            cascade:true,
            eager:false,
        }
    }
});

export default ArtistSchema;