import {EntitySchema} from "typeorm";
import GenreModel from "../models/genre.model";

const GenreSchema = new EntitySchema({
    name:'Genre',
    target: GenreModel,
    tableName:'genre',
    columns:{
        id:{
            primary:true,
            type:"uuid",
            generated:"uuid",
            nullable:false
        },
        genre:{
            type: "varchar",
            nullable:false
        }
    },
    relations:{
        artist:{
            target:"ArtistModel",
            type:"one-to-many",
            inverseSide:"genre",
            joinColumn:true,
            cascade:true,
            eager:false
        }
    }
});

export default GenreSchema;