import {EntitySchema} from "typeorm";
import SongModel from "../models/song.model";
const SongSchema = new EntitySchema({
    name:'Song',
    target: SongModel,
    tableName:'song',
    columns:{
        id:{
            primary:true,
            type:"uuid",
            generated:"uuid",
            nullable:false
        },
        nameLagu:{
            type: "varchar",
            nullable:false
        },
        duration:{
            type: "varchar",
            nullable:false
        }
    },
    relations: {
        artist: {
            target: "ArtistModel",
            type: "many-to-one",
            joinColumn: true,
            eager: true,
            nullable:false
        }
    }
});

export default SongSchema;