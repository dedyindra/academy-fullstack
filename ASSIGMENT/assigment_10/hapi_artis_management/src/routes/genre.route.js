import {GenreService} from "../service";
import Boom from "@hapi/boom";
import Joi from "@hapi/joi";
import fs from "file-system";

const genreService = new GenreService();
const GenreRouter =[
    {
        method: 'POST',
        path: '/genre',
            handler: async (request, h) => {
                let data = request.payload;
                let genre =   JSON.parse(data.genre);
                genre = await genreService.createGenre(genre)
                //console.log(data.genre.id);
              //  console.log(data);
                if (data.file) {
                   // const name = data.file.hapi.filename;
                    const  namaFile = genre.id;
               //     const path = __dirname + "/" + namaFile +".png";
                    const path = "E:\\xampp\\htdocs\\img\\" +namaFile+ ".png";
                    const file = fs.createWriteStream(path);
                    file.on('error', (err) => console.error(err));
                    data.file.pipe(file);
                    data.file.on('end', (err) => {
                        const ret = {
                            filename: data.file.hapi.filename,
                            headers: data.file.hapi.headers
                        };
                        return JSON.stringify(ret);
                    })
                }
                if (!genre) {
                    throw  Boom.notFound('song not found');
                } else {
                    return genre;
                }
            },
            options: {
                payload: {
                    output: 'stream',
                    parse: true,
                    allow: 'multipart/form-data'
                }
            }
            },
    {
        method: 'GET',
        path: '/genres',
        config: {
            // auth:'simple',
            handler: async (req, h) => {
                const genre = await genreService.findAllGenre();
                if (!genre) {
                    throw  Boom.notFound('customer not found');
                } else {
                    return genre;
                }

            }

        }
    }
];
export default GenreRouter;