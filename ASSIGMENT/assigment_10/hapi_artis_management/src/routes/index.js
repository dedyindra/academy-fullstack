import ArtistRouter from "./artist.route";
import GenreRouter from "./genre.route";
import SongRoute from "./song.route";
import LoginRoute from "./login.route";

export default [].concat(
    ArtistRouter,
    GenreRouter,
    SongRoute,
    LoginRoute
)
