import {ArtistService} from "../service";
import Boom from "@hapi/boom";
import Joi from "@hapi/joi";
import fs from "file-system";

const artistService = new ArtistService();
const ArtistRouter = [
    {
        method: 'POST',
        path: '/artist',
        handler: async (request, h) => {
            let data = request.payload;
            let artist =   JSON.parse(data.artist);
            artist = await artistService.createArtist(artist)
            if (data.file) {
                const  namaFile = artist.id;
                const path = "E:\\xampp\\htdocs\\img\\" +namaFile+ ".png";
                const file = fs.createWriteStream(path);
                file.on('error', (err) => console.error(err));
                data.file.pipe(file);
                data.file.on('end', (err) => {
                    const ret = {
                        filename: data.file.hapi.filename,
                        headers: data.file.hapi.headers
                    };
                    return JSON.stringify(ret);
                })
            }
            if (!artist) {
                throw  Boom.notFound('song not found');
            } else {
                return artist;
            }
        },
        options: {
            payload: {
                output: 'stream',
                parse: true,
                allow: 'multipart/form-data'
            }
        }
    },
    {
        method: 'GET',
        path: '/artists',
        config: {
            // auth:'simple',
            handler: async (req, h) => {
                const artist = await artistService.findAllArtist();
                if (!artist) {
                    throw  Boom.notFound('artist not found');
                } else {
                    return artist;
                }
            }

        }
    },
    {
        method: 'GET',
        path: '/artist/{id}',
        config: {
            // auth:'simple',
            handler: async (req, h) => {
                const {params} = req;
                const artist = await artistService.findArtistByGenreId(params.id);
                if (!artist) {
                    throw  Boom.notFound('artist not found');
                } else {
                    return artist;
                }
            }
        }
    },
];
export default ArtistRouter;