import {SongService} from "../service";
import Boom from "@hapi/boom";
import Joi from "@hapi/joi";

const songService = new SongService();
const SongRoute = [
    {
        method: 'POST',
        path: '/song',
        config:{
            handler: async (req, h) => {
                let song = req.payload;
                song = await songService.createSong(song)
                if (!song) {
                    throw  Boom.notFound('song not found');
                } else {
                    return song;
                }

            },
        }
    },
    {
        method: 'GET',
        path: '/songs',
        config: {
            // auth:'simple',
            handler: async (req, h) => {
                const song = await songService.findAllSong();
                if (!song) {
                    throw  Boom.notFound('song not found');
                } else {
                    return song;
                }
            }

        }
    },
    {
        method: 'GET',
        path: '/song/{id}',
        config: {
            // auth:'simple',
            handler: async (req, h) => {
                const {params} = req;
                const song = await songService.findSongByArtistId(params.id);
                if (!song) {
                    throw  Boom.notFound('artist not found');
                } else {
                    return song;
                }
            }
        }
    }
];
export default SongRoute;