import {getRepository} from "typeorm";
import Login from "../models/login.model";
import bcrypt  from 'bcryptjs';
import Boom from '@hapi/boom'

export default class LoginService {

    loginRepository() {
        return getRepository(Login);
    }
    async beforeCreate(password){
    const salt = bcrypt.genSaltSync();
    return  bcrypt.hashSync(password,salt)
    }
    async validate  (request,username,password){
        const user = await new LoginService().findByUsername(username);

        if (!user){
            return {credentials:null,isValid:false}
        }

        const isValid= await new LoginService().validPassword(password,user.password);
        const credentials = {id:user.id,username:user.username}
        return {isValid,credentials}
    };
    async validPassword(password,checkPassword){
        return  bcrypt.compareSync(password,checkPassword)
    }

    async validateCookie(request, session) {
        const user = await  this.loginRepository().findOne(session.id);
        if (!user) return { valid: false };

        return { valid: true, credentials: user };
    }

    async updatePassword(data){
        const id =`${data.id}`;
        let datanya = this.loginRepository().findOne(id) ;
        if(datanya){
            const {password} = data;
            data.password = await this.beforeCreate(password);
            return this.loginRepository().save(data);
        }
    }
    async createPassword(data){
        const {password} = data;
        data.password = await this.beforeCreate(password);
        return this.loginRepository().save(data);
    }

    async findByUsername(username){
        return await this.loginRepository().findOne({username});
    }

    async login(data){
        const {username,password}=data;
        const user = await this.findByUsername(username);
        if (user && await this.validPassword(password,user.password)){
            return user;
        }else {
            throw Boom.unauthorized('Invalid Username or Password')
        }
    }
}