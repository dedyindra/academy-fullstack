import {getRepository, Like} from "typeorm";
import Boom from "@hapi/boom";
import SongModel from "../models/song.model";
class SongService {

    songRepository() {
        return getRepository(SongModel);
    }
    async createSong(song){
        return await this.songRepository().save(song);
    }
    async findAllSong(){
        return await this.songRepository().find();
    }
    async findSongById(id){
        let song = await  this.songRepository().findOne(id);
        if (!song) throw Boom.notFound('song not found');
        return song;
    }
    async updateSong(song){
        let songToUpdate =await this.findSongById(song.id);
        this.songRepository().merge(songToUpdate,song);
        return await this.songRepository().save(songToUpdate);
    }
    async deleteSong(id){
        let song = await this.findSongById(id);
        return await this.songRepository().delete(song);
    }
    async findSongByArtistId(id) {
        let song = await this.songRepository().find({artist: Like(`%${id}%`)});
        if (song === undefined) {
            return  {message: "data tidak ada", status: 404}
        }
        return song;
    }
}

export default SongService;