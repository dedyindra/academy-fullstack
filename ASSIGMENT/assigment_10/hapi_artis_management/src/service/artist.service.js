import {getRepository, Like} from "typeorm";
import Boom from "@hapi/boom";
import ArtistModel from "../models/artist.model";
class ArtistService {

    artistRepository() {
        return getRepository(ArtistModel);
    }
    async createArtist(artist){
        return await this.artistRepository().save(artist);
    }
    async findAllArtist(){
        return await this.artistRepository().find()
    }
    async findArtistById(id){
        let artist = await this.artistRepository().findOne(id);
        if (!artist) throw Boom.notFound('artist not found')
        return artist;
    }
    async updateArtist(artist){
        let artistToUpdate = await this.findArtistById(artist.id);
        this.artistRepository().merge(artistToUpdate,artist);
        return await this.artistRepository().save(artistToUpdate);
    }
    async deleteArtist(id){
        let artist = await this.findArtistById(id);
        return await this.artistRepository().delete(artist);
    }

    async findArtistByGenreId(id) {
        let artist = await this.artistRepository().find({genre: Like(`%${id}%`)});
        if (artist === undefined) {
            return  {message: "data tidak ada", status: 404}
        }
        return artist;
    }
}

export default ArtistService;