import ArtistService from "./artist.service";
import GenreService from "./genre.service";
import SongService from "./song.service";

export {
    ArtistService,
    GenreService,
    SongService
}