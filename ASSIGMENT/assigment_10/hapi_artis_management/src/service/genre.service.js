import {getRepository} from "typeorm";
import Boom from "@hapi/boom";
import GenreModel from "../models/genre.model";
class GenreService {

    genreRepository() {
        return getRepository(GenreModel);
    }
    async createGenre(genre){
        return await this.genreRepository().save(genre)
    }
    async findAllGenre(){
        return await this.genreRepository().find();
    }
    async findGenreById(id){
        let genre = await this.genreRepository().findOne(id);
        if (!genre) throw Boom.notFound('Genre Not Found')
        return genre;
    }
    async updateGenre(genre){
        let genreToUpdate = await this.findGenreById(genre.id);
        this.genreRepository().merge( genreToUpdate,genre);
        return await this.genreRepository().save(genreToUpdate);
    }
    async deleteGenre(id){
        let genre = await this.findGenreById(id);
        return await this.genreRepository().delete(genre);
    }
}

export default GenreService;