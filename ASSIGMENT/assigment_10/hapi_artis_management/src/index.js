import createConnection from './db/connection';
import Hapi from '@hapi/hapi'
import Config from './config';
 import routes from './routes';
import validate from "./config/auth.validate";

process.on('unhandledRejection', (err) => {
    console.log(err);
    process.exit(1);
});

export default async () => {
    const config = new Config();
    config.configure();


    const connection = await createConnection();
    const server = Hapi.server({
        port: process.env.APP_PORT,
        host: process.env.APP_HOST
    });

   // config.getAuthConfig()
   //  const plugins = getPlugins({
   //      authPlugin: config.getAuthConfig(),
   //      basicAuthPlugin: config.getAuthConfig(),
   //  });

//    await server.register(plugins);
    await server.register(require('@hapi/basic'));
    await server.register(require('@hapi/cookie'));
    server.auth.strategy('session', 'cookie', {
        cookie: {
            name: 'sid-example',
            password: '!wsYhFA*C2U6nz=Bu^%A@^F#SF3&kSR6',
            isSecure: false
        }
    });


    server.auth.default('session');
    await server.auth.strategy('simple','basic',{validate});
    server.route(routes);
    if (connection.isConnected) {
        console.log('DATABASE CONNECTED');
        await server.start();
        console.log(`DB connection name ${connection.name}`);
        console.log('Server ', process.env.APP_NAME, ' running on ', server.info.uri)
    }
    return server.listener;
}


