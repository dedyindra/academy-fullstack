"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _dotenv = _interopRequireDefault(require("dotenv"));

var _path = require("path");

var Config =
/*#__PURE__*/
function () {
  function Config() {
    (0, _classCallCheck2["default"])(this, Config);
    this.config = {};
  }

  (0, _createClass2["default"])(Config, [{
    key: "configure",
    value: function configure() {
      if (process.env.NODE_ENV === 'test') {
        console.log("DB TEST");

        _dotenv["default"].config({
          path: (0, _path.resolve)("test.env")
        });
      } else {
        console.log("DB run");

        _dotenv["default"].config();
      }

      if (!process.env.APP_NAME) {
        console.error('Environment file (.env) not found in folder');
        process.exit(1);
      }

      process.env.BASE_PATH = (0, _path.dirname)((0, _path.resolve)('index.js'));
    }
  }, {
    key: "get",
    value: function get(key) {
      return this.config[key] || null;
    }
  }, {
    key: "getServerConfig",
    value: function getServerConfig() {
      return {
        port: process.env.APP_PORT || this.get('APP_PORT'),
        host: process.env.APP_HOST || this.get('APP_HOST')
      };
    }
  }, {
    key: "getAuthConfig",
    value: function getAuthConfig() {
      return {
        sid: process.env.AUTH_COOKIE_SID || this.get('AUTH_COOKIE_SID'),
        label: process.env.AUTH_COOKIE_LABEL || this.get('AUTH_COOKIE_LABEL'),
        password: process.env.AUTH_COOKIE_PASSWORD || this.get('AUTH_COOKIE_PASSWORD'),
        secure: process.env.AUTH_COOKIE_SECURE === 'true' || this.get('AUTH_COOKIE_SECURE') === 'true',
        httpOnly: process.env.AUTH_COOKIE_HTTPONLY === 'true' || this.get('AUTH_COOKIE_HTTPONLY') === 'true',
        ttl: Number(process.env.AUTH_COOKIE_TTL) * 60 * 1000
      };
    }
  }]);
  return Config;
}();

var _default = Config;
exports["default"] = _default;