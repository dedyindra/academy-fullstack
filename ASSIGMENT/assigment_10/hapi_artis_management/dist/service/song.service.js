"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _typeorm = require("typeorm");

var _boom = _interopRequireDefault(require("@hapi/boom"));

var _song = _interopRequireDefault(require("../models/song.model"));

var SongService =
/*#__PURE__*/
function () {
  function SongService() {
    (0, _classCallCheck2["default"])(this, SongService);
  }

  (0, _createClass2["default"])(SongService, [{
    key: "songRepository",
    value: function songRepository() {
      return (0, _typeorm.getRepository)(_song["default"]);
    }
  }, {
    key: "createSong",
    value: function () {
      var _createSong = (0, _asyncToGenerator2["default"])(
      /*#__PURE__*/
      _regenerator["default"].mark(function _callee(song) {
        return _regenerator["default"].wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.next = 2;
                return this.songRepository().save(song);

              case 2:
                return _context.abrupt("return", _context.sent);

              case 3:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      function createSong(_x) {
        return _createSong.apply(this, arguments);
      }

      return createSong;
    }()
  }, {
    key: "findAllSong",
    value: function () {
      var _findAllSong = (0, _asyncToGenerator2["default"])(
      /*#__PURE__*/
      _regenerator["default"].mark(function _callee2() {
        return _regenerator["default"].wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _context2.next = 2;
                return this.songRepository().find();

              case 2:
                return _context2.abrupt("return", _context2.sent);

              case 3:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, this);
      }));

      function findAllSong() {
        return _findAllSong.apply(this, arguments);
      }

      return findAllSong;
    }()
  }, {
    key: "findSongById",
    value: function () {
      var _findSongById = (0, _asyncToGenerator2["default"])(
      /*#__PURE__*/
      _regenerator["default"].mark(function _callee3(id) {
        var song;
        return _regenerator["default"].wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                _context3.next = 2;
                return this.songRepository().findOne(id);

              case 2:
                song = _context3.sent;

                if (song) {
                  _context3.next = 5;
                  break;
                }

                throw _boom["default"].notFound('song not found');

              case 5:
                return _context3.abrupt("return", song);

              case 6:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3, this);
      }));

      function findSongById(_x2) {
        return _findSongById.apply(this, arguments);
      }

      return findSongById;
    }()
  }, {
    key: "updateSong",
    value: function () {
      var _updateSong = (0, _asyncToGenerator2["default"])(
      /*#__PURE__*/
      _regenerator["default"].mark(function _callee4(song) {
        var songToUpdate;
        return _regenerator["default"].wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                _context4.next = 2;
                return this.findSongById(song.id);

              case 2:
                songToUpdate = _context4.sent;
                this.songRepository().merge(songToUpdate, song);
                _context4.next = 6;
                return this.songRepository().save(songToUpdate);

              case 6:
                return _context4.abrupt("return", _context4.sent);

              case 7:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee4, this);
      }));

      function updateSong(_x3) {
        return _updateSong.apply(this, arguments);
      }

      return updateSong;
    }()
  }, {
    key: "deleteSong",
    value: function () {
      var _deleteSong = (0, _asyncToGenerator2["default"])(
      /*#__PURE__*/
      _regenerator["default"].mark(function _callee5(id) {
        var song;
        return _regenerator["default"].wrap(function _callee5$(_context5) {
          while (1) {
            switch (_context5.prev = _context5.next) {
              case 0:
                _context5.next = 2;
                return this.findSongById(id);

              case 2:
                song = _context5.sent;
                _context5.next = 5;
                return this.songRepository()["delete"](song);

              case 5:
                return _context5.abrupt("return", _context5.sent);

              case 6:
              case "end":
                return _context5.stop();
            }
          }
        }, _callee5, this);
      }));

      function deleteSong(_x4) {
        return _deleteSong.apply(this, arguments);
      }

      return deleteSong;
    }()
  }, {
    key: "findSongByArtistId",
    value: function () {
      var _findSongByArtistId = (0, _asyncToGenerator2["default"])(
      /*#__PURE__*/
      _regenerator["default"].mark(function _callee6(id) {
        var song;
        return _regenerator["default"].wrap(function _callee6$(_context6) {
          while (1) {
            switch (_context6.prev = _context6.next) {
              case 0:
                _context6.next = 2;
                return this.songRepository().find({
                  artist: (0, _typeorm.Like)("%".concat(id, "%"))
                });

              case 2:
                song = _context6.sent;

                if (!(song === undefined)) {
                  _context6.next = 5;
                  break;
                }

                return _context6.abrupt("return", {
                  message: "data tidak ada",
                  status: 404
                });

              case 5:
                return _context6.abrupt("return", song);

              case 6:
              case "end":
                return _context6.stop();
            }
          }
        }, _callee6, this);
      }));

      function findSongByArtistId(_x5) {
        return _findSongByArtistId.apply(this, arguments);
      }

      return findSongByArtistId;
    }()
  }]);
  return SongService;
}();

var _default = SongService;
exports["default"] = _default;