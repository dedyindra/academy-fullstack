"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _typeorm = require("typeorm");

var _boom = _interopRequireDefault(require("@hapi/boom"));

var _artist = _interopRequireDefault(require("../models/artist.model"));

var ArtistService =
/*#__PURE__*/
function () {
  function ArtistService() {
    (0, _classCallCheck2["default"])(this, ArtistService);
  }

  (0, _createClass2["default"])(ArtistService, [{
    key: "artistRepository",
    value: function artistRepository() {
      return (0, _typeorm.getRepository)(_artist["default"]);
    }
  }, {
    key: "createArtist",
    value: function () {
      var _createArtist = (0, _asyncToGenerator2["default"])(
      /*#__PURE__*/
      _regenerator["default"].mark(function _callee(artist) {
        return _regenerator["default"].wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.next = 2;
                return this.artistRepository().save(artist);

              case 2:
                return _context.abrupt("return", _context.sent);

              case 3:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      function createArtist(_x) {
        return _createArtist.apply(this, arguments);
      }

      return createArtist;
    }()
  }, {
    key: "findAllArtist",
    value: function () {
      var _findAllArtist = (0, _asyncToGenerator2["default"])(
      /*#__PURE__*/
      _regenerator["default"].mark(function _callee2() {
        return _regenerator["default"].wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _context2.next = 2;
                return this.artistRepository().find();

              case 2:
                return _context2.abrupt("return", _context2.sent);

              case 3:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, this);
      }));

      function findAllArtist() {
        return _findAllArtist.apply(this, arguments);
      }

      return findAllArtist;
    }()
  }, {
    key: "findArtistById",
    value: function () {
      var _findArtistById = (0, _asyncToGenerator2["default"])(
      /*#__PURE__*/
      _regenerator["default"].mark(function _callee3(id) {
        var artist;
        return _regenerator["default"].wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                _context3.next = 2;
                return this.artistRepository().findOne(id);

              case 2:
                artist = _context3.sent;

                if (artist) {
                  _context3.next = 5;
                  break;
                }

                throw _boom["default"].notFound('artist not found');

              case 5:
                return _context3.abrupt("return", artist);

              case 6:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3, this);
      }));

      function findArtistById(_x2) {
        return _findArtistById.apply(this, arguments);
      }

      return findArtistById;
    }()
  }, {
    key: "updateArtist",
    value: function () {
      var _updateArtist = (0, _asyncToGenerator2["default"])(
      /*#__PURE__*/
      _regenerator["default"].mark(function _callee4(artist) {
        var artistToUpdate;
        return _regenerator["default"].wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                _context4.next = 2;
                return this.findArtistById(artist.id);

              case 2:
                artistToUpdate = _context4.sent;
                this.artistRepository().merge(artistToUpdate, artist);
                _context4.next = 6;
                return this.artistRepository().save(artistToUpdate);

              case 6:
                return _context4.abrupt("return", _context4.sent);

              case 7:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee4, this);
      }));

      function updateArtist(_x3) {
        return _updateArtist.apply(this, arguments);
      }

      return updateArtist;
    }()
  }, {
    key: "deleteArtist",
    value: function () {
      var _deleteArtist = (0, _asyncToGenerator2["default"])(
      /*#__PURE__*/
      _regenerator["default"].mark(function _callee5(id) {
        var artist;
        return _regenerator["default"].wrap(function _callee5$(_context5) {
          while (1) {
            switch (_context5.prev = _context5.next) {
              case 0:
                _context5.next = 2;
                return this.findArtistById(id);

              case 2:
                artist = _context5.sent;
                _context5.next = 5;
                return this.artistRepository()["delete"](artist);

              case 5:
                return _context5.abrupt("return", _context5.sent);

              case 6:
              case "end":
                return _context5.stop();
            }
          }
        }, _callee5, this);
      }));

      function deleteArtist(_x4) {
        return _deleteArtist.apply(this, arguments);
      }

      return deleteArtist;
    }()
  }, {
    key: "findArtistByGenreId",
    value: function () {
      var _findArtistByGenreId = (0, _asyncToGenerator2["default"])(
      /*#__PURE__*/
      _regenerator["default"].mark(function _callee6(id) {
        var artist;
        return _regenerator["default"].wrap(function _callee6$(_context6) {
          while (1) {
            switch (_context6.prev = _context6.next) {
              case 0:
                _context6.next = 2;
                return this.artistRepository().find({
                  genre: (0, _typeorm.Like)("%".concat(id, "%"))
                });

              case 2:
                artist = _context6.sent;

                if (!(artist === undefined)) {
                  _context6.next = 5;
                  break;
                }

                return _context6.abrupt("return", {
                  message: "data tidak ada",
                  status: 404
                });

              case 5:
                return _context6.abrupt("return", artist);

              case 6:
              case "end":
                return _context6.stop();
            }
          }
        }, _callee6, this);
      }));

      function findArtistByGenreId(_x5) {
        return _findArtistByGenreId.apply(this, arguments);
      }

      return findArtistByGenreId;
    }()
  }]);
  return ArtistService;
}();

var _default = ArtistService;
exports["default"] = _default;