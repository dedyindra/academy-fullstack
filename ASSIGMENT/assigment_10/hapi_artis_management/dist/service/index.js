"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "ArtistService", {
  enumerable: true,
  get: function get() {
    return _artist["default"];
  }
});
Object.defineProperty(exports, "GenreService", {
  enumerable: true,
  get: function get() {
    return _genre["default"];
  }
});
Object.defineProperty(exports, "SongService", {
  enumerable: true,
  get: function get() {
    return _song["default"];
  }
});

var _artist = _interopRequireDefault(require("./artist.service"));

var _genre = _interopRequireDefault(require("./genre.service"));

var _song = _interopRequireDefault(require("./song.service"));