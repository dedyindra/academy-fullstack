"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _typeorm = require("typeorm");

var _login2 = _interopRequireDefault(require("../models/login.model"));

var _bcryptjs = _interopRequireDefault(require("bcryptjs"));

var _boom = _interopRequireDefault(require("@hapi/boom"));

var LoginService =
/*#__PURE__*/
function () {
  function LoginService() {
    (0, _classCallCheck2["default"])(this, LoginService);
  }

  (0, _createClass2["default"])(LoginService, [{
    key: "loginRepository",
    value: function loginRepository() {
      return (0, _typeorm.getRepository)(_login2["default"]);
    }
  }, {
    key: "beforeCreate",
    value: function () {
      var _beforeCreate = (0, _asyncToGenerator2["default"])(
      /*#__PURE__*/
      _regenerator["default"].mark(function _callee(password) {
        var salt;
        return _regenerator["default"].wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                salt = _bcryptjs["default"].genSaltSync();
                return _context.abrupt("return", _bcryptjs["default"].hashSync(password, salt));

              case 2:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }));

      function beforeCreate(_x) {
        return _beforeCreate.apply(this, arguments);
      }

      return beforeCreate;
    }()
  }, {
    key: "validate",
    value: function () {
      var _validate = (0, _asyncToGenerator2["default"])(
      /*#__PURE__*/
      _regenerator["default"].mark(function _callee2(request, username, password) {
        var user, isValid, credentials;
        return _regenerator["default"].wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _context2.next = 2;
                return new LoginService().findByUsername(username);

              case 2:
                user = _context2.sent;

                if (user) {
                  _context2.next = 5;
                  break;
                }

                return _context2.abrupt("return", {
                  credentials: null,
                  isValid: false
                });

              case 5:
                _context2.next = 7;
                return new LoginService().validPassword(password, user.password);

              case 7:
                isValid = _context2.sent;
                credentials = {
                  id: user.id,
                  username: user.username
                };
                return _context2.abrupt("return", {
                  isValid: isValid,
                  credentials: credentials
                });

              case 10:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2);
      }));

      function validate(_x2, _x3, _x4) {
        return _validate.apply(this, arguments);
      }

      return validate;
    }()
  }, {
    key: "validPassword",
    value: function () {
      var _validPassword = (0, _asyncToGenerator2["default"])(
      /*#__PURE__*/
      _regenerator["default"].mark(function _callee3(password, checkPassword) {
        return _regenerator["default"].wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                return _context3.abrupt("return", _bcryptjs["default"].compareSync(password, checkPassword));

              case 1:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3);
      }));

      function validPassword(_x5, _x6) {
        return _validPassword.apply(this, arguments);
      }

      return validPassword;
    }()
  }, {
    key: "validateCookie",
    value: function () {
      var _validateCookie = (0, _asyncToGenerator2["default"])(
      /*#__PURE__*/
      _regenerator["default"].mark(function _callee4(request, session) {
        var user;
        return _regenerator["default"].wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                _context4.next = 2;
                return this.loginRepository().findOne(session.id);

              case 2:
                user = _context4.sent;

                if (user) {
                  _context4.next = 5;
                  break;
                }

                return _context4.abrupt("return", {
                  valid: false
                });

              case 5:
                return _context4.abrupt("return", {
                  valid: true,
                  credentials: user
                });

              case 6:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee4, this);
      }));

      function validateCookie(_x7, _x8) {
        return _validateCookie.apply(this, arguments);
      }

      return validateCookie;
    }()
  }, {
    key: "updatePassword",
    value: function () {
      var _updatePassword = (0, _asyncToGenerator2["default"])(
      /*#__PURE__*/
      _regenerator["default"].mark(function _callee5(data) {
        var id, datanya, password;
        return _regenerator["default"].wrap(function _callee5$(_context5) {
          while (1) {
            switch (_context5.prev = _context5.next) {
              case 0:
                id = "".concat(data.id);
                datanya = this.loginRepository().findOne(id);

                if (!datanya) {
                  _context5.next = 8;
                  break;
                }

                password = data.password;
                _context5.next = 6;
                return this.beforeCreate(password);

              case 6:
                data.password = _context5.sent;
                return _context5.abrupt("return", this.loginRepository().save(data));

              case 8:
              case "end":
                return _context5.stop();
            }
          }
        }, _callee5, this);
      }));

      function updatePassword(_x9) {
        return _updatePassword.apply(this, arguments);
      }

      return updatePassword;
    }()
  }, {
    key: "createPassword",
    value: function () {
      var _createPassword = (0, _asyncToGenerator2["default"])(
      /*#__PURE__*/
      _regenerator["default"].mark(function _callee6(data) {
        var password;
        return _regenerator["default"].wrap(function _callee6$(_context6) {
          while (1) {
            switch (_context6.prev = _context6.next) {
              case 0:
                password = data.password;
                _context6.next = 3;
                return this.beforeCreate(password);

              case 3:
                data.password = _context6.sent;
                return _context6.abrupt("return", this.loginRepository().save(data));

              case 5:
              case "end":
                return _context6.stop();
            }
          }
        }, _callee6, this);
      }));

      function createPassword(_x10) {
        return _createPassword.apply(this, arguments);
      }

      return createPassword;
    }()
  }, {
    key: "findByUsername",
    value: function () {
      var _findByUsername = (0, _asyncToGenerator2["default"])(
      /*#__PURE__*/
      _regenerator["default"].mark(function _callee7(username) {
        return _regenerator["default"].wrap(function _callee7$(_context7) {
          while (1) {
            switch (_context7.prev = _context7.next) {
              case 0:
                _context7.next = 2;
                return this.loginRepository().findOne({
                  username: username
                });

              case 2:
                return _context7.abrupt("return", _context7.sent);

              case 3:
              case "end":
                return _context7.stop();
            }
          }
        }, _callee7, this);
      }));

      function findByUsername(_x11) {
        return _findByUsername.apply(this, arguments);
      }

      return findByUsername;
    }()
  }, {
    key: "login",
    value: function () {
      var _login = (0, _asyncToGenerator2["default"])(
      /*#__PURE__*/
      _regenerator["default"].mark(function _callee8(data) {
        var username, password, user;
        return _regenerator["default"].wrap(function _callee8$(_context8) {
          while (1) {
            switch (_context8.prev = _context8.next) {
              case 0:
                username = data.username, password = data.password;
                _context8.next = 3;
                return this.findByUsername(username);

              case 3:
                user = _context8.sent;
                _context8.t0 = user;

                if (!_context8.t0) {
                  _context8.next = 9;
                  break;
                }

                _context8.next = 8;
                return this.validPassword(password, user.password);

              case 8:
                _context8.t0 = _context8.sent;

              case 9:
                if (!_context8.t0) {
                  _context8.next = 13;
                  break;
                }

                return _context8.abrupt("return", user);

              case 13:
                throw _boom["default"].unauthorized('Invalid Username or Password');

              case 14:
              case "end":
                return _context8.stop();
            }
          }
        }, _callee8, this);
      }));

      function login(_x12) {
        return _login.apply(this, arguments);
      }

      return login;
    }()
  }]);
  return LoginService;
}();

exports["default"] = LoginService;