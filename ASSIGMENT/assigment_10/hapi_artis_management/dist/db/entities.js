"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _artist = _interopRequireDefault(require("../schema/artist.schema"));

var _genre = _interopRequireDefault(require("../schema/genre.schema"));

var _song = _interopRequireDefault(require("../schema/song.schema"));

var _login = _interopRequireDefault(require("../schema/login.schema"));

var _default = {
  ArtistSchema: _artist["default"],
  GenreSchema: _genre["default"],
  SongSchema: _song["default"],
  LoginSchema: _login["default"]
};
exports["default"] = _default;