"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _service = require("../service");

var _boom = _interopRequireDefault(require("@hapi/boom"));

var _joi = _interopRequireDefault(require("@hapi/joi"));

var songService = new _service.SongService();
var SongRoute = [{
  method: 'POST',
  path: '/song',
  config: {
    handler: function () {
      var _handler = (0, _asyncToGenerator2["default"])(
      /*#__PURE__*/
      _regenerator["default"].mark(function _callee(req, h) {
        var song;
        return _regenerator["default"].wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                song = req.payload;
                _context.next = 3;
                return songService.createSong(song);

              case 3:
                song = _context.sent;

                if (song) {
                  _context.next = 8;
                  break;
                }

                throw _boom["default"].notFound('song not found');

              case 8:
                return _context.abrupt("return", song);

              case 9:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }));

      function handler(_x, _x2) {
        return _handler.apply(this, arguments);
      }

      return handler;
    }()
  }
}, {
  method: 'GET',
  path: '/songs',
  config: {
    // auth:'simple',
    handler: function () {
      var _handler2 = (0, _asyncToGenerator2["default"])(
      /*#__PURE__*/
      _regenerator["default"].mark(function _callee2(req, h) {
        var song;
        return _regenerator["default"].wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _context2.next = 2;
                return songService.findAllSong();

              case 2:
                song = _context2.sent;

                if (song) {
                  _context2.next = 7;
                  break;
                }

                throw _boom["default"].notFound('song not found');

              case 7:
                return _context2.abrupt("return", song);

              case 8:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2);
      }));

      function handler(_x3, _x4) {
        return _handler2.apply(this, arguments);
      }

      return handler;
    }()
  }
}, {
  method: 'GET',
  path: '/song/{id}',
  config: {
    // auth:'simple',
    handler: function () {
      var _handler3 = (0, _asyncToGenerator2["default"])(
      /*#__PURE__*/
      _regenerator["default"].mark(function _callee3(req, h) {
        var params, song;
        return _regenerator["default"].wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                params = req.params;
                _context3.next = 3;
                return songService.findSongByArtistId(params.id);

              case 3:
                song = _context3.sent;

                if (song) {
                  _context3.next = 8;
                  break;
                }

                throw _boom["default"].notFound('artist not found');

              case 8:
                return _context3.abrupt("return", song);

              case 9:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3);
      }));

      function handler(_x5, _x6) {
        return _handler3.apply(this, arguments);
      }

      return handler;
    }()
  }
}];
var _default = SongRoute;
exports["default"] = _default;