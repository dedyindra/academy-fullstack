"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _service = require("../service");

var _boom = _interopRequireDefault(require("@hapi/boom"));

var _joi = _interopRequireDefault(require("@hapi/joi"));

var _fileSystem = _interopRequireDefault(require("file-system"));

var artistService = new _service.ArtistService();
var ArtistRouter = [{
  method: 'POST',
  path: '/artist',
  handler: function () {
    var _handler = (0, _asyncToGenerator2["default"])(
    /*#__PURE__*/
    _regenerator["default"].mark(function _callee(request, h) {
      var data, artist, namaFile, path, file;
      return _regenerator["default"].wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              data = request.payload;
              artist = JSON.parse(data.artist);
              _context.next = 4;
              return artistService.createArtist(artist);

            case 4:
              artist = _context.sent;

              if (data.file) {
                namaFile = artist.id;
                path = "E:\\xampp\\htdocs\\img\\" + namaFile + ".png";
                file = _fileSystem["default"].createWriteStream(path);
                file.on('error', function (err) {
                  return console.error(err);
                });
                data.file.pipe(file);
                data.file.on('end', function (err) {
                  var ret = {
                    filename: data.file.hapi.filename,
                    headers: data.file.hapi.headers
                  };
                  return JSON.stringify(ret);
                });
              }

              if (artist) {
                _context.next = 10;
                break;
              }

              throw _boom["default"].notFound('song not found');

            case 10:
              return _context.abrupt("return", artist);

            case 11:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    }));

    function handler(_x, _x2) {
      return _handler.apply(this, arguments);
    }

    return handler;
  }(),
  options: {
    payload: {
      output: 'stream',
      parse: true,
      allow: 'multipart/form-data'
    }
  }
}, {
  method: 'GET',
  path: '/artists',
  config: {
    // auth:'simple',
    handler: function () {
      var _handler2 = (0, _asyncToGenerator2["default"])(
      /*#__PURE__*/
      _regenerator["default"].mark(function _callee2(req, h) {
        var artist;
        return _regenerator["default"].wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _context2.next = 2;
                return artistService.findAllArtist();

              case 2:
                artist = _context2.sent;

                if (artist) {
                  _context2.next = 7;
                  break;
                }

                throw _boom["default"].notFound('artist not found');

              case 7:
                return _context2.abrupt("return", artist);

              case 8:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2);
      }));

      function handler(_x3, _x4) {
        return _handler2.apply(this, arguments);
      }

      return handler;
    }()
  }
}, {
  method: 'GET',
  path: '/artist/{id}',
  config: {
    // auth:'simple',
    handler: function () {
      var _handler3 = (0, _asyncToGenerator2["default"])(
      /*#__PURE__*/
      _regenerator["default"].mark(function _callee3(req, h) {
        var params, artist;
        return _regenerator["default"].wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                params = req.params;
                _context3.next = 3;
                return artistService.findArtistByGenreId(params.id);

              case 3:
                artist = _context3.sent;

                if (artist) {
                  _context3.next = 8;
                  break;
                }

                throw _boom["default"].notFound('artist not found');

              case 8:
                return _context3.abrupt("return", artist);

              case 9:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3);
      }));

      function handler(_x5, _x6) {
        return _handler3.apply(this, arguments);
      }

      return handler;
    }()
  }
}];
var _default = ArtistRouter;
exports["default"] = _default;