"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _artist = _interopRequireDefault(require("./artist.route"));

var _genre = _interopRequireDefault(require("./genre.route"));

var _song = _interopRequireDefault(require("./song.route"));

var _login = _interopRequireDefault(require("./login.route"));

var _default = [].concat(_artist["default"], _genre["default"], _song["default"], _login["default"]);

exports["default"] = _default;