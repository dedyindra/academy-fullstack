"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _service = require("../service");

var _boom = _interopRequireDefault(require("@hapi/boom"));

var _joi = _interopRequireDefault(require("@hapi/joi"));

var _fileSystem = _interopRequireDefault(require("file-system"));

var genreService = new _service.GenreService();
var GenreRouter = [{
  method: 'POST',
  path: '/genre',
  handler: function () {
    var _handler = (0, _asyncToGenerator2["default"])(
    /*#__PURE__*/
    _regenerator["default"].mark(function _callee(request, h) {
      var data, genre, namaFile, path, file;
      return _regenerator["default"].wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              data = request.payload;
              genre = JSON.parse(data.genre);
              _context.next = 4;
              return genreService.createGenre(genre);

            case 4:
              genre = _context.sent;

              //console.log(data.genre.id);
              //  console.log(data);
              if (data.file) {
                // const name = data.file.hapi.filename;
                namaFile = genre.id; //     const path = __dirname + "/" + namaFile +".png";

                path = "E:\\xampp\\htdocs\\img\\" + namaFile + ".png";
                file = _fileSystem["default"].createWriteStream(path);
                file.on('error', function (err) {
                  return console.error(err);
                });
                data.file.pipe(file);
                data.file.on('end', function (err) {
                  var ret = {
                    filename: data.file.hapi.filename,
                    headers: data.file.hapi.headers
                  };
                  return JSON.stringify(ret);
                });
              }

              if (genre) {
                _context.next = 10;
                break;
              }

              throw _boom["default"].notFound('song not found');

            case 10:
              return _context.abrupt("return", genre);

            case 11:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    }));

    function handler(_x, _x2) {
      return _handler.apply(this, arguments);
    }

    return handler;
  }(),
  options: {
    payload: {
      output: 'stream',
      parse: true,
      allow: 'multipart/form-data'
    }
  }
}, {
  method: 'GET',
  path: '/genres',
  config: {
    // auth:'simple',
    handler: function () {
      var _handler2 = (0, _asyncToGenerator2["default"])(
      /*#__PURE__*/
      _regenerator["default"].mark(function _callee2(req, h) {
        var genre;
        return _regenerator["default"].wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _context2.next = 2;
                return genreService.findAllGenre();

              case 2:
                genre = _context2.sent;

                if (genre) {
                  _context2.next = 7;
                  break;
                }

                throw _boom["default"].notFound('customer not found');

              case 7:
                return _context2.abrupt("return", genre);

              case 8:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2);
      }));

      function handler(_x3, _x4) {
        return _handler2.apply(this, arguments);
      }

      return handler;
    }()
  }
}];
var _default = GenreRouter;
exports["default"] = _default;