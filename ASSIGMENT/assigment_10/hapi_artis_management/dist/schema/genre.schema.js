"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _typeorm = require("typeorm");

var _genre = _interopRequireDefault(require("../models/genre.model"));

var GenreSchema = new _typeorm.EntitySchema({
  name: 'Genre',
  target: _genre["default"],
  tableName: 'genre',
  columns: {
    id: {
      primary: true,
      type: "uuid",
      generated: "uuid",
      nullable: false
    },
    genre: {
      type: "varchar",
      nullable: false
    }
  },
  relations: {
    artist: {
      target: "ArtistModel",
      type: "one-to-many",
      inverseSide: "genre",
      joinColumn: true,
      cascade: true,
      eager: false
    }
  }
});
var _default = GenreSchema;
exports["default"] = _default;