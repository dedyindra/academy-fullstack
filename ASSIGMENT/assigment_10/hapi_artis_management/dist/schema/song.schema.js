"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _typeorm = require("typeorm");

var _song = _interopRequireDefault(require("../models/song.model"));

var SongSchema = new _typeorm.EntitySchema({
  name: 'Song',
  target: _song["default"],
  tableName: 'song',
  columns: {
    id: {
      primary: true,
      type: "uuid",
      generated: "uuid",
      nullable: false
    },
    nameLagu: {
      type: "varchar",
      nullable: false
    },
    duration: {
      type: "varchar",
      nullable: false
    }
  },
  relations: {
    artist: {
      target: "ArtistModel",
      type: "many-to-one",
      joinColumn: true,
      eager: true,
      nullable: false
    }
  }
});
var _default = SongSchema;
exports["default"] = _default;