"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _typeorm = require("typeorm");

var _artist = _interopRequireDefault(require("../models/artist.model"));

var ArtistSchema = new _typeorm.EntitySchema({
  name: 'Artist',
  target: _artist["default"],
  tableName: 'artist',
  columns: {
    id: {
      primary: true,
      type: "uuid",
      generated: "uuid",
      nullable: false
    },
    name: {
      type: "varchar",
      nullable: false
    },
    bornPlace: {
      type: "varchar",
      nullable: false
    },
    debut: {
      type: "datetime",
      nullable: false
    }
  },
  relations: {
    genre: {
      target: "GenreModel",
      type: "many-to-one",
      joinColumn: true,
      eager: true,
      nullable: false
    },
    song: {
      target: "SongModel",
      type: "one-to-many",
      inverseSide: "artist",
      joinColumn: true,
      cascade: true,
      eager: false
    }
  }
});
var _default = ArtistSchema;
exports["default"] = _default;