"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _supertest = _interopRequireDefault(require("supertest"));

var _genre = _interopRequireDefault(require("../../service/genre.service"));

var _index = _interopRequireDefault(require("../../index"));

var genreService = new _genre["default"]();
var dataGenre = {
  genre: "POP"
};
var server;
describe('genre route', function () {
  beforeAll(
  /*#__PURE__*/
  (0, _asyncToGenerator2["default"])(
  /*#__PURE__*/
  _regenerator["default"].mark(function _callee() {
    return _regenerator["default"].wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.next = 2;
            return (0, _index["default"])();

          case 2:
            server = _context.sent;
            _context.next = 5;
            return genreService.genreRepository().clear();

          case 5:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  })));
  beforeEach(
  /*#__PURE__*/
  (0, _asyncToGenerator2["default"])(
  /*#__PURE__*/
  _regenerator["default"].mark(function _callee2() {
    return _regenerator["default"].wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            _context2.next = 2;
            return genreService.genreRepository().clear();

          case 2:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2);
  })));
  it('post should create a genre',
  /*#__PURE__*/
  (0, _asyncToGenerator2["default"])(
  /*#__PURE__*/
  _regenerator["default"].mark(function _callee3() {
    var response;
    return _regenerator["default"].wrap(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            _context3.next = 2;
            return (0, _supertest["default"])(server).post('/genre').set('Accept', 'application/x-www-form-urlencoded').send(dataGenre);

          case 2:
            response = _context3.sent;
            expect(response.statusCode).toEqual(200);
            expect(response.payload).toHaveProperty('id');
            expect(response.payload.genre).toEqual('POP');

          case 6:
          case "end":
            return _context3.stop();
        }
      }
    }, _callee3);
  })));
  afterEach(
  /*#__PURE__*/
  (0, _asyncToGenerator2["default"])(
  /*#__PURE__*/
  _regenerator["default"].mark(function _callee4() {
    return _regenerator["default"].wrap(function _callee4$(_context4) {
      while (1) {
        switch (_context4.prev = _context4.next) {
          case 0:
            if (server) {
              server.close();
            }

          case 1:
          case "end":
            return _context4.stop();
        }
      }
    }, _callee4);
  })));
});