"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _song = _interopRequireDefault(require("../../service/song.service"));

var _artist = _interopRequireDefault(require("../../service/artist.service"));

var _genre = _interopRequireDefault(require("../../service/genre.service"));

var _index = _interopRequireDefault(require("../../index"));

var songService = new _song["default"]();
var genreService = new _genre["default"]();
var artistService = new _artist["default"]();
var dataGenre = {
  genre: "POP"
};
var dataSongs = [{
  nameLagu: "rindu kamu",
  duration: "05:00"
}, {
  nameLagu: "kenangan terindah",
  duration: "05:00"
}];
var server;
describe('create song', function () {
  beforeEach(
  /*#__PURE__*/
  (0, _asyncToGenerator2["default"])(
  /*#__PURE__*/
  _regenerator["default"].mark(function _callee() {
    return _regenerator["default"].wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.next = 2;
            return (0, _index["default"])();

          case 2:
            server = _context.sent;
            _context.next = 5;
            return songService.songRepository().clear();

          case 5:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  })));
  it('should post data song',
  /*#__PURE__*/
  (0, _asyncToGenerator2["default"])(
  /*#__PURE__*/
  _regenerator["default"].mark(function _callee2() {
    var genre, dataArtist, artist, dataSong, song, result;
    return _regenerator["default"].wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            _context2.next = 2;
            return genreService.genreRepository().save(dataGenre);

          case 2:
            genre = _context2.sent;
            dataArtist = {
              bornPlace: "jak",
              debut: "2019-12-03 00:00:00",
              name: "st 13",
              genre: genre.id
            };
            _context2.next = 6;
            return artistService.artistRepository().save(dataArtist);

          case 6:
            artist = _context2.sent;
            dataSong = {
              nameLagu: "rindu kamu",
              duration: "05:00",
              artist: artist.id
            };
            _context2.next = 10;
            return songService.createSong(dataSong);

          case 10:
            song = _context2.sent;
            _context2.next = 13;
            return songService.songRepository().findOne(song.id);

          case 13:
            result = _context2.sent;
            expect(result.nameLagu).toEqual(song.nameLagu);

          case 15:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2);
  })));
  afterEach(
  /*#__PURE__*/
  (0, _asyncToGenerator2["default"])(
  /*#__PURE__*/
  _regenerator["default"].mark(function _callee3() {
    return _regenerator["default"].wrap(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            if (server) {
              server.close();
            }

          case 1:
          case "end":
            return _context3.stop();
        }
      }
    }, _callee3);
  })));
});