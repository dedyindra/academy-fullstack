"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _artist = _interopRequireDefault(require("../../service/artist.service"));

var _genre = _interopRequireDefault(require("../../service/genre.service"));

var _index = _interopRequireDefault(require("../../index"));

var artistService = new _artist["default"]();
var genreService = new _genre["default"]();
var dataGenre = {
  genre: "POP"
};
var server;
describe('create artist', function () {
  beforeEach(
  /*#__PURE__*/
  (0, _asyncToGenerator2["default"])(
  /*#__PURE__*/
  _regenerator["default"].mark(function _callee() {
    return _regenerator["default"].wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.next = 2;
            return (0, _index["default"])();

          case 2:
            server = _context.sent;
            _context.next = 5;
            return artistService.artistRepository().clear();

          case 5:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  })));
  it('should post data artist',
  /*#__PURE__*/
  (0, _asyncToGenerator2["default"])(
  /*#__PURE__*/
  _regenerator["default"].mark(function _callee2() {
    var genre, dataArtist, artist, result;
    return _regenerator["default"].wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            _context2.next = 2;
            return genreService.genreRepository().save(dataGenre);

          case 2:
            genre = _context2.sent;
            dataArtist = {
              bornPlace: "jakarta",
              debut: "2019-12-03 00:00:00",
              name: "st 13",
              genre: genre.id
            };
            _context2.next = 6;
            return artistService.createArtist(dataArtist);

          case 6:
            artist = _context2.sent;
            _context2.next = 9;
            return artistService.artistRepository().findOne(artist.id);

          case 9:
            result = _context2.sent;
            expect(result.name).toEqual(artist.name);

          case 11:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2);
  })));
  afterEach(
  /*#__PURE__*/
  (0, _asyncToGenerator2["default"])(
  /*#__PURE__*/
  _regenerator["default"].mark(function _callee3() {
    return _regenerator["default"].wrap(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            if (server) {
              server.close();
            }

          case 1:
          case "end":
            return _context3.stop();
        }
      }
    }, _callee3);
  })));
});
describe('find artist', function () {
  beforeEach(
  /*#__PURE__*/
  (0, _asyncToGenerator2["default"])(
  /*#__PURE__*/
  _regenerator["default"].mark(function _callee4() {
    return _regenerator["default"].wrap(function _callee4$(_context4) {
      while (1) {
        switch (_context4.prev = _context4.next) {
          case 0:
            _context4.next = 2;
            return artistService.artistRepository().clear();

          case 2:
          case "end":
            return _context4.stop();
        }
      }
    }, _callee4);
  })));
  it('should get data artist by id',
  /*#__PURE__*/
  (0, _asyncToGenerator2["default"])(
  /*#__PURE__*/
  _regenerator["default"].mark(function _callee5() {
    var genres, dataArtist, _ref6, id, name, artist;

    return _regenerator["default"].wrap(function _callee5$(_context5) {
      while (1) {
        switch (_context5.prev = _context5.next) {
          case 0:
            _context5.next = 2;
            return genreService.genreRepository().save(dataGenre);

          case 2:
            genres = _context5.sent;
            dataArtist = {
              bornPlace: "jakarta",
              debut: "2019-12-03 00:00:00",
              name: "st 13",
              genre: genres.id
            };
            _context5.next = 6;
            return artistService.artistRepository().save(dataArtist);

          case 6:
            _ref6 = _context5.sent;
            id = _ref6.id;
            name = _ref6.name;
            _context5.next = 11;
            return artistService.findArtistById(id);

          case 11:
            artist = _context5.sent;
            expect(artist).toMatchObject({
              id: id,
              name: name
            });

          case 13:
          case "end":
            return _context5.stop();
        }
      }
    }, _callee5);
  })));
  it('should get all data artist',
  /*#__PURE__*/
  (0, _asyncToGenerator2["default"])(
  /*#__PURE__*/
  _regenerator["default"].mark(function _callee6() {
    var saveArtist, genres, dataArtist, i, artist;
    return _regenerator["default"].wrap(function _callee6$(_context6) {
      while (1) {
        switch (_context6.prev = _context6.next) {
          case 0:
            saveArtist = [];
            _context6.next = 3;
            return genreService.genreRepository().save(dataGenre);

          case 3:
            genres = _context6.sent;
            dataArtist = [{
              bornPlace: "jakarta",
              debut: "2019-12-03 00:00:00",
              name: "st 13",
              genre: genres.id
            }, {
              bornPlace: "jakarta",
              debut: "2019-12-03 00:00:00",
              name: "st 13",
              genre: genres.id
            }];
            i = 1;

          case 6:
            if (!(i < dataArtist.length)) {
              _context6.next = 15;
              break;
            }

            _context6.t0 = saveArtist;
            _context6.next = 10;
            return artistService.artistRepository().save(dataArtist[i]);

          case 10:
            _context6.t1 = _context6.sent;

            _context6.t0.push.call(_context6.t0, _context6.t1);

          case 12:
            i++;
            _context6.next = 6;
            break;

          case 15:
            _context6.next = 17;
            return artistService.findAllArtist();

          case 17:
            artist = _context6.sent;
            expect(artist).toHaveLength(saveArtist.length);

          case 19:
          case "end":
            return _context6.stop();
        }
      }
    }, _callee6);
  })));
  afterEach(
  /*#__PURE__*/
  (0, _asyncToGenerator2["default"])(
  /*#__PURE__*/
  _regenerator["default"].mark(function _callee7() {
    return _regenerator["default"].wrap(function _callee7$(_context7) {
      while (1) {
        switch (_context7.prev = _context7.next) {
          case 0:
            if (server) {
              server.close();
            }

          case 1:
          case "end":
            return _context7.stop();
        }
      }
    }, _callee7);
  })));
});