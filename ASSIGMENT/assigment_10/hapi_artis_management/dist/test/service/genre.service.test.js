"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _genre = _interopRequireDefault(require("../../service/genre.service"));

var _index = _interopRequireDefault(require("../../index"));

var genreService = new _genre["default"]();
var dataGenre = {
  genre: "POP"
};
var dataGenres = [{
  genre: "POP"
}, {
  genre: "ROCK"
}];
var server;
describe('create genre', function () {
  beforeEach(
  /*#__PURE__*/
  (0, _asyncToGenerator2["default"])(
  /*#__PURE__*/
  _regenerator["default"].mark(function _callee() {
    return _regenerator["default"].wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.next = 2;
            return (0, _index["default"])();

          case 2:
            server = _context.sent;
            _context.next = 5;
            return genreService.genreRepository().clear();

          case 5:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  })));
  it('should post data genre',
  /*#__PURE__*/
  (0, _asyncToGenerator2["default"])(
  /*#__PURE__*/
  _regenerator["default"].mark(function _callee2() {
    var genre, result;
    return _regenerator["default"].wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            _context2.next = 2;
            return genreService.createGenre(dataGenre);

          case 2:
            genre = _context2.sent;
            _context2.next = 5;
            return genreService.genreRepository().findOne(genre.id);

          case 5:
            result = _context2.sent;
            expect(result).toMatchObject(genre);

          case 7:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2);
  })));
  afterEach(
  /*#__PURE__*/
  (0, _asyncToGenerator2["default"])(
  /*#__PURE__*/
  _regenerator["default"].mark(function _callee3() {
    return _regenerator["default"].wrap(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            if (server) {
              server.close();
            }

          case 1:
          case "end":
            return _context3.stop();
        }
      }
    }, _callee3);
  })));
});
describe('find genre', function () {
  beforeEach(
  /*#__PURE__*/
  (0, _asyncToGenerator2["default"])(
  /*#__PURE__*/
  _regenerator["default"].mark(function _callee4() {
    return _regenerator["default"].wrap(function _callee4$(_context4) {
      while (1) {
        switch (_context4.prev = _context4.next) {
          case 0:
            _context4.next = 2;
            return genreService.genreRepository().clear();

          case 2:
          case "end":
            return _context4.stop();
        }
      }
    }, _callee4);
  })));
  it('should get data genre by id',
  /*#__PURE__*/
  (0, _asyncToGenerator2["default"])(
  /*#__PURE__*/
  _regenerator["default"].mark(function _callee5() {
    var _ref6, id, genre, genres;

    return _regenerator["default"].wrap(function _callee5$(_context5) {
      while (1) {
        switch (_context5.prev = _context5.next) {
          case 0:
            _context5.next = 2;
            return genreService.genreRepository().save(dataGenre);

          case 2:
            _ref6 = _context5.sent;
            id = _ref6.id;
            genre = _ref6.genre;
            _context5.next = 7;
            return genreService.findGenreById(id);

          case 7:
            genres = _context5.sent;
            expect(genres).toMatchObject({
              id: id,
              genre: genre
            });

          case 9:
          case "end":
            return _context5.stop();
        }
      }
    }, _callee5);
  })));
  it('should get all data genre',
  /*#__PURE__*/
  (0, _asyncToGenerator2["default"])(
  /*#__PURE__*/
  _regenerator["default"].mark(function _callee6() {
    var saveGenre, i, genre;
    return _regenerator["default"].wrap(function _callee6$(_context6) {
      while (1) {
        switch (_context6.prev = _context6.next) {
          case 0:
            saveGenre = [];
            i = 1;

          case 2:
            if (!(i < dataGenres.length)) {
              _context6.next = 11;
              break;
            }

            _context6.t0 = saveGenre;
            _context6.next = 6;
            return genreService.genreRepository().save(dataGenres[i]);

          case 6:
            _context6.t1 = _context6.sent;

            _context6.t0.push.call(_context6.t0, _context6.t1);

          case 8:
            i++;
            _context6.next = 2;
            break;

          case 11:
            _context6.next = 13;
            return genreService.findAllGenre();

          case 13:
            genre = _context6.sent;
            expect(genre).toHaveLength(saveGenre.length);

          case 15:
          case "end":
            return _context6.stop();
        }
      }
    }, _callee6);
  })));
  afterEach(
  /*#__PURE__*/
  (0, _asyncToGenerator2["default"])(
  /*#__PURE__*/
  _regenerator["default"].mark(function _callee7() {
    return _regenerator["default"].wrap(function _callee7$(_context7) {
      while (1) {
        switch (_context7.prev = _context7.next) {
          case 0:
            if (server) {
              server.close();
            }

          case 1:
          case "end":
            return _context7.stop();
        }
      }
    }, _callee7);
  })));
});