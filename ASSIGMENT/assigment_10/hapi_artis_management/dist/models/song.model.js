"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var SongModel = function SongModel(id, duration, songName, artisId) {
  (0, _classCallCheck2["default"])(this, SongModel);
  this.id = id;
  this.duration = duration;
  this.songName = songName;
  this.artisId = artisId;
};

var _default = SongModel;
exports["default"] = _default;