"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var ArtistModel = function ArtistModel(id, bornPlace, debut, name, genreId) {
  (0, _classCallCheck2["default"])(this, ArtistModel);
  this.id = id;
  this.bornPlace = bornPlace;
  this.debut = debut;
  this.name = name;
  this.genreId = genreId;
};

var _default = ArtistModel;
exports["default"] = _default;