"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = _default;

var _basicAuth = _interopRequireDefault(require("./basic-auth/basic-auth.plugin"));

// import authPlugin from './auth';
var plugins = [// authPlugin,
_basicAuth["default"]];

function _default(pluginOptions) {
  return plugins.map(function (plugin) {
    var options = pluginOptions[plugin.name];
    return {
      plugin: plugin,
      options: options
    };
  });
}

;