"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _login = _interopRequireDefault(require("../../service/login.service"));

var basic = _interopRequireWildcard(require("@hapi/basic"));

var validate = new _login["default"]().validate();
var basicAuthPlugin = {
  name: 'basicAuthPlugin',
  version: '1.0',
  register: function () {
    var _register = (0, _asyncToGenerator2["default"])(
    /*#__PURE__*/
    _regenerator["default"].mark(function _callee(server, options) {
      return _regenerator["default"].wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              _context.next = 2;
              return server.register(basic);

            case 2:
              server.auth.strategy('simple', 'basic', {
                validate: validate
              });

            case 3:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    }));

    function register(_x, _x2) {
      return _register.apply(this, arguments);
    }

    return register;
  }()
};
var _default = basicAuthPlugin;
exports["default"] = _default;