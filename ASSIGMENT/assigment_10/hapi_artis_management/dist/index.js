"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _connection = _interopRequireDefault(require("./db/connection"));

var _hapi = _interopRequireDefault(require("@hapi/hapi"));

var _config = _interopRequireDefault(require("./config"));

var _routes = _interopRequireDefault(require("./routes"));

var _auth = _interopRequireDefault(require("./config/auth.validate"));

process.on('unhandledRejection', function (err) {
  console.log(err);
  process.exit(1);
});

var _default =
/*#__PURE__*/
(0, _asyncToGenerator2["default"])(
/*#__PURE__*/
_regenerator["default"].mark(function _callee() {
  var config, connection, server;
  return _regenerator["default"].wrap(function _callee$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          config = new _config["default"]();
          config.configure();
          _context.next = 4;
          return (0, _connection["default"])();

        case 4:
          connection = _context.sent;
          server = _hapi["default"].server({
            port: process.env.APP_PORT,
            host: process.env.APP_HOST
          }); // config.getAuthConfig()
          //  const plugins = getPlugins({
          //      authPlugin: config.getAuthConfig(),
          //      basicAuthPlugin: config.getAuthConfig(),
          //  });
          //    await server.register(plugins);

          _context.next = 8;
          return server.register(require('@hapi/basic'));

        case 8:
          _context.next = 10;
          return server.register(require('@hapi/cookie'));

        case 10:
          server.auth.strategy('session', 'cookie', {
            cookie: {
              name: 'sid-example',
              password: '!wsYhFA*C2U6nz=Bu^%A@^F#SF3&kSR6',
              isSecure: false
            }
          });
          server.auth["default"]('session');
          _context.next = 14;
          return server.auth.strategy('simple', 'basic', {
            validate: _auth["default"]
          });

        case 14:
          server.route(_routes["default"]);

          if (!connection.isConnected) {
            _context.next = 21;
            break;
          }

          console.log('DATABASE CONNECTED');
          _context.next = 19;
          return server.start();

        case 19:
          console.log("DB connection name ".concat(connection.name));
          console.log('Server ', process.env.APP_NAME, ' running on ', server.info.uri);

        case 21:
          return _context.abrupt("return", server.listener);

        case 22:
        case "end":
          return _context.stop();
      }
    }
  }, _callee);
}));

exports["default"] = _default;