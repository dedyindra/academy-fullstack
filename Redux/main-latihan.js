// import {createStore} from "redux";
const {createStore} = require('redux')

const initial={
    number:0,
    person:{name:"",age:0}
}

function counter(state=initial,action) {
    switch (action.type) {
        case 'ADD_NEW_ELEMENT':
            return state.concat([0])
        case 'INCREMENT':
        return {...state,number: state.number+1}
        case 'DECREMENT':
            return {...state,number: state.number-1}
        case 'CHANGE_NAME':
            return {...state,person:{...state.person,name:action.newName}}
        default:return state;
    }
}
let store = createStore(counter);
store.subscribe(()=>{console.log(store.getState())})
store.dispatch({type:'INCREMENT'})
store.dispatch({type:'INCREMENT'})
store.dispatch({type:'INCREMENT'})
store.dispatch({type:'CHANGE_NAME',newName:"dedy"});
