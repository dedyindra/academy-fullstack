import {createStore} from "redux";
function counter(state=[],action) {
    switch (action.type) {
        case 'ADD_NEW_ELEMENT':
            return state.concat([{nama:"",age:0}])
        case 'INCREMENT':
            return state.map((element,index)=>{
                if (index==action.index){
                    return state={nama:element.nama + action.nama,age: action.age }
                }else {
                    return element
                }
            })
        case 'DECREMENT':
            return  state -1;
        default:return state;
    }
}
let store = createStore(counter);
store.subscribe(()=>{console.log(store.getState())})
store.dispatch({type:'ADD_NEW_ELEMENT'});
store.dispatch({type:'ADD_NEW_ELEMENT'});
store.dispatch({type:'ADD_NEW_ELEMENT'});
store.dispatch({type:'INCREMENT',index:1,nama:"dedy",age:0});
store.dispatch({type:'INCREMENT',index:1,nama:" indra",age:0});
store.dispatch({type:'INCREMENT',index:1,nama:" setiawan",age:0});
store.dispatch({type:'INCREMENT',index:0,nama:"w", age:50});
store.dispatch({type:'INCREMENT',index:0,nama:"wa",age:0});
