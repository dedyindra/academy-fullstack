import {createStore} from "redux";

function counter(state=[],action) {
switch (action.type) {
    case 'ADD_NEW_ELEMENT':
    return state.concat([0])
    case 'INCREMENT':
    return state.map((element,index)=>{
        if (index==action.index){
            return element +1
        }else {
            return element
        }
    })
    case 'DECREMENT':
        return state.map((element,index)=>{
            if (index==action.index){
                return element -1
            }else {
                return element
            }
        });
    default:return state;
}
}
let store = createStore(counter);
store.subscribe(()=>{console.log(store.getState())})
store.dispatch({type:'ADD_NEW_ELEMENT'});
store.dispatch({type:'ADD_NEW_ELEMENT'});
store.dispatch({type:'ADD_NEW_ELEMENT'});
store.dispatch({type:'INCREMENT'});
store.dispatch({type:'INCREMENT'});
store.dispatch({type:'INCREMENT',index:1});
