package com.enigma.model;

public class Block extends Rectangle{
    public Double height;
    public Double getVolume(){
        return this.length*this.width*this.height;
    }
    public  Block(Double length,Double width,Double height){
        super(length, width);
        this.height=height;
    }
    public Double getSurface(){
        return 2*(this.length*this.width)+2*(this.length*this.height)+2*(this.width*this.height);
    }
    public String print() {
        return "Block{ length = "+this.length+
                ", width ="+this.width+
                ", volume ="+getVolume()+
                ", surface ="+getSurface()+
                "round = "+getRound()+"}";
    }
}
