# Dedy Indra Setiawan :computer: [my profile](https://www.linkedin.com/in/dedy-indra-setiawan-b59b18142/)

Hari ini harus lebih baik dari hari kemarin :muscle:

#[Summary Class A](https://git.enigmacamp.com/enigma-camp/class-a/summary-class-a)

## JAVA 

#persiapan
- instal jdk
- intelij idea
- node js
- npm

#IDE 
- java
- eclipse
- netbin
- intelij

#case
- camelCase
- PascalCase
- UPPERCASE
- lowercase
- Snake_case
- kebab-case



"Jika kamu tidak sanggup menahan lelahnya belajar maka kamu harus sanggup menahan perihnya kebodohan" - Imam Syafi'i -



