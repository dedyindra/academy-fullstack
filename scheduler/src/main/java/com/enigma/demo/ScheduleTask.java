package com.enigma.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class ScheduleTask {
    @Autowired
   // RestTemplate restTemplate;
    // sheduler lebih prepare di pisahkan dari microservices tinggal hit api dengan rest template
    //corn pattern  untuk jalankan perintah appnya
    // " 0 */1 * * * *" corn per menit
    // "0 * * * * * 2017" corn pertahun
    @Scheduled(cron = "* * * * * *")
    public void jalan() {
        System.out.println("jalan");
        //restTemplate.exchange("",HttpMethod.get);
    }
}
