package com.enigma;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class hello extends HttpServlet {
    private static Logger logger = Logger.getLogger(hello.class.getName());

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.log(Level.INFO,"do get dipanggil");
        String name = req.getParameter("name");
        String output = "<html>"+"<body>"+
                "<h1>Aing "+ name+" development</h1>"+
                "</body>"+"</html>";
        resp.setContentType("text/html");
        resp.getWriter().print(output);

    }

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.log(Level.INFO,"service dipanggil");
        super.service(req, resp);
        System.out.println("servis terpanggil");
    }

    @Override
    public void init() throws ServletException {
        logger.log(Level.INFO,"init dipanggil");
        super.init();
        System.out.println("init terpanggil");
    }
}
