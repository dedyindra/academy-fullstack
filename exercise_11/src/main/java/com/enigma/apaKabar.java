package com.enigma;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class apaKabar extends HttpServlet {
    private static Logger logger = Logger.getLogger(apaKabar.class.getName());

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.log(Level.INFO,"do get dipanggil");
        String angka1 = req.getParameter("angka1");
        String angka2 = req.getParameter("angka2");
        Integer hasil = Integer.parseInt(angka1)+Integer.parseInt(angka2);
        req.setAttribute("hasil",String.valueOf(hasil));
        req.getRequestDispatcher("/hasil/hasilnya.jsp").forward(req,resp);

//        String output = "<html>"+"<body>"+
//                "<h1>Apa kabar dunia</h1>"+
//                "</body>"+"</html>";
//        resp.setContentType("text/html");
//        resp.getWriter().print(output);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String angka1 = req.getParameter("angka1");
        String angka2 = req.getParameter("angka2");
        Integer hasil = Integer.parseInt(angka1)+Integer.parseInt(angka2);
        req.setAttribute("hasil",String.valueOf(hasil));
        req.getRequestDispatcher("/hasil/hasilnya.jsp").forward(req,resp);

    }
}
