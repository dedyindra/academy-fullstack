package com.enigma.config;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HibernateConfig {
    private static SessionFactory sessionFactory;
    private static SessionFactory buildSessionFactory(){
        Configuration configuration = new Configuration();
        SessionFactory sessionFactory = configuration.configure("hibernate.cfg.xml").buildSessionFactory();
        return sessionFactory;
    }
    public static SessionFactory getSessionFactory(){
        if (sessionFactory==null) return buildSessionFactory();
        return sessionFactory;
    }

}
