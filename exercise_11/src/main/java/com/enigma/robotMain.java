package com.enigma;

import com.enigma.model.Direction;
import com.enigma.model.Robot;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.logging.Logger;

public class robotMain extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String koordinat = req.getParameter("angka1");
        String[] xy = koordinat.split(",");
        Robot baymax = new Robot(Integer.parseInt(xy[0]),Integer.parseInt(xy[1]), Direction.valueOf(xy[2]));
        String jumData = req.getParameter("angka3");
        int fuel = Integer.parseInt(jumData);
        baymax.fillFuel(fuel);
        String comands = req.getParameter("angka2");
        baymax.comand(comands);
         baymax.run();
       String bayma = baymax.print();
        req.setAttribute("hasil",bayma);
        req.getRequestDispatcher("/hasil/hasilnya.jsp").forward(req,resp);
    }
}
