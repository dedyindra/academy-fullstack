package com.enigma.model;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "student")
public class Student {
    @Id
    private Integer id;
    @Column(name = "name")
private String name;
    @Column(name = "birth_place")
private String birthPlace;
    @Column(name = "birth_date")
private Date birthDate;
    @Column(name = "gender")
private String gender;

@ManyToOne
@JoinColumn(name = "room_id")
private Room room;
@OneToMany(mappedBy = "students")
private List<Student_Subject> student_subject;

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    public List<Student_Subject> getStudent_subject() {
        return student_subject;
    }

    public void setStudent_subject(List<Student_Subject> student_subject) {
        this.student_subject = student_subject;
    }

    public Student(Integer id, String name, String birthPlace, Date birthDate, String gender) {
        this.id = id;
        this.name = name;
        this.birthPlace = birthPlace;
        this.birthDate = birthDate;
        this.gender = gender;
     //  this.room = room;
    }

    public Student() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBirthPlace() {
        return birthPlace;
    }

    public void setBirthPlace(String birthPlace) {
        this.birthPlace = birthPlace;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", birthPlace='" + birthPlace + '\'' +
                ", birthDate=" + birthDate +
                ", gender='" + gender + '\'' +
                '}';
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }
}
