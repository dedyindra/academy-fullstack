package com.enigma.model;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "student_subject")
public class Student_Subject {
    @Id
    private Integer id;
    @ManyToOne
    @JoinColumn(name = "student_id")
    private Student students;
    @ManyToOne
    @JoinColumn(name = "subject_id")
    private Subject subject;

    public Student_Subject(Integer id, Student students, Subject subject) {
        this.id = id;
        this.students = students;
        this.subject = subject;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Student getStudents() {
        return students;
    }

    public void setStudents(Student students) {
        this.students = students;
    }

    public Subject getSubject() {
        return subject;
    }

    public void setSubject(Subject subject) {
        this.subject = subject;
    }

    @Override
    public String toString() {
        return "Student_Subject{" +
                "id=" + id +
                ", students=" + students +
                ", subject=" + subject +
                '}';
    }
}
