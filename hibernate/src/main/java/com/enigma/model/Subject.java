package com.enigma.model;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "subject")
public class Subject {
    @Id
    private Integer id;
    @Column(name = "subject_name")
private  String subject;
    @Column(name = "sks")
private String sks;
    @OneToMany(mappedBy = "subject")
    private List<Student_Subject> student_subject;

    public Subject(Integer id, String subject, String sks) {
        this.id = id;
        this.subject = subject;
        this.sks = sks;
    }

    public List<Student_Subject> getStudent_subjects() {
        return student_subject;
    }

    public void setStudent_subjects(List<Student_Subject> student_subjects) {
        this.student_subject = student_subjects;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getSks() {
        return sks;
    }

    public void setSks(String sks) {
        this.sks = sks;
    }

    @Override
    public String toString() {
        return "Subject{" +
                "id=" + id +
                ", subject='" + subject + '\'' +
                ", sks='" + sks + '\'' +
                ", student_subject=" + student_subject +
                '}';
    }
}
