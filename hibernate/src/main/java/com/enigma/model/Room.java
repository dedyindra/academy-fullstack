package com.enigma.model;

import javax.persistence.*;
import java.util.List;
@Entity
@Table(name = "room")
public class Room {
    @Id
private Integer id;
    @Column
private String name;
    @Column
private Integer capacity;
@OneToMany(mappedBy = "room")
    private List<Student> students;

    public Room(Integer id, String name, Integer capacity, List<Student> students) {
        this.id = id;
        this.name = name;
        this.capacity = capacity;
        this.students = students;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getCapacity() {
        return capacity;
    }

    public void setCapacity(Integer capacity) {
        this.capacity = capacity;
    }

    public List<Student> getStudents() {
        return students;
    }

    public void setStudents(List<Student> students) {
        this.students = students;
    }

    @Override
    public String toString() {
        return "Room{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", capacity=" + capacity +
                ", students=" + students +
                '}';
    }
}
