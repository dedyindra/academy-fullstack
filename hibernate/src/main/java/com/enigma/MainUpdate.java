package com.enigma;

import com.enigma.model.Student;
import com.enigma.service.StudentService;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MainUpdate {
    public static void main(String[] args) throws Exception{

        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("masukan id yang akan di update :");
        Integer id = Integer.parseInt(bufferedReader.readLine());
        System.out.print("Name :");
        String name = bufferedReader.readLine();
        System.out.print("Birth Place :");
        String brithplace =bufferedReader.readLine();
        System.out.print("Birth Date :");
        String birth_date =bufferedReader.readLine();
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date fd = formatter.parse(birth_date);
        java.sql.Date date = new java.sql.Date(fd.getTime());
        System.out.print("Gender :");
        String gender = bufferedReader.readLine();
        Student student = new Student(id,name,brithplace,date,gender);
        StudentService.update(student);


    }
}
