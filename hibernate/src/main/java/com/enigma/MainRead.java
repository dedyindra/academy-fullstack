package com.enigma;

import com.enigma.config.HibernateConfig;
import com.enigma.model.Student;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.util.List;

public class MainRead {
    public static void main(String[] args) {
        SessionFactory sessionFactory = HibernateConfig.getSessionFactory();
        Session session = sessionFactory.getCurrentSession();

        //read
        session.beginTransaction();
        List<Student> students = session.createQuery("from com.enigma.model.Student").getResultList();
        session.getTransaction().commit();
        for (Student student:students
             ) {
            System.out.println(student.toString());

        }
    }
}
