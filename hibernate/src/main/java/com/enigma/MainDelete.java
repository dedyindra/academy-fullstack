package com.enigma;

import com.enigma.model.Student;
import com.enigma.service.StudentService;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class MainDelete {
    public static void main(String[] args) throws Exception{
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("masukan id yang akan di hapus :");
        Integer id = Integer.parseInt(bufferedReader.readLine());
        StudentService.deleteStudent(id);

    }
}
