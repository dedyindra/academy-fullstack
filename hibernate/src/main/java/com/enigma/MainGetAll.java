package com.enigma;

import com.enigma.model.Student;
import com.enigma.service.StudentService;

import java.util.List;

public class MainGetAll {
    public static void main(String[] args) {
        List<Student> students = StudentService.getAllStudent();
        for (Student student:students) {
            System.out.println(student.toString());
        }
    }
}
