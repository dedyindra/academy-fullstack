package com.enigma;

import com.enigma.model.Room;
import com.enigma.model.Student;
import com.enigma.service.StudentService;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MainCreate {
    public static void main(String[] args) throws Exception {

        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("masukan banyak insert data :");
        Integer length = Integer.parseInt(bufferedReader.readLine());
        Student insertstudent= new Student();
        for (int i = 0; i < length ; i++) {
            System.out.print("Id :");
            Integer id = Integer.parseInt(bufferedReader.readLine());
            insertstudent.setId(id);
            System.out.print("nama :");
            String name = bufferedReader.readLine();
            insertstudent.setName(name);
            System.out.print("birth place :");
            String birth_place = bufferedReader.readLine();
            insertstudent.setBirthPlace(birth_place);
            System.out.print("birth date :");
            String birth_date = bufferedReader.readLine();
            System.out.print("gender :");
            String gender = bufferedReader.readLine();
            insertstudent.setGender(gender);
            System.out.print("room id :");
           // Integer room = Integer.parseInt(bufferedReader.readLine());
           // insertstudent.setRoom(room);
            DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            Date fd = formatter.parse(birth_date);
            java.sql.Date sqlDate = new java.sql.Date(fd.getTime());
            insertstudent.setBirthDate(fd);

            StudentService.create(insertstudent);
        }

    }
}
