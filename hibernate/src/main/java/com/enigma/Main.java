package com.enigma;

import com.enigma.config.HibernateConfig;
import com.enigma.model.Room;
import com.enigma.model.Student;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.util.Date;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        SessionFactory sessionFactory = HibernateConfig.getSessionFactory();
        Session session = sessionFactory.getCurrentSession();
//     session.beginTransaction();
//        Student student = session.get(Student.class,1);
session.beginTransaction();
Room room = session.get(Room.class,1);
//  System.out.println(room.toString());
        List<Student> students = room.getStudents();
        for (Student student :students
             ) {
            System.out.println(student.toString());

        }
session.getTransaction().commit();
        //        Student student = session
       // System.out.println(student.getName());
    }
}
