package com.enigma;

import com.enigma.model.Student;
import com.enigma.service.StudentService;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

public class MainGetId {
    public static void main(String[] args)throws Exception {
        BufferedReader bufferedReader=new BufferedReader(new InputStreamReader(System.in));
        System.out.print("Masukkan id :");
        Integer search =Integer.parseInt(bufferedReader.readLine());
        List<Student> students = StudentService.getById(search);
        for (Student student:students) {
            System.out.println(student.toString());
        }
    }
}
