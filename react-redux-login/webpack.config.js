var path    = require('path');
var hwp     = require('html-webpack-plugin');

module.exports = {
    devServer: {
        contentBase: 'http://localhost',
        port: 7070,
        // Send API requests on localhost to API server get around CORS.
        proxy: {
            '/api': {
                target: {
                    host: "localhost",
                    protocol: 'http:',
                    port: 3000
                }
            }
        }
    }
}