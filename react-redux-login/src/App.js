import React from 'react';
import logo from './logo.svg';
import './App.css';
import {Provider} from "react-redux";
import {createStore} from "redux";
import artist from "./reducer/ArtistReducer";
import {BrowserRouter as Router, Route, Switch, Link, Redirect} from "react-router-dom";
import ArtistContainer from "./artist/ArtistContainer";
import Login from "./Login";

class App extends React.Component {
    render() {
        return (
            <Router>
                <div className="App">
                    <Switch>
                        <Redirect from="/" exact to="/login" />
                        <Provider store={createStore(artist)}>
                        <Route path="/login" component={Login}/>
                        <Route path="/artis">

                                <ArtistContainer/>

                        </Route>
                        </Provider>
                    </Switch>
                </div>
            </Router>
        );
    }
}

export default App;

