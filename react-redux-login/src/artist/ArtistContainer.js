import React from 'react';
import ArtistTable from "./ArtistTable";
import {connect} from "react-redux";
import {fetchDataArtist} from "../service/Service";
class ArtistContainer extends React.Component {
    componentDidMount() {
        this.fetchDetail()
    }
    fetchDetail = async () => {
        const data = await fetchDataArtist();
        this.props.dispatch({type:"GET_ARTIST",payload:data});
    }
    render() {
        console.log(this.props.artists  ,"dadada")
        return (
            <div className="row">
                {this.props.artists.map((element, index) => {
                    return <div className="custom-row">
                        <ArtistTable element={element} index={index} />
                    </div>
                })}
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {...state,artists: state.artists}
}

export default connect(mapStateToProps)(ArtistContainer)
