import React from 'react';
import { Link} from "react-router-dom";
import '../materialize.min.css'
class ArtistTable extends React.Component {
    render() {
        return (
            <div className="card-panel teal">
                    <p>{this.props.element.name} </p>
                    <p>{this.props.element.bornPlace} </p>
                    <p>{this.props.element.debut} </p>
                    <Link  className="waves-effect waves-light btn" to={`/list-song/${this.props.element.id}`}>List of Song</Link>
            </div>
        );
    }
}
export default ArtistTable;
