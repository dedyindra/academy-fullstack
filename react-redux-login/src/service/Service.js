export async function fetchDataArtist() {
    const data =  await  fetch(`http://localhost:7070/Artist-list`, {method:'GET'})
        .then((res)=>{
            return res.json()
        })
    return data;
}

export async  function fetchDataDetail(id) {
    const data =  await  fetch(`http://localhost:7070/artist/${id}/song`, {method:'GET'})
        .then((res)=>{
            console.log(res)
            return res.json()
        })
    return data;
}
export async  function PostData(artist) {
    return await fetch('http://localhost:7070/Artist', {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            "Content-type": "application/json"
        },
        body: JSON.stringify(artist)
    })
        .then((res) => {
            return res.json()
            console.log(res.json())
        }).catch(err => err);
}
