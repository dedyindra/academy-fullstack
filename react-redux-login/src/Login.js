import React from 'react';
import './style.css'
import {connect} from "react-redux"
class Login extends React.Component {
    render() {
        console.log("Siccess")
        return (
            <div>
                <div className="kotak_login">
                    <form>
                        <label>Username</label>
                        <input type="text" onChange={(event) => {
                            this.props.dispatch({type:"HANDLE_CHANGE_USERNAME",newName: event.target.value
                            })
                        }}  name="username" className="form_login" placeholder="Username atau email .."/>
                        <label>Password</label>
                        <input onChange={(event) => {
                            this.props.dispatch({type:"HANDLE_CHANGE_PASSWORD",newPassword: event.target.value
                            })
                        }} type="text" name="password" className="form_login" placeholder="Password .."/>
                        <input type="submit" className="tombol_login" value="LOGIN"/>
                    </form>
                </div>
            </div>
        );
    }
}

export default connect()(Login) ;