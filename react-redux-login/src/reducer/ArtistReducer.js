const intialState = {
    artists: [],
    crediential:{username:"",password:""}
}
export default function artist(state = intialState, action) {
    console.log(state, action, "ini artist")
    switch (action.type) {
        case "GET_ARTIST":
            return {...state, artists: action.payload};
        case "GET_SONG":
            return {...state, song: action.payload};
        case 'HANDLE_CHANGE_USERNAME':
            return  {...state, crediential: {...state.crediential, username: action.newName }}
        case 'HANDLE_CHANGE_PASSWORD':
            return  {...state, crediential: {...state.crediential, password: action.newPassword }}
        default:
            return state;
    }
}