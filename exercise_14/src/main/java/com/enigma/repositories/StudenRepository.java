package com.enigma.repositories;

import com.enigma.model.Artist;
import com.enigma.model.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StudenRepository extends JpaRepository<Student,Integer> {
}
