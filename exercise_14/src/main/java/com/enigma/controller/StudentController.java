package com.enigma.controller;

import com.enigma.model.Student;
import com.enigma.repositories.StudenRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
public class StudentController {
    @Autowired
    StudenRepository studenRepository;

    @GetMapping("/")
    public String doWhatEver(){
        return "index";
    }

    @GetMapping("/student-form")
    public ModelAndView tostudentform()
    {
        return new ModelAndView("student-form","student",new Student());
    }
    @PostMapping("/student")
    public String toStudent(@ModelAttribute("student") Student student,Model model){
        studenRepository.save(student);
        List<Student> students = studenRepository.findAll();
        model.addAttribute("x",student);
        model.addAttribute("studentList",students);
        return "student-view";
    }




    //untuk pemanggilan parameter satu satu
//    @PostMapping("/student")
//    public String toStudentForm(@RequestParam Integer id, @RequestParam String name, Model model){
//        Student student = new Student(id,name);
//        model.addAttribute("x",student);
//        return "student-view";
//    }

//    @GetMapping("/student-form")
//    public String toStudentForm(){
//        return "student-form";
//}
  }
