package com.enigma.controller;

import com.enigma.model.Artist;
import com.enigma.repositories.ArtistRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
public class ArtistController {
    @Autowired
    ArtistRepository artistRepository;

    @GetMapping("/artist")
    public String toAartistForm(Model model) {
        List<Artist> artists = artistRepository.findAll();
        model.addAttribute("artist_List", artists);
        return "artist/artist";
    }
    @GetMapping("/detail")
    public ModelAndView toAartistdetail(@RequestParam Integer id ) {
        Artist artists =  artistRepository.findById(id).get();
        return new ModelAndView("artist/artist-form","artist", artists);
    }
    @GetMapping("/artist-form")
    public ModelAndView toartistform() {
        return new ModelAndView("artist/artist-form", "artist", new Artist());
    }
    @PostMapping("/artist")
    public String toArtistView(@ModelAttribute("artist") Artist artist, Model model) {
        artistRepository.save(artist);
        return "redirect:/artist";
    }

}
