package com.enigma.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
@Entity
public class Song {
    @Id
    private Integer id;
    private String lagu;

    @ManyToOne
    private Artist artist;

    public Song() {
    }

    public Song(Integer id, String lagu) {
        this.id = id;
        this.lagu = lagu;
    }

    public Artist getArtist() {
        return artist;
    }

    public void setArtist(Artist artist) {
        this.artist = artist;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLagu() {
        return lagu;
    }

    public void setLagu(String lagu) {
        this.lagu = lagu;
    }
}
