<%--
  Created by IntelliJ IDEA.
  User: Enigmacamp
  Date: 10/7/2019
  Time: 4:00 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<form>
    <c:forEach items="${artist_List}" var="artist">
        <fmt:label path="id">id</fmt:label>
        <input type="text" name="id" value="${artist.id}">
        <fmt:label path="name">name</fmt:label>
        <input type="text" name="name" value="${artist.name}"><br>
        <input type="text" name="artistForm" value="${artist.artistFrom}">
        <fmt:label path="debut">debut</fmt:label>
        <input type="date" name="debut" value="${artist.debut}">
        <input type="submit">
    </c:forEach>
</form>
</body>
</html>
