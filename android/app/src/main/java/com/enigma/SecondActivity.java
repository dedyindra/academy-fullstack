package com.enigma;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class SecondActivity extends AppCompatActivity {
    TextView text1;
    public static final String EXTRA_DATA = "EXTRA_DATA";
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final Intent intent = getIntent();
        final String data = intent.getStringExtra(EXTRA_DATA);
        text1 =  findViewById(R.id.textView1);
        text1.setText(data);
    }
    @Override
    protected void onStop() {
        super.onStop();
        System.out.println("second onStop() called");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        System.out.println(" second onDestroy() called");
    }

    @Override
    protected void onPause() {
        super.onPause();
        System.out.println(" second onPause() called");
    }

    @Override
    protected void onResume() {
        super.onResume();
        System.out.println(" second onResume() called");
    }
}
