package com.enigma;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.firebase.FirebaseError;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.google.firebase.crashlytics.core.CrashlyticsCore;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    Button button1;
    Button reset;
    TextView text1;
    EditText inputText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        System.out.println("onCreate() called");
        setContentView(R.layout.another_layout);
        button1 =  findViewById(R.id.button);
        inputText =  findViewById(R.id.editText);
        reset =  findViewById(R.id.button3);
        text1 =  findViewById(R.id.textView2);

        button1.setOnClickListener(this);
        inputText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                text1.setText(s);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }



    public void kepencet(View v){
        String nama = inputText.getText().toString();
        text1.setText(nama);
    }
    public void reset(View v){
        text1.setText("");
        inputText.setText("");
    }

    public void move(View view){
        String nama = inputText.getText().toString();
        Intent intent = new Intent(this,SecondActivity.class);
        intent.putExtra(SecondActivity.EXTRA_DATA, nama);
        startActivity(intent);
    }
    @Override
    protected void onStart() {
        super.onStart();
        System.out.println("onStart() called");
    }

    @Override
    protected void onStop() {
        super.onStop();
        System.out.println("onStop() called");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        System.out.println("onDestroy() called");
    }

    @Override
    protected void onPause() {
        super.onPause();
        System.out.println("onPause() called");
    }

    @Override
    protected void onResume() {
        super.onResume();
        System.out.println("onResume() called");
    }

    @Override
    public void onClick(View v) {
        //try{
            throw new RuntimeException("Test Crash");
//        }catch(Exception e){
//            FirebaseCrashlytics.getInstance().log(e.getMessage());
//        }
    }
}

