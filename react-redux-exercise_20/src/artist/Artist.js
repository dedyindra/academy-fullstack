import React from 'react';
import '../materialize.min.css'
import {connect} from "react-redux"
class Artist extends React.Component {
    render() {
        return (
            <div className="card-panel teal">
                <h1>{this.props.element.name}</h1>
                <h1>{this.props.element.bornPlace}</h1>
            </div>
        );
    }
}
export default Artist;
