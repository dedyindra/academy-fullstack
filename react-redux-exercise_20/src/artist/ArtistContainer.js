import React from 'react';
import '../materialize.min.css'
import {connect} from "react-redux"
import {fetchDataArtist} from "../element/Service";
import Artist from "./Artist";

class ArtistContainer extends React.Component {
    componentDidMount() {
        this.fetchDetail()
    }


    fetchDetail = async () => {
        const data = await fetchDataArtist();
        this.props.dispatch({type:"ARTISTGET",payload:data});
    }

    render() {
        console.log(this.props.artist  ,"dadada")
        return (
            <div className="row">
                    {this.props.artist.map((element, index) => {
                        return <div className="custom-row">
                          <Artist element={element} index={index} />
                        </div>
                    })}
            </div>
        );
    }
}
const mapStateToProps = (state) => {
    return {...state,artist: state.artist}
}
export default connect(mapStateToProps)(ArtistContainer);
