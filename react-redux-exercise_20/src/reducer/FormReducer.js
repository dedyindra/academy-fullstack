const intialState = {
    song: [],
}
export  default  function song(state=intialState,action) {
    console.log(state,action)
    switch (action.type) {
        case 'ADD_ELEMENT':
            return {...state, song : state.song.concat([{title:"",album:0}])};
        case 'HANDLE_CHANGE_TITLE':
            return {...state, song : state.song.map((element,index)=>{
                    if (index===action.index){
                     return   {...element, title: action.newtitle}
                    }else {
                        return element
                    }
                })}
        case 'HANDLE_CHANGE_ALBUM':
            return {...state, song : state.song.map((element,index)=>{
                    if (index===action.index){
                        return   {...element,album: action.newalbum}
                    }else {
                        return element
                    }
                })}

        default:return state;
    }



}