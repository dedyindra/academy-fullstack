export async function fetchDataArtist() {
    const data =  await  fetch(`http://localhost:7070/Artist-list`, {method:'GET'})
        .then((res)=>{
            return res.json()
        })
    return data;
}