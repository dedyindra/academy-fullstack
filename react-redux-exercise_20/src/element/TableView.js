import React from 'react';
import '../materialize.min.css'
import {connect} from "react-redux"
import CardView from "./CardView";
class TableView extends React.Component {
    render() {
        return (
                <div className="card-panel teal">
                    <table>
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>Title</th>
                            <th>Album</th>
                        </tr>
                        </thead>
                        {this.props.song.map((element ,index )=>{
                            return <tr>
                                <td>{index+1}</td>
                                    <td>{element.title}</td>
                                    <td>{element.album}</td>
                                </tr>
                        })}
                    </table>
                </div>
        );
    }
}
const mapStateToProps=(state)=>{
    return {song: state.song}
}
export default connect(mapStateToProps)(TableView);
