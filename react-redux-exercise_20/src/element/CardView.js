import React from 'react';
import '../materialize.min.css'
import {connect} from "react-redux"
import {changeAge, changeAlbum, changeName, changeTitle} from "./Action";
class CardView extends React.Component {
    render() {
        return (
                <div className="card-panel teal">
                    <label><b>{this.props.index +1 }</b> </label>
                    <form>
                        <label>Title :</label><br/>
                        <input onChange={(event)=>{ this.props.dispatch({...changeTitle,
                            index : this.props.index,newtitle:event.target.value})}} value={this.props.title} type="text"/><br/>
                        <label>Album :</label><br/>
                        <input onChange={(event)=>{ this.props.dispatch({...changeAlbum,
                            index : this.props.index,newalbum:event.target.value})}} value={this.props.album} type="number"/><br/>
                    </form>
                </div>

        );
    }
}

export default connect()(CardView);
