import React from 'react';
import '../materialize.min.css'
import {connect} from "react-redux"
import CardView from "./CardView";
import {addElemenAction} from "./Action";
import TableView from "./TableView";
import {fetchDataArtist} from "./Service";
import Artist from "../artist/Artist";

class FormContainer extends React.Component {
     render() {
        return (
            <div className="row">
                <div className="col s3">
                    <button className="waves-effect waves-light btn" onClick={() => {
                        this.props.dispatch(addElemenAction)
                    }}> Tambah Form
                    </button>
                    <TableView/>

                </div>

                <div className="col s9">
                    {this.props.song.map((element, index) => {
                        return <div className="custom-row">
                            <CardView index={index} element={element} key={index}/>
                        </div>
                    })}

                </div>
            </div>

        );
    }
}


const mapStateToProps = (state) => {
    return {...state}
}
export default connect(mapStateToProps)(FormContainer);
