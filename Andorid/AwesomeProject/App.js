/* eslint-disable prettier/prettier */
/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    Image,
} from 'react-native';

import {
    Header,
    LearnMoreLinks,
    Colors,
    DebugInstructions,
    ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import StarRating from 'react-native-star-rating';

export default class Bananas extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            starCount: 3.5,
        };
    }

    onStarRatingPress(rating) {
        this.setState({
            starCount: rating,
        });
    }

    render() {
        let pic = {
            uri: 'https://upload.wikimedia.org/wikipedia/commons/7/7d/Mount_Bromo_at_sunrise%2C_showing_its_volcanoes_and_Mount_Semeru_%28background%29.jpg',
        };
        let pic2 = {
            uri: 'https://beritajatim.com/wp-content/uploads/2019/02/shutterstock_758055910.jpg',
        };
        let pic3 = {
            uri: 'https://cdn2.tstatic.net/wartakota/foto/bank/images/bromo-satu.jpg',
        };
        let pic4 = {
            uri: 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcS_QaNIXAs1P_9yAt9Kd_h1vPiv1VYf7Lu4i9Vw_A8cBh42K6l3',
        };
        let pic5 = {
            uri: 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSNBLA4caD6HfIWOytX-jazPqAMGKLdJSaT1FdeFczKS5fKzDae',
        };
        let pic6 = {
            uri: 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQTDVc2YIw5eShOENSRDzb6XZHPf-cOrQxhBwWvltE0BKBfNTLb',
        };
        let pic7 = {
            uri: 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTdNPYBJRvstYjuyBatu4WMRqcb3pnkM6ZLTcc6nfY1pbDakSln',
        };
        return (
            <ScrollView>
                <View>
                    <View style={styles}>
                        <Image source={pic} style={styles.bigView}/>
                    </View>
                    <View>
                        <Text style={styles.textHeader}>Gunung bromo bintang lima</Text>
                    </View>ghgh
                    <View style={styles.body}>
                        <View style={styles.bodyContent}>
                            <View style={styles.menuBox}>
                                <Image source={pic2} style={styles.smallView}/>
                            </View>
                            <View style={styles.menuBox}>
                                <Image source={pic3} style={styles.smallView}/>
                            </View>
                            <View style={styles.menuBox}>
                                <Image source={pic4} style={styles.smallView}/>
                            </View>
                            <View style={styles.menuBox}>
                                <Image source={pic5} style={styles.smallView}/>
                            </View>
                            <View style={styles.menuBox}>
                                <Image source={pic6} style={styles.smallView}/>
                            </View>
                            <View style={styles.menuBox}>
                                <Image source={pic7} style={styles.smallView}/>
                            </View>
                        </View>
                    </View>
                </View>
            </ScrollView>
        );
    }
}
const styles = StyleSheet.create({
    bigView: {
        width: 360,
        height: 250,
    },
    smallView: {
        width: 100,
        height: 100,
    },
    headerContent: {
        padding: 30,
        alignItems: 'center',
    },
    body: {
        flex: 1,
        alignItems: 'center',
        padding: 30,
    },
    bodyContent: {
        paddingTop: 10,
        flexDirection: 'row',
        flexWrap: 'wrap',
    },
    menuBox: {
        backgroundColor: '#DCDCDC',
        width: 100,
        height: 100,
        alignItems: 'center',
        justifyContent: 'center',
        margin: 25,
        shadowColor: 'black',
        shadowOpacity: 2,
        shadowOffset: {
            height: 2,
            width: -2,
        },
        elevation: 4,
    },
    textHeader: {
        fontSize: 20,
        fontStyle: 'italic',
        textAlign: 'center',
        marginTop: 20,
    },
});

