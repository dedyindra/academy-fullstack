import com.enigma.model.*;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException{
//=======================abstraktion====================
        BufferedReader dataIn = new BufferedReader(new InputStreamReader( System.in) );
        Skill jump = new Skill("jump",22,33);
        AssasinHeroes balmond = new AssasinHeroes("ball",40,44,44,jump);
        Heroes gundala = new Heroes("gundala",44,44,44,jump);
        Heroes saitama = new Heroes("Saitama",1000,500,50,jump);
        Tower tower = new Tower(3000,50);
        AssasinHeroes ninja = new AssasinHeroes("ninja",70,50,10,jump);
        saitama.attack(tower);
        saitama.castSkill(gundala);
        saitama.castSkill(ninja);
        System.out.println(gundala.print());
        System.out.println(saitama.print());
        System.out.println(tower.print());


    }

}
