package com.enigma.services;

import com.enigma.entities.Product;
import com.enigma.entities.Store;
import com.enigma.execption.InsufficientQuantityException;
import com.enigma.repositories.ProductRepository;
import com.enigma.services.impl.ProductService;
import com.enigma.services.impl.StoreService;
import com.enigma.spesification.ProductSpesification;
import com.enigma.spesification.StoreSpesification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Logger;

@Service
public class ProductServiceImplement implements ProductService {
    @Autowired
    ProductRepository productRepository;
    @Autowired
    StoreService storeService;
    private static Logger logger = Logger.getLogger(ProductServiceImplement.class.getName());

    @Override
    public Product save(Product product) {
        return productRepository.save(product);
    }

    @Override
    public Product getProduct(String id) {
        if (!productRepository.findById(id).isPresent()) {
            return new Product();
        }

        return productRepository.findById(id).get();
    }

    @Override
    public Page<Product> GetAlLProductWithSpesification(Pageable pageable, Product product) {
        return productRepository.findAll(ProductSpesification.findByCriterias(product),pageable);
    }

    @Override
    public List<Product> getProductByStoreId(Integer id) {
        Store store = storeService.getStore(id);
        return store.getProducts();
    }

    //=========PAKAI METHOD QUERY===========
    @Override
    public List<Product> GetAllByName(String Keyword) {
        return productRepository.findAllByNameContains(Keyword);
    }

    @Override
    public void deduct(String id, Integer quantity) {
        Product product = getProduct(id);
        if (product.getQuantity() < quantity) {
            throw new InsufficientQuantityException();
        }
        product.deductQuantity(quantity);
        save(product);
    }

    @Override
    public BigDecimal getProductById(String id) {
        return getProduct(id).getPrice();
    }

    @Override
    public Page<Product> GetAll(Pageable pageable) {
        return productRepository.findAll(pageable);
    }

    @Override
    public void delete(String id) {
        productRepository.deleteById(id);
    }

    @Override
    public void update(Product product) {

    }

    @Override
    public Product saved(Product product) {
        Store store =  storeService.getStore(product.getPasangIdStore());
        product.setStoreId(store);
        return productRepository.save(product);
    }

    @Override
    public Product saving(Integer id,Product product){
        Store store =  storeService.getStore(id);
        product.setStoreId(store);
        return productRepository.save(product);
    }

}
