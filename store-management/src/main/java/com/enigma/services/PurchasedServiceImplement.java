package com.enigma.services;

import com.enigma.entities.Product;
import com.enigma.entities.Purchased;
import com.enigma.entities.Users;
import com.enigma.repositories.PurchasedRepository;
import com.enigma.services.impl.ProductService;
import com.enigma.services.impl.PurchasedService;
import com.enigma.services.impl.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PurchasedServiceImplement implements PurchasedService {
    @Autowired
    PurchasedRepository purchasedRepository;
@Autowired
    UsersService usersService;
@Autowired
ProductService productService;
    @Override
    public void save(Purchased purchased) {
        purchasedRepository.save(purchased);
    }
    @Override
    public Purchased getPurchased(String id) {
        if (!purchasedRepository.findById(id).isPresent()){
            return new Purchased();
        }
        return purchasedRepository.findById(id).get();
    }
    @Override
    public List<Purchased> GetAll() {
        return purchasedRepository.findAll();
    }
    @Override
    public Purchased purchasing(Purchased purchased) {
       productService.deduct(purchased.getProductId() ,purchased.getQuantity());
        purchased.setPurchasedPrice(productService.getProductById(purchased.getProductId()));
     return    purchasedRepository.save(purchased);

    }
    @Override
    public List<Purchased> getPurchasedfromuser(String id) {
        Users users = usersService.getUserById(id);
        return users.getPurchaseds();
    }
    @Override
    public Purchased saving(String id,Purchased purchased){
        Users users =  usersService.getUserById(id) ;

        Product product = productService.getProduct(purchased.getProductId());
         purchased.setUsersid(users);
         purchased.setProductIdok(product);

        productService.deduct(purchased.getProductId() ,purchased.getQuantity());
        purchased.setPurchasedPrice(productService.getProductById(purchased.getProductId()));
        return purchasedRepository.save(purchased);
    }
}
