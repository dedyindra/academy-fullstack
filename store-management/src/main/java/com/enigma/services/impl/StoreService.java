package com.enigma.services.impl;

import com.enigma.entities.Store;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;

public interface StoreService {
     Store save(Store store);
     Store getStore(Integer id);
     Page<Store> GetAll(Pageable pageable, String keyword);
     public Page<Store> GetAllStoreWithSpesification(Pageable pageable, Store store);
     Page<Store> GetAllStore(Pageable pageable, Example<Store> exampleMatcher);
     void delete(Integer id);
     void update(Store store);

}
