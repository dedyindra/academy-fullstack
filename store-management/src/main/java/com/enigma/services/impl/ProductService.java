package com.enigma.services.impl;

import com.enigma.entities.Product;
import com.enigma.entities.Store;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.math.BigDecimal;
import java.util.List;

public interface ProductService {
    Product save(Product product);
    Product saving(Integer id,Product product);
    Product saved(Product product);
    Product getProduct(String  id);
    Page<Product> GetAll(Pageable pageable);
    public List<Product>getProductByStoreId(Integer id);
    public Page<Product> GetAlLProductWithSpesification(Pageable pageable, Product product);
    List<Product> GetAllByName(String Keyword);
    void delete(String id);
    void update(Product product);
   BigDecimal getProductById(String id);
   void deduct(String id,Integer quantity);
   //void ValidationQuantity(String id,Integer quantity);
    //List<Product>
}
