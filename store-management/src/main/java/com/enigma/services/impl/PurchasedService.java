package com.enigma.services.impl;

import com.enigma.entities.Purchased;

import java.util.List;

public interface PurchasedService {
    void save(Purchased purchased);
    Purchased getPurchased(String  id);
    List<Purchased> GetAll();
    Purchased purchasing(Purchased purchased);
    List<Purchased> getPurchasedfromuser(String id);
    Purchased saving(String id,Purchased purchased);
}
