package com.enigma.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "product")
public class Product {

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    private String id;
    private String name;
    private Integer quantity;
    private BigDecimal price;
    @ManyToOne()
    @JoinColumn(name = "store_id")
    @JsonIgnore
    private Store storeId;

    @OneToMany(mappedBy = "productIdok",cascade=CascadeType.PERSIST)
    @JsonIgnore
    List<Purchased> purchaseds = new ArrayList<>();
    @Transient
    private Integer pasangIdStore;

    public Product(String name, Integer quantity, BigDecimal price) {
        this.name = name;
        this.quantity = quantity;
        this.price = price;
    }

    public Integer getPasangIdStore() {
        return pasangIdStore;
    }

    public void setPasangIdStore(Integer pasangIdStore) {
        this.pasangIdStore = pasangIdStore;
    }

    public Store getStoreId() {
        return storeId;
    }

    public void setStoreId(Store storeId) {
        this.storeId = storeId;
    }

    public Product() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return Objects.equals(id, product.id) &&
                Objects.equals(name, product.name) &&
                Objects.equals(quantity, product.quantity) &&
                price.compareTo(product.price) == 0;
        //   Objects.equals(price, product.price);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, quantity, price);
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public void deductQuantity(Integer quantity) {
        this.quantity = this.quantity - quantity;
    }

//    @Transient
//    @JsonGetter(value = "store_id")
//    public Integer getIdstore() {
//        return Idstore ;
//    }

}
