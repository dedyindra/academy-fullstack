package com.enigma.entities;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
public class Purchased {
    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    private String id;
    private Integer quantity;
    private BigDecimal totalPrice;
     @ManyToOne()
    @JoinColumn(name = "user_id")
     private Users usersid;
     @Transient
    private String userId;

    @ManyToOne()
    @JoinColumn(name = "product_id")
    private Product productIdok;
    @Transient
    private String productId;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Purchased() {
    }

    public Users getUsersid() {
        return usersid;
    }

    public void setUsersid(Users usersid) {
        this.usersid = usersid;
    }

    public Product getProductIdok() {
        return productIdok;
    }

    public void setProductIdok(Product productIdok) {
        this.productIdok = productIdok;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(BigDecimal totalPrice) {
        this.totalPrice = totalPrice;
    }

    public void setPurchasedPrice(BigDecimal basePriceProduk) {
        this.totalPrice = basePriceProduk.multiply(new BigDecimal(this.quantity));
    }
}
