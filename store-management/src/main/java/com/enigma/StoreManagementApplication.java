package com.enigma;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.AutoConfigurations;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication

// PEMBUATAN BEAN MANUAL ADALAH pengganti dari anotaton spring bootaplication yang di atas awalnya kek dibawaj
//@EnableAutoConfiguration
//@EnableJpaRepositories(basePackages = "com.enigma.repository")
//@ComponentScan(basePackages = "com.enigma.controller")

//cara declare bean tanpa di dalam class
//@Configuration(AutoConfigurations)
public class StoreManagementApplication {

    public static void main(String[] args) {
        SpringApplication.run(StoreManagementApplication.class, args);
    }
// ini pembuatan nya di dalam
    //@Bean
}
