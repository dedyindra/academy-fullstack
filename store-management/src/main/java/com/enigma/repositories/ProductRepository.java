package com.enigma.repositories;

import com.enigma.entities.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface ProductRepository extends JpaRepository<Product,String> , JpaSpecificationExecutor<Product> {
    //====== METHOD QUERY=========
     // ini method query yang 1 kondisi by name
    public List<Product> findAllByNameContains(String keyword);
    // ini yang method query yang 2 kondisi by name dan id
    //public List<Product> findAllByNameAAndQuantityIsLessThanEqual(String keyword,Integer id);
    //=============NATIVE QUERY==========
//@Query(nativeQuery = true,value = "select * from product where name like ?")
//    public List<Product> cari(String keyword);
}

