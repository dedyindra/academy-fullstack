package com.enigma.repositories;

import com.enigma.entities.Store;
import com.enigma.spesification.StoreSpesification;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface StoreRepository extends JpaRepository<Store,Integer>, JpaSpecificationExecutor<Store> {
   Page<Store> findAllByStoreNameContainsOrAddressContainsOrDescriptionContainsOrPhoneNumberContains ( String authorName, String address, String description, String phone,Pageable pageable);
//@Query(nativeQuery = true,value = "select * from mst_store where store_name like '%?%' OR address like '%?%' or description like '%?%'")
//    public Page<Store> cari(Pageable pageable,String keyword);
    }
