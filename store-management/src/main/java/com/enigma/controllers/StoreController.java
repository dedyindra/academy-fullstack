package com.enigma.controllers;

import com.enigma.entities.Store;
import com.enigma.services.impl.StoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.web.bind.annotation.*;

@RestController
public class StoreController {
    @Autowired
    StoreService service;

    @GetMapping("/store/{id}")
    public Store getStoreById(@PathVariable Integer id){
        return service.getStore(id);
    }
    @PostMapping("/store")
    public Store save(@RequestBody Store store){
        return service.save(store);
    }
    @CrossOrigin
    @GetMapping("/store")
    public Page<Store> getListStore(@RequestParam Integer size , @RequestParam Integer page, @RequestParam String keywords){
        Pageable pageable = PageRequest.of(page,size);
        return service.GetAll(pageable,keywords);
    }
    @GetMapping("/store-search")
    public Page<Store> getListStored(@RequestParam Integer size , @RequestParam Integer page,@RequestBody Store store){
        ExampleMatcher exampleMatcher = ExampleMatcher.matchingAny().
                withStringMatcher(ExampleMatcher.StringMatcher.CONTAINING);
        Example<Store> storeExample = Example.of(store,exampleMatcher);
        Pageable pageable = PageRequest.of(page,size);
        return service.GetAllStore(pageable,storeExample);
    }

    @GetMapping("/store-search-spesification")
    public Page<Store> getListStoredWithSpesification(@RequestParam Integer size , @RequestParam Integer page,@RequestBody Store store){
        Pageable pageable = PageRequest.of(page,size);
        return service.GetAllStoreWithSpesification (pageable,store);
    }

    @GetMapping("/delete/{id}")
    public void deleteStore(@PathVariable Integer id){
        service.delete(id);
    }
}
