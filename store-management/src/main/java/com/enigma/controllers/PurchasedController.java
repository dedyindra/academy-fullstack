package com.enigma.controllers;

import com.enigma.entities.Purchased;
import com.enigma.services.impl.PurchasedService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class PurchasedController {
@Autowired
    PurchasedService purchasedService;
@GetMapping("/purchased")
    public List<Purchased> getAllPurchased(){
    return purchasedService.GetAll();
}
    @PostMapping("/purchased")
    public Purchased save(@RequestBody Purchased purchased){
        return purchasedService.purchasing(purchased);
    }
    @GetMapping("/purchased/{id}")
    public Purchased getPurcashedById(@PathVariable String id){
        return purchasedService.getPurchased(id);
    }
    @GetMapping("user/{id}/purchased")
    public List<Purchased> getstorrebyidandProduct(@PathVariable String id){
        return  purchasedService.getPurchasedfromuser(id);
    }
    @PostMapping("user/{id}/purchased")
    public Purchased posttProductByStoreId(@PathVariable String id, @RequestBody Purchased purchased){
        return purchasedService.saving(id,purchased);
    }
}

