package com.enigma.controllers;

        import com.enigma.entities.Product;
        import com.enigma.entities.Store;
        import com.enigma.services.impl.ProductService;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.data.domain.Page;
        import org.springframework.data.domain.PageRequest;
        import org.springframework.data.domain.Pageable;
        import org.springframework.web.bind.annotation.*;

        import java.util.List;

@RestController
public class ProductController {
    @Autowired
    ProductService productService;

    @GetMapping("/product")
    public Page<Product> getListProduct(@RequestParam Integer size , @RequestParam Integer page){
        Pageable pageable = PageRequest.of(page,size);
        return productService.GetAll(pageable);
    }

    @GetMapping("/products/{keyword}")
    public List<Product> getListProductByName(@PathVariable String keyword){
        return productService.GetAllByName(keyword);
    }
    @GetMapping("/product/{id}")
    public Product getProductById(@PathVariable String id){
        return productService.getProduct(id);
    }

    @PostMapping("product")
    public Product save(@RequestBody Product product){
        return productService.save(product);
    }
    @GetMapping("/delete-product/{id}")
    public void deleteProduct(@PathVariable String id){
        productService.delete(id);
    }

    @GetMapping("store/{id}/product")
    public List<Product> getstorrebyidandProduct(@PathVariable Integer id){
        return productService.getProductByStoreId(id);
    }
    @PostMapping("store/{id}/products")
    public Product postProductByStoreId(@PathVariable Integer id,@RequestBody Product product){
        return productService.saving(id, product);
    }
    @PostMapping("product-save")
    public Product getProductStoreId(@RequestBody Product product){
        return productService.saving(product.getPasangIdStore(),product);
    }
    @GetMapping("/product-search-spesification")
    public Page<Product> getListProudctdWithSpesification(@RequestParam Integer size , @RequestParam Integer page, @RequestBody Product product){
        Pageable pageable = PageRequest.of(page,size);
        return productService.GetAlLProductWithSpesification (pageable,product);
    }

}
