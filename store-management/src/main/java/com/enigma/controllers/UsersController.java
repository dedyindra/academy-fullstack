package com.enigma.controllers;

import com.enigma.entities.Users;
import com.enigma.services.impl.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class UsersController {
@Autowired
    UsersService usersService;
@GetMapping("/user/{id}")
    public Users getUserBuyId(@PathVariable String id){
return usersService.getUserById(id);
}
@PostMapping("/user")
    public Users saveUser(@RequestBody Users users){
    return usersService.save(users);
}
@GetMapping("/users")
    public List<Users> getListUser(){
    return usersService.GetAll();
}
@DeleteMapping("/delete-user/{id}")
    public void deleteUser(@PathVariable String id){
    usersService.delete(id);
}

}
