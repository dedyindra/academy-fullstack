package com.enigma.spesification;

import com.enigma.entities.Product;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.util.StringUtils;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.Collection;

public class ProductSpesification {
    public static Specification<Product> findByCriterias(Product product){
        return new Specification<Product>() {
            @Override
            public Predicate toPredicate(Root<Product> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
                final Collection<Predicate> predicates = new ArrayList<>();
                if(!StringUtils.isEmpty(product.getName())){
                    final Predicate namenamePredicate = criteriaBuilder.like(criteriaBuilder.lower(root.get("name")),"%"+product.getName()+"%");
                    predicates.add(namenamePredicate);
                }
                if(!StringUtils.isEmpty(product.getQuantity())){
                    final Predicate quantityPredicate = criteriaBuilder.like(criteriaBuilder.lower(root.get("quantity")),"%"+product.getQuantity()+"%");
                    predicates.add(quantityPredicate);
                }
                return criteriaBuilder.or(predicates.toArray(new Predicate[predicates.size()]));
            }
        };
    }

}
