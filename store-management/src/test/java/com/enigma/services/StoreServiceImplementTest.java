package com.enigma.services;

import com.enigma.entities.Store;
import com.enigma.repositories.StoreRepository;
import com.enigma.services.impl.StoreService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class StoreServiceImplementTest {
@Autowired
    StoreRepository storeRepository;
@Autowired
    StoreService storeService;
@Before
public void  cleanup(){
    storeRepository.deleteAll();
}
    @Test
    public void Store_save_expected_created() {
    Store store = new Store("dedystore","jakatra","jual ayam","0872121");
    Store  store2 = storeService.save(store);
    Store result = storeRepository.findById(store2.getId()).get();
    assertEquals(store2,result);
    }
    //KALO GET ALL NGECEKNYA PAKAI SIZE atau jumlah data nya itu lebuh baik OKE:)
    @Test
    public void Store_getAllProduct_should_true_whenSizeEqual_2() {
        Store store = new Store("dedystore","jakatra","jual ayam","0872121");
        Store storeDua = new Store("dedystore","jakatra","jual ayam","0872121");
          storeService.save(store);
         storeService.save(storeDua);
      assertEquals(2,storeRepository.findAll().size());
    }
    @Test
    public void store_get_by_id(){
        Store store2= new Store("dedystore","jakatra","jual ayam","0872121");
        storeService.save(store2);
        Store result = storeRepository.findById(store2.getId()).get();
        assertEquals(store2,result);
    }
//    @Test
//    public void  whenDeleteByIdFromRepository_thenDeletingShouldBeSuccessful(){
//        Store store2= new Store("dedystore","jakatra","jual ayam","0872121");
//        storeService.save(store2);
//        storeService.delete( storeRepository.findById(store2.getId()).get().getId());
//        assertEquals(0,storeService.GetAll().size());
//    }


}