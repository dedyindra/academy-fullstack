package com.enigma.services;

import com.enigma.entities.Product;
import com.enigma.repositories.ProductRepository;
import com.enigma.services.impl.ProductService;
import com.enigma.services.impl.PurchasedService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.List;

import static org.junit.Assert.*;
@RunWith(SpringRunner.class)
@SpringBootTest
public class ProductServiceImplementTest {

    @Autowired
    ProductRepository productRepository;
    @Autowired
    ProductService productService;
    @Autowired
    PurchasedService purchasedService;
    @Before
    public void  cleanup(){
    productRepository.deleteAll();
    }
    @Test
    public void Produk_save_expected_created() {
        BigDecimal bigDecimal =new BigDecimal(1000) ;
       Product product = new Product("ayam goreng",1,bigDecimal) ;
       product =  productService.save(product);
        Product result = productRepository.findById(product.getId()).get();
        assertEquals(product,result);
    }
    @Test
    public void Produk_get_by_id(){
        BigDecimal bigDecimal =new BigDecimal(1000) ;
        Product product = new Product("ayam goreng",1,bigDecimal) ;
        productService.save(product);
        Product result =  productRepository.findById(product.getId()).get();
        assertEquals(product,result);
    }
    @Test
    public void getAllProduct_should_true_whenSizeEqual_2() {
        BigDecimal bigDecimal =new BigDecimal(1000) ;
        Product product = new Product("ayam goreng",1,bigDecimal) ;
        Product product1 = new Product("ayam goreng",1,bigDecimal) ;
        productService.save(product);
        productService.save(product1);
        assertEquals(2,productRepository.findAll().size());
    }


    @Test
    public void Produk_quantity_should_2_when_produk_quantity2and_purchased_1(){
        BigDecimal bigDecimal =new BigDecimal(1000) ;
        Product product = new Product("ayam goreng",3,bigDecimal) ;
        productService.save(product);
        productService.deduct(product.getId(),1);
        Integer expectedQuantity = 2
                ;
        assertEquals(expectedQuantity,productRepository.findById(product.getId()).get().getQuantity());
    }
    @Test
    public void create_BigDesimal_from_produk(){
        BigDecimal bigDecimal =new BigDecimal(1000) ;
        Product product = new Product("ayam goreng",2,bigDecimal) ;
        productService.save(product);
        assertEquals(new BigDecimal("1000.00"),productRepository.findById(product.getId())
                .get().getPrice());
    }
    @Test
    public void getProdukByname_should_(){

        Product product = new Product("ayam goreng",2,new BigDecimal(1000));
        Product product1 = new Product("ayam bakar",2,new BigDecimal(1000));
        productService.save(product);
        productService.save(product1);
        List<Product> excepted=  productService.GetAllByName("sapi goreng");
        //List<Product> productList = productRepository.findAllByNameContains("sapi goreng");
        assertEquals(2,excepted.size());

    }
    //    @Test
//    public void Produk_quantity_should_1_when_produk_quantity2and_purchased_1(){
//        BigDecimal bigDecimal =new BigDecimal(1000) ;
//        Product product = new Product("ayam goreng",2,bigDecimal) ;
//        productService.save(product);
//        Purchased purchased = new Purchased();
//        purchased.setQuantity(1);
//        product.deductQuantity(purchased.getQuantity());
//        productService.save(product);
//     assertEquals(product.getQuantity(),productRepository.findById(product.getId()).get().getQuantity());
//    }
}