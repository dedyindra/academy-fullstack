package com.enigma.services;

import com.enigma.entities.Users;
import com.enigma.repositories.UsersRepository;
import com.enigma.services.impl.UsersService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;

import static org.junit.Assert.*;
@RunWith(SpringRunner.class)
@SpringBootTest
public class UsersServiceImplementTest {
@Autowired
    UsersService usersService;
@Autowired
    UsersRepository usersRepository;

@Before
public void cleanUp(){
    usersRepository.deleteAll();
}
    @Test
    public void save_user_when_insert_user() {
        Users users = new Users("dedy","jakarta",new Date(),"L") ;
        users =  usersService.save(users);
        Users result = usersRepository.findById(users.getId()).get();
        assertEquals(users,result);
    }
    @Test
    public void getUser_by_id(){
        Users users = new Users("dedy indra","jakarta",new  Date(),"L") ;
        usersService.save(users);
        Users result =  usersRepository.findById(users.getId()).get();
        assertEquals(users,result);
    }
    @Test
    public void User_getAll_should_true_whenSizeEqual_2() {
        Users users = new Users("dedy indra","jakarta",new  Date(),"L") ;
        Users users1 = new Users("dedy indra","jakarta",new  Date(),"L") ;
        usersRepository.save(users);
        usersRepository.save(users1);
        assertEquals(2,usersRepository.findAll().size());
    }
    @Test
    public void  whenDeleteByIdFromRepository_should_0_thenDeletingShouldBeSuccessful(){
        Users users1 = new Users("dedy indra","jakarta",new  Date(),"L") ;
        usersRepository.save(users1);
        usersService.delete( usersRepository.findById(users1.getId()).get().getId());
        assertEquals(0,usersService.GetAll().size());
    }
}