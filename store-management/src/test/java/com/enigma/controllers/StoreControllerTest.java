package com.enigma.controllers;

import com.enigma.entities.Store;
import com.enigma.repositories.StoreRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class StoreControllerTest {
@Autowired
    MockMvc mockMvc;
@Autowired
    StoreRepository storeRepository;
    @Test
    public void save_store_is_ok()throws  Exception {
        ObjectMapper mapper = new  ObjectMapper();
        Store store = new Store("dedystore","jakatra","jual ayam","0872121");
        mockMvc.perform(post("/store")
        .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(store))).andExpect(status().isOk());

    }
    @Test
    public void savestore_should_existinDb() throws  Exception {
        ObjectMapper mapper = new  ObjectMapper();
        Store store = new Store("dedystore","jakatra","jual ayam","0872121");
        String response=    mockMvc.perform(post("/store")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(store))).andReturn().getResponse().getContentAsString();
        store = new ObjectMapper().readValue(response,Store.class);
        Assert.assertEquals(store, storeRepository.findById(store.getId()).get());
    }

      @Test
    public void getAllToDos() throws Exception {
        List<Store> toDoList = new ArrayList<Store>();
        toDoList.add(new Store("dedystore","jakatra","jual ayam","0872121"));
        toDoList.add(new Store("dedystore","jakatra","jual ayam","0872121"));
        when(storeRepository.findAll()).thenReturn(toDoList);
        mockMvc.perform(MockMvcRequestBuilders.get("/store")
                .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(jsonPath("$.id", hasSize(2))).andDo(print());
    }

    @Test
    public void getSroreByIdAPI() throws Exception
    {
        Store store = new Store("dedystore","jakatra","jual ayam","0872121");
        mockMvc.perform( MockMvcRequestBuilders
                .get("/store/{id}", 1)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(store.getId()));
    }
}