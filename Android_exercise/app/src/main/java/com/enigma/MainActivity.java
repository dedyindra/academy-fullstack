package com.enigma;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    EditText username, password;
    Button login;
    TextView alertUsername;
    TextView alertPassword;
    String user = "dedy";
    String pwd = "1234";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        username = findViewById(R.id.editText);
        password = findViewById(R.id.editText3);
        alertUsername = findViewById(R.id.textView3);
        alertPassword = findViewById(R.id.textView4);
        login = findViewById(R.id.button);
    }

    public void login(View v) {
        String name = username.getText().toString();
        String pass = password.getText().toString();
        if (user != null && password!= null) {
            if (name.equals(user)&&pass.equals(pwd)) {
                Toast.makeText(getApplicationContext(),"LOGIN SUKSESS",Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(this, SecondActivity.class);
                intent.putExtra(SecondActivity.EXTRA_DATA, name);
                startActivity(intent);
            }else {
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage("user salah").setNegativeButton("close",null).show();
                username.setText("");
                password.setText("");
               // alertUsername.setText("username atau password salah ");
            }
        }else {
            //AlertDialog.Builder builder = new AlertDialog.Builder(this);
            //builder.setMessage("user salah").setNegativeButton("close",null).show();
            alertUsername.setText("username dan password tidak boleh kosong ");

        }
    }

}
