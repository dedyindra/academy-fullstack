package com.enigma;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;

public class SecondActivity extends AppCompatActivity {
    TextView username ;


    public static final String EXTRA_DATA = "1";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.second_activity);
        Intent intent = getIntent();
        final String data = intent.getStringExtra(EXTRA_DATA);

        username =  findViewById(R.id.textView);
        username.setText(data);

    }
    public void logout(View v){
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }


    @Override
    protected void onStop() {
        super.onStop();
        setContentView(R.layout.activity_main);
    }
}
