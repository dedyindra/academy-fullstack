package com.enigma.controller;

import com.enigma.model.Student;
import com.enigma.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.logging.Level;
import java.util.logging.Logger;
    @Controller
    public class StudentController {
        Logger logger = Logger.getLogger(StudentController.class.getName());
        @Autowired
       StudentService studentService;

    @RequestMapping("/")
    public String goToIndex(){
        return "index";
    }
//    @RequestMapping("/student")
//    public String goToStudent(){
//            return "student";
//        }
    @GetMapping("/student")
    public String gotoStudentForm(ModelMap model){
        logger.log(Level.INFO,"Melewati sini");
     //   model.addAttribute("hallo", studentService.getAll());
    return "student";
    }

    @GetMapping("/student-list")
    public String gotWhatEver(){
    return "student";
    }
    @PostMapping("/student-submit")
        public String doWhatever(@RequestParam String name, @RequestParam Integer id, ModelMap model){
        logger.log(Level.INFO,"Melewati param "+id);
        Student student = new Student();
        student.setId(id);
        student.setName(name);
        studentService.save(student);
       model.addAttribute("hallo", studentService.getAll());

        return "student";
    }

}


