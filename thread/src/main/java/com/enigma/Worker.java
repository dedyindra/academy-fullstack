package com.enigma;

public class Worker extends Thread{
    @Override
    public void run() {
        Thread current = Thread.currentThread();
        for (int i = 1; i <= 10 ; i++) {
            try {
                current.sleep(1200);
                System.out.println("Worker"+ current.getId() + " tread id = " + i);

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
