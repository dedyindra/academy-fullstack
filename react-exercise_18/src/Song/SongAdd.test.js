import React from 'react';
import {shallow} from 'enzyme';
import SongAdd from "./SongAdd";
//jelaskan
describe('song Add component component', () => {
    const songAdd = shallow(<SongAdd />)
    //pengujian render div
    describe('render', ()=> {
        it('should contain one div ', () => {
            expect(songAdd.find('div')).toHaveLength(1)
        })
        it('should contain one div and equal class name ', () => {
            expect(songAdd.find('div').prop('className')).toEqual("card-panel teal")
        })
        it('should contain one form ', () => {
            expect(songAdd.find('form')).toHaveLength(1)
        })
        it('should contain two label ', () => {
            expect(songAdd.find('label')).toHaveLength(2)
        })
        it('should contain one button ', () => {
            expect(songAdd.find('button')).toHaveLength(1)
        })
    });
});
