import React from 'react';
import {shallow} from 'enzyme';
import ArtistAdd from "./ArtistAdd";
//jelaskan
describe('artistAdd component component', () => {
    const artisAdd = shallow(<ArtistAdd />)
    //pengujian render div
    describe('render', ()=> {
        it('should contain one div ', () => {
            expect(artisAdd.find('div')).toHaveLength(1)
        })

        it('should contain one div and equal class name ', () => {
            expect(artisAdd.find('div').prop('className')).toEqual("card-panel teal")
        })
        it('should contain one form  as a child to div  ', () => {
            expect(artisAdd.find('div').children('form')).toHaveLength(1)
        })
        it('should contain one input  as a child to form  ', () => {
            expect(artisAdd.find('form').children('input')).toHaveLength(4)
        })
        it('should contain four label ', () => {
            expect(artisAdd.find('label')).toHaveLength(4)
        })
        it('should contain four button ', () => {
            expect(artisAdd.find('button')).toHaveLength(1)
        })


    });
});
