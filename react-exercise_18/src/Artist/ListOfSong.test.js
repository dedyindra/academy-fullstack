import React from 'react';
import {shallow} from 'enzyme';
import ListOfSong from "./ListOfSong";
//jelaskan
describe('sideBar component component', () => {
    const lisofSong = shallow(<ListOfSong kirim={[]} />)
    //pengujian render div
    describe('render', ()=> {
        it('should contain one div ', () => {
            expect(lisofSong.find('div')).toHaveLength(1)
        })

        it('should contain one div and equal class name ', () => {
            expect(lisofSong.find('div').prop('className')).toEqual("card-panel teal")
        })
    });
});
