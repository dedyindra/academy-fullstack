import React from 'react';
import {fetchData, PostData, PostDataWithImage} from "./ArtistService";
import {Redirect} from 'react-router-dom';

class ArtistAdd extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            redirect: false,
            artistForm: {}
        }
    }
    // handleInputName = (event) => {
    //     let artist = {...this.state.artistForm}
    //     artist.name = event.target.value
    //     this.setState({artistForm: {...artist}})
    // }
    // handleInputBornPlace = (event) => {
    //     let artist = {...this.state.artistForm}
    //     artist.bornPlace = event.target.value
    //     this.setState({artistForm: {...artist}})
    // }
    // handleInputDebut = (event) => {
    //     let artist = {...this.state.artistForm}
    //     artist.debut = event.target.value
    //     this.setState({artistForm: {...artist}})
    // }

    //  handleImage = (event) =>{
    //         let pictures = event.target.picture.files[0]
    //         return pictures;
    // }
    handleOnSubmit = (event) => {
        event.preventDefault();
        this.setState({
            redirect: true
        })
        let artist = this.state.artistForm;
        artist.name = event.target.name.value;
        artist.bornPlace = event.target.bornPlace.value;
        artist.debut = event.target.debut.value;
         this.setState({artistForm: artist})
        //this.setState({artistform: [...this.state.artistForm, artist]})
        console.log(this.state.artistForm)
        let pictures = event.target.picture.files[0];
        PostDataWithImage(this.state.artistForm,pictures)
    }
    renderRedirect = () => {
        if (this.state.redirect) {
            return <Redirect to='/artis-list'/>
        }
    }

    render() {
        return (
            <div className="card-panel teal">
                <form onSubmit={this.handleOnSubmit} >
                    <label>name : </label><br/>
                    <input type="text" name="name"/>
                    <label>bornPlace : </label><br/>
                    <input type="text" name="bornPlace"/>
                    <label>Debut : </label><br/>
                    <input type="date" name="debut"/>
                    <label>image : </label><br/>
                    <input type="file" name="picture"/> <br/> <br/>
                    {this.renderRedirect()}
                    <button type="submit">Submit</button>
                </form>
            </div>

        );

    }
}

export default ArtistAdd;
