import React from 'react';
import {shallow} from 'enzyme';
import Navbar from "./Navbar";
//jelaskan
describe('navbar component component', () => {
    const navBar = shallow(<Navbar />)
    //pengujian render div
    describe('render', ()=> {
        it('should contain one nav ', () => {
            expect(navBar.find('nav')).toHaveLength(1)
        })
        it('should contain one div ', () => {
            expect(navBar.find('div')).toHaveLength(1)
        })
        it('should contain one div and equal class name ', () => {
            expect(navBar.find('div').prop('className')).toEqual("nav-wrapper")
        })
        it('should contain one form  as a child to nav  ', () => {
            expect(navBar.find('div').children('nav')).toHaveLength(1)
        })
        it('should contain one a href ', () => {
            expect(navBar.find('a')).toHaveLength(3)
        })
        it('should contain one i and equal class name ', () => {
            expect(navBar.find('i').prop('className')).toEqual("material-icons")
        })
        it('should contain one i ', () => {
            expect(navBar.find('i')).toHaveLength(1)
        })
        it('should contain one img ', () => {
            expect(navBar.find('img')).toHaveLength(1)
        })
        it('should contain one img and equal class name ', () => {
            expect(navBar.find('img').prop('className')).toEqual("music-logo")
        })
        it('should contain one ul ', () => {
            expect(navBar.find('ul')).toHaveLength(1)
        })
        it('should contain one ul lii ', () => {
            expect(navBar.find('ul').children('li')).toHaveLength(2)
        })


    });
});
