import React from 'react';
import {shallow} from 'enzyme';
import SideBar from "./SideBar";
//jelaskan
describe('sideBar component component', () => {
    const sideBar = shallow(<SideBar />)
    //pengujian render div
    describe('render', ()=> {
        it('should contain one div ', () => {
            expect(sideBar.find('div')).toHaveLength(1)
        })

        it('should contain one div and equal class name ', () => {
            expect(sideBar.find('div').prop('className')).toEqual("collection")
        })


    });
});
