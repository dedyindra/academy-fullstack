import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('http://3c6b7914.ngrok.io/')

WebUI.setText(findTestObject('Object Repository/Page_Test expose localhost/input_fill the form_form-control'), 'admin')

WebUI.setEncryptedText(findTestObject('Object Repository/Page_Test expose localhost/input_Username_form-control'), 'RAIVpflpDOg=')

WebUI.click(findTestObject('Object Repository/Page_Test expose localhost/button_submit'))

WebUI.click(findTestObject('Object Repository/Page_Test expose localhost/button_submit'))

WebUI.click(findTestObject('Object Repository/Page_Test expose localhost/div_fill the formUsernamepasswordsubmit'))

WebUI.click(findTestObject('Object Repository/Page_Test expose localhost/div_fill the formUsernamepasswordsubmit'))

WebUI.click(findTestObject('Object Repository/Page_Test expose localhost/button_submit'))

WebUI.acceptAlert()

if (true) {
    WebUI.setText(findTestObject('Object Repository/Page_Test expose localhost/input_fill the form_form-control'), 'test')

    WebUI.acceptAlert()
}

