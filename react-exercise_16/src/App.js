import React from 'react';
import './App.css';
import SimpleComponent from "./SimpleComponent";
import Card from "./Card";
import Header from "./Header";
import StudentForm from "./student/StudentForm";
import './materialize.min.css';

class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            people: []
        }
    }
    ubahname=(event)=>{
        let newPeople = {...this.state.people}
        newPeople.name=event.target.value
        this.setState({people:{...newPeople}})
    }

    submit = (event) => {
        event.preventDefault()
        let newPeople = {...this.state.people}
        newPeople.name = event.target.name.value
        newPeople.age = event.target.age.value
        console.log("submit data")
        console.log(this.state.people)
        this.setState({people: [...this.state.people, newPeople]})
    }

    render() {
        let items = [];
        for (let i = 0; i < this.state.people.length; i++) {
            items.push(<Card kirim={this.state.people[i]}/>)
        }
        return (

            <div className="App">
                <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"/>
                <Header/>
                <SimpleComponent/>
                <StudentForm submit={this.submit}/>
                {items}
            </div>

        );

    }
}


export default App;
