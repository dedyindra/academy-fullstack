import React from 'react';
import './App.css';
import logo from "./logo.svg";

class Counter extends React.Component {

    constructor(props) {
        super(props);
        this.state={
            value:0
        }
    }

    minus =()=>{
        let currenValue=this.state.value
        this.setState({value:currenValue -1 })
    }
    plus=()=>{
        let currenValue = this.state.value
        this.setState({value:currenValue +1})
    }
    render() {
        return (
            <div className="App-card">
                <button onClick={this.minus}>minus</button>
                <button onClick={this.plus}>Tambah</button>
                <h1>angka : { this.state.value}</h1>
            </div>
        );
    }
}

export default Counter;
