import React from 'react';
import {shallow} from 'enzyme';
import Header from "./Header";


//jelaskan
describe('header component', () => {
    const headerComponent = shallow(<Header/>)
    //pengujian render div
    describe('render', ()=> {
        it('should contain one div ', () => {
            expect(headerComponent.find('div')).toHaveLength(1)
        })
        it('should contain one header as a child to div ', () => {
            expect(headerComponent.find('div').children('header')).toHaveLength(1)
        })
        it('should contain one header as a child to div and equal class name ', () => {
            expect(headerComponent.find('div').children('header').prop('className')).toEqual("App-header")
        })
        it('should contain one img as a child to header ', () => {
            expect(headerComponent.find('header').children('img')).toHaveLength(1)
        })

    });
});
