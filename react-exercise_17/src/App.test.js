import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import {shallow} from 'enzyme';


//jelaskan
describe('App component', () => {
  const appContainer = shallow(<App/>)
  //pengujian render div
  describe('render', ()=> {
    it('should contain one div ', () => {
      expect(appContainer.find('div')).toHaveLength(1)
    })
    it('should contain one script as a child to div ', () => {
      expect(appContainer.find('div').children('script')).toHaveLength(1)
    })
    it('should contain one script as a child to div and equal src', () => {
      expect(appContainer.find('div').children('script').prop('src')).toEqual("https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js")
    })
    it('should contain one header as a child to div ', () => {
      expect(appContainer.find('div').children('Header')).toHaveLength(1)
    })
    it('should contain one Navigation as a child to div ', () => {
      expect(appContainer.find('div').children('Navigation')).toHaveLength(1)
    })

  });
});
