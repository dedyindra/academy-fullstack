import React from 'react';
import {Table} from 'reactstrap';
import {Link} from "react-router-dom";
class Product_list extends React.Component {
    render() {
        let items = this.props.kirim;
        return (
            <div>
                <table className="striped">
                    <tr>
                        <th>id</th>
                        <th>nama product</th>
                        <th>quantity</th>
                        <th>price</th>
                        <th>detail</th>
                    </tr>
                    {items.map(item => {
                        return (
                            <tr>
                                <td>{item.product.id}</td>
                                <td>{item.product.name}</td>
                                <td>{item.product.quantity}</td>
                                <td>{item.product.prize}</td>
                                <td><Link to={"/product-list/"+item.product.id}>detail</Link></td>
                            </tr>
                        );
                    })}
                </table>

            </div>

        );
    }
}
export default Product_list;
