import React from 'react';
import {shallow} from 'enzyme';
import Product_list from "./Product_list";


//jelaskan
describe('Product-list component', () => {
    const productList = shallow(<Product_list kirim={[]} />)
    //pengujian render div
    describe('render', ()=> {
        it('should contain one div ', () => {
            expect(productList.find('div')).toHaveLength(1)
        })
        it('should contain one table  as a child to div  ', () => {
            expect(productList.find('div').children('table')).toHaveLength(1)
        })
        it('should contain one table and equal class name ', () => {
            expect(productList.find('table').prop('className')).toEqual("striped")
        })
        it('should contain one tr no map ', () => {
            expect(productList.find('tr')).toHaveLength(1)
        })
        it('should contain one th  as a child to tr  ', () => {
            expect(productList.find('tr').children('th')).toHaveLength(5)
        })


    });
});
