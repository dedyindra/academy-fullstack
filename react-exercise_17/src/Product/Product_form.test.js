import React from 'react';
import {shallow} from 'enzyme';
import Product_list from "./Product_list";
import Product_form from "./Product_form";


//jelaskan
describe('Product-form component', () => {
    const productForm = shallow(<Product_form kirim={[]} />)
    //pengujian render div
    describe('render', ()=> {
        it('should contain one div ', () => {
            expect(productForm.find('div')).toHaveLength(1)
        })
        it('should contain one form  as a child to div  ', () => {
            expect(productForm.find('div').children('form')).toHaveLength(1)
        })
        it('should contain one input  as a child to form  ', () => {
            expect(productForm.find('form').children('input')).toHaveLength(4)
        })
        describe('function', ()=> {
            it('td name should contain string  onchange perform ', () => {
                productForm.find('input').at(1).simulate('onSubmit',{target:{value:'angga'}})
                //bisa di console
                const productList = shallow(<Product_list kirim={[]}  />).dive()
                productForm.find('button').at(0).simulate('click',{})
                expect(productList.find('td').at(1).text()).toEqual('angga')
            })
        });
    });
});
