import React from 'react';
import {shallow} from 'enzyme';
import Product_detail from "./Product_detail";


//jelaskan
describe('Product-detail component', () => {
    const productDetail = shallow(<Product_detail kirim={[]} />)
    //pengujian render div
    describe('render', ()=> {
        it('should contain one div ', () => {
            expect(productDetail.find('div')).length(1)
        })
        it('should contain one form  as a child to div  ', () => {
            expect(productDetail.find('div').children('form')).toHaveLength(1)
        })
        it('should contain one input  as a child to form  ', () => {
            expect(productDetail.find('form').children('input')).toHaveLength(4)
        })
    });
});
