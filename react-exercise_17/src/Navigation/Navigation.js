import React from 'react';
import '../App.css';
import logo from "../logo.svg";
import {Link} from "react-router-dom";

class Navigation extends React.Component {
    render() {
        return (
            <div className="nav-wrapper">
                <nav>
                    <a href="#" className="brand-logo right">Product</a>
                    <ul id="nav-mobile" className="left hide-on-med-and-down">
                        <li><Link to="/product-list/">product-list</Link></li>
                        <li><Link to="/product-form">product</Link></li>
                    </ul>
                </nav>
            </div>
        );
    }
}


export default Navigation;
