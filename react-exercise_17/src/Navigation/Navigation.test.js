import React from 'react';
import {shallow} from 'enzyme';
import Navigation from "./Navigation";
//jelaskan
describe('Navigation component', () => {
    const NavigationComponent = shallow(<Navigation/>)
    //pengujian render div
    describe('render', ()=> {
        it('should contain one div ', () => {
            expect(NavigationComponent.find('div')).toHaveLength(1)
        })
        it('should contain one nav as a child to div ', () => {
            expect(NavigationComponent.find('div').children('nav')).toHaveLength(1)
        })
        it('should contain one ul as a child to nav ', () => {
            expect(NavigationComponent.find('nav').children('ul')).toHaveLength(1)
        })
        it('should contain one li as a child to ul ', () => {
            expect(NavigationComponent.find('ul').children('li')).toHaveLength(2)
        })
        it('should contain one div v and equal class name ', () => {
            expect(NavigationComponent.find('div').prop('className')).toEqual("nav-wrapper")
        })
        it('should contain one ul as a child to nav  and equal class name', () => {
            expect(NavigationComponent.find('nav').children('ul').prop('className')).toEqual("left hide-on-med-and-down")
        })

    });
});
