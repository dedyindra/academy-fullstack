import React from 'react';
import logo from './logo.svg';
import './App.css';
import Header from "./component/Header";
import {BrowserRouter as Router, Route, Switch, Link} from "react-router-dom";
import Product_form from "./Product/Product_form";
import Product_list from "./Product/Product_list";
import './materialize.min.css';
import Product_detail from "./Product/Product_detail";
import Navigation from "./Navigation/Navigation";
class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            product: []
        }
    }
    submit = (event) => {
        event.preventDefault()
        let newproduct = {...this.state.product}
        newproduct.id = event.target.id.value++
        newproduct.name = event.target.name.value
        newproduct.quantity = event.target.quantity.value
        newproduct.prize = event.target.prize.value
        console.log("submit data")
        console.log(this.state.product)
        this.setState({product: [...this.state.product, newproduct]})
    }
    render() {
        let items = [];
        for (let i = 0; i < this.state.product.length; i++) {
            items.push({product: this.state.product[i]})
        }
        return (
            <Router>
                <div className="App">
                    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"/>
                    <Header/>
                    <Navigation/>
                    <Switch>
                        <Route path="/product-list/" exact><Product_list kirim={items}/></Route>
                        <Route path="/product-list/:id" render={(props) => <Product_detail {...props}  kirim={this.state.product} />}></Route>
                        <Route path="/product-form"><Product_form submit={this.submit}/></Route>
                    </Switch>
                </div>
            </Router>
        );
    }
}

export default App;
