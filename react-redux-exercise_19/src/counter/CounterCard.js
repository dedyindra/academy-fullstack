import React from 'react';
import '../materialize.min.css'
import {connect} from "react-redux"
import {decremnetrAction, incrementAction} from "./CounterAction";
class CounterCard extends React.Component {
    render() {
        return (
            <div className="custom-row">
                    <div className="card-panel teal">
                        <button onClick={()=>{this.props.dispatch({...incrementAction, index : this.props.index})}} >plus</button>
                        <h1>{this.props.element}</h1>
                        <button onClick={()=>{this.props.dispatch({...decremnetrAction, index : this.props.index})}} >min</button>
                    </div>
            </div>
        );
    }
}
export default connect()(CounterCard);
