import CounterContainer from "../CounterContainer";

const intialState = {
    component:[],
    people:{name:"dedy"}
}
export  default  function counter(state=intialState,action) {
    console.log(state,action)
    switch (action.type) {
        case 'ADD_ELEMENT':
                    return {...state, component : state.component.concat([0])};
        case 'INCREMENT':
                return {...state, component : state.component.map((element,index)=>{
                if (index===action.index){
                    return element +   1
                 }else {
                     return element
                 }
             })}
        case 'DECREMENT':
            return {...state, component : state.component.map((element,index)=>{
                    if (index===action.index){
                        return element -1
                    }else {
                        return element
                    }
                })}
        case 'PLUS':
            let newPeople = {...this.state.people}
            newPeople.name=action.target.value
            return {people:{...newPeople}}

        default:return state;
    }
}