import React from 'react';
import '../materialize.min.css'
import {connect} from "react-redux"
import {addElemenAction, decremnetrAction, incrementAction} from "./CounterAction";
import CounterCard from "./CounterCard";
class CounterContainer extends React.Component {
    render() {
        return (
            <div className="custom-row">
                <button onClick={()=>{this.props.dispatch(addElemenAction)}}> Tambah Counter</button>
                {this.props.component.map((element ,index )=>{
                    return <CounterCard index={index} element={element}  key={index} />
                })}
            </div>
        );
    }
}
const mapStateToProps=(state)=>{
    return {component: state.component}
}
export default connect(mapStateToProps)(CounterContainer);
